<?php
  //Data, connection, auth
        $dataFromTheForm = $_POST['fieldName']; // request data from the form
        $soapUrl = "https://www.billhighway.com/API/payment.asmx?op=ccPaymentByGroup"; // asmx URL of WSDL
        

        // xml post structure

        $xml_post_string = '<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <ccPaymentByGroup xmlns="www.billhighway.com/api">
      <apiKey>string</apiKey>
      <featureID>string</featureID>
      <clientID>int</clientID>
      <groupID>int</groupID>
      <Amount>decimal</Amount>
      <nameOnCard>string</nameOnCard>
      <cardType>string</cardType>
      <cardNumber>string</cardNumber>
      <expMonth>string</expMonth>
      <expYear>string</expYear>
      <cvv>string</cvv>
      <address>string</address>
      <city>string</city>
      <state>string</state>
      <postalCode>string</postalCode>
      <email>string</email>
      <memo>string</memo>
      <occurs>string</occurs>
      <numberOfOccurrences>string</numberOfOccurrences>
    </ccPaymentByGroup>
  </soap:Body>
</soap:Envelope>';   // data from the form, e.g. some ID number

           $headers = array(
                        "Content-type: text/xml;charset=\"utf-8\"",
                        "SOAPAction: www.billhighway.com/api/ccPaymentByGroup", 
                        "Content-length: ".strlen($xml_post_string),
                    ); //SOAPAction: your op URL

            $url = $soapUrl;

            // PHP cURL  for https connection with auth
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_USERPWD, $soapUser.":".$soapPassword); // username and password - declared at the top of the doc
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            // converting
            $response = curl_exec($ch); 
            curl_close($ch);

            print($response);
?>
