<<<<<<< HEAD
$(function(){
	$('#notification_close').live('click', function(){ $('#notification').slideUp() });

	// Hide data collection fields upon loading and tie slidetoggle to heading
	var name_changed = false;
	var fields= new Array();
	$('#data_collection_heading').click(function(){
		$('#data_collection').slideToggle();
		$(this).toggleClass('contract expand');
	});

	$('#list_name').change(function(){name_changed=true}); // Only check the name if the input field has changed

	// Add ajax to list name field to check for dupes
	$('#list_name').blur(function(){
		if ($('#list_name').val().length != 0 && name_changed)
		{
			$('#list_name_container').css('background-image','url(images/ajax-loader.gif)');
			$.get("ajax/check-listname.php", { list_name: $(this).val() }, function(data){
				img = (data) ? 'url(images/check.png)':'url(images/error.png)';
				$('#list_name_container').css('background-image',img);
			});
		}
	});

	// Build drag and drop data collection boxes
	$('ul.droptrue').sortable({
		connectWith: 'ul',
		opacity: 0.6,
		placeholder: 'ui-state-highlight',
		forcePlaceholderSize:true,
		revert: 200
	});

	// Upon saving, collect data and submit to add_list.php
	$('#save').click(function(){
		// collect the form input and data collection data and send via ajax to a script to add.
		$('#chosen li').each(function(index){
			//console.log($(this).attr('id'));
			fields[index] = $(this).attr('id');
		});
		//console.log($('form').serialize());
		if($('input[name="list_name"]').val().length==0 || $('input[name="list_owner"]').val().length==0)
		{
			alert('Please enter a list name and owner email address');
			return false;
		}
		var list_status = ($('input:[name=list_status]:checked').val()==1) ? 1:0;
		$.get('ajax/add-list.php', {
			list_name: $('input:[name=list_name]').val(), 
			list_owner: $('input:[name=list_owner]').val(), 
			list_description: $('textarea[name=list_description]').val(), 
			list_status:list_status, 
			data_collection: fields.toString()}, function(data){
				if(data)
				{
					$('#notification').html('List successfully created. <span id="notification_close"><img src="images/close.png" style="vertical-align:middle" alt="close"></span>').hide().slideDown();
					$('input:[name=list_name]').val('');
					$('textarea[name=list_description]').val('');
					$.get("ajax/generate-form.php", { id: data}, function(data){
						$('#list_add_html').html('Subscribe/Unsubscribe HTML:'+data).hide().fadeIn();
					});
				}
				else
				{
					$('#notification').html('Error - unable to create list. <span id="notification_close"><img src="images/close.png" style="vertical-align:middle" alt="close"></span>').hide().slideDown();
				}
				$('#notification_close').click(function(){ $('#notification').slideUp() });
		});
	});

	// Upon updating an existing list, collect data and submit to update_list.php
	$('#update').click(function(){
		if(confirm('Are you sure you want to update this list?'))
		{
			// collect the form input and data collection data and send via ajax to a script to add.
			$('#chosen li').each(function(index){
				//console.log($(this).attr('id'));
				fields[index] = $(this).attr('id');
			});
			//console.log($('form').serialize());
			var list_status = ($('input:[name=list_status]:checked').val()==1) ? 1:0;
			$.get('ajax/update-list.php', {
				list_id: $('input:[name=list_id]').val(),
				list_name: $('input:[name=list_name]').val(), 
				list_owner: $('input:[name=list_owner]').val(), 
				list_description: $('textarea[name=list_description]').val(), 
				list_status: list_status, 
				data_collection: fields.toString()}, function(data){
				//alert('Data Loaded: ' + data);
				if(data=="updated") window.location = "list_management_edit.php?u=1&id="+$('input:[name=list_id]').val();
			});
		}
	});

	$('#delete').click(function(){
		if(confirm('Are you sure you want to delete this list?')) window.location.replace("list_management_edit.php?a=d&id="+$('input:[name=list_id]').val());
	});

	$('#duplicate').click(function(){
		if(confirm('Are you sure you want to duplicate this list? Duplicating a list will create a new list with "_copy" appended to the list name (which you can change), containing the same members as the original list.'))
		{
			window.location.replace("list_management_edit.php?a=c&id="+$('input:[name=list_id]').val());
		}
	});

	$('#list_add_html').hide();

	$('#list_list').change(function(){
		if($(this).val()>0)
		{
			window.location.replace("list_management_edit.php?id="+$(this).val()); // reload page and pass the id
		}
	});

	$('#navlist a').each(function(){
		if($(this).text()=='Lists') $(this).attr('id','current');
	});
});
=======
$(function(){
	$('#notification_close').live('click', function(){ $('#notification').slideUp() });

	// Hide data collection fields upon loading and tie slidetoggle to heading
	var name_changed = false;
	var fields= new Array();
	$('#data_collection_heading').click(function(){
		$('#data_collection').slideToggle();
		$(this).toggleClass('contract expand');
	});

	$('#list_name').change(function(){name_changed=true}); // Only check the name if the input field has changed

	// Add ajax to list name field to check for dupes
	$('#list_name').blur(function(){
		if ($('#list_name').val().length != 0 && name_changed)
		{
			$('#list_name_container').css('background-image','url(images/ajax-loader.gif)');
			$.get("ajax/check-listname.php", { list_name: $(this).val() }, function(data){
				img = (data) ? 'url(images/check.png)':'url(images/error.png)';
				$('#list_name_container').css('background-image',img);
			});
		}
	});

	// Build drag and drop data collection boxes
	$('ul.droptrue').sortable({
		connectWith: 'ul',
		opacity: 0.6,
		placeholder: 'ui-state-highlight',
		forcePlaceholderSize:true,
		revert: 200
	});

	// Upon saving, collect data and submit to add_list.php
	$('#save').click(function(){
		// collect the form input and data collection data and send via ajax to a script to add.
		$('#chosen li').each(function(index){
			//console.log($(this).attr('id'));
			fields[index] = $(this).attr('id');
		});
		//console.log($('form').serialize());
		if($('input[name="list_name"]').val().length==0 || $('input[name="list_owner"]').val().length==0)
		{
			alert('Please enter a list name and owner email address');
			return false;
		}
		var list_status = ($('input:[name=list_status]:checked').val()==1) ? 1:0;
		$.get('ajax/add-list.php', {
			list_name: $('input:[name=list_name]').val(), 
			list_owner: $('input:[name=list_owner]').val(), 
			list_description: $('textarea[name=list_description]').val(), 
			list_status:list_status, 
			data_collection: fields.toString()}, function(data){
				if(data)
				{
					$('#notification').html('List successfully created. <span id="notification_close"><img src="images/close.png" style="vertical-align:middle" alt="close"></span>').hide().slideDown();
					$('input:[name=list_name]').val('');
					$('textarea[name=list_description]').val('');
					$.get("ajax/generate-form.php", { id: data}, function(data){
						$('#list_add_html').html('Subscribe/Unsubscribe HTML:'+data).hide().fadeIn();
					});
				}
				else
				{
					$('#notification').html('Error - unable to create list. <span id="notification_close"><img src="images/close.png" style="vertical-align:middle" alt="close"></span>').hide().slideDown();
				}
				$('#notification_close').click(function(){ $('#notification').slideUp() });
		});
	});

	// Upon updating an existing list, collect data and submit to update_list.php
	$('#update').click(function(){
		if(confirm('Are you sure you want to update this list?'))
		{
			// collect the form input and data collection data and send via ajax to a script to add.
			$('#chosen li').each(function(index){
				//console.log($(this).attr('id'));
				fields[index] = $(this).attr('id');
			});
			//console.log($('form').serialize());
			var list_status = ($('input:[name=list_status]:checked').val()==1) ? 1:0;
			$.get('ajax/update-list.php', {
				list_id: $('input:[name=list_id]').val(),
				list_name: $('input:[name=list_name]').val(), 
				list_owner: $('input:[name=list_owner]').val(), 
				list_description: $('textarea[name=list_description]').val(), 
				list_status: list_status, 
				data_collection: fields.toString()}, function(data){
				//alert('Data Loaded: ' + data);
				if(data=="updated") window.location = "list_management_edit.php?u=1&id="+$('input:[name=list_id]').val();
			});
		}
	});

	$('#delete').click(function(){
		if(confirm('Are you sure you want to delete this list?')) window.location.replace("list_management_edit.php?a=d&id="+$('input:[name=list_id]').val());
	});

	$('#duplicate').click(function(){
		if(confirm('Are you sure you want to duplicate this list? Duplicating a list will create a new list with "_copy" appended to the list name (which you can change), containing the same members as the original list.'))
		{
			window.location.replace("list_management_edit.php?a=c&id="+$('input:[name=list_id]').val());
		}
	});

	$('#list_add_html').hide();

	$('#list_list').change(function(){
		if($(this).val()>0)
		{
			window.location.replace("list_management_edit.php?id="+$(this).val()); // reload page and pass the id
		}
	});

	$('#navlist a').each(function(){
		if($(this).text()=='Lists') $(this).attr('id','current');
	});
});
>>>>>>> b59280b13ce027f89f28f2d12e991e62fe2dd1bd
