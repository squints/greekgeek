<<<<<<< HEAD
$(function(){
	/*
	$('#showContent').click(function(){
		content = (tinyMCE.get('message')) ? tinyMCE.get('message').getContent(): $('#message').val();
		console.log(content);
		alert(content.length);
	});
	*/
	$("#loadTemplateDialog").dialog({
		width:400,
		maxWidth:800,
		height:500,
		autoOpen: false,
		modal:true
	});
	$("#saveTemplateDialog").dialog({
		autoOpen: false,
		modal:true
	});
	$('#loadDraftsDialog').dialog({
		autoOpen: false,
		modal:true,
		position: 'center',
		maxWidth:800,
		width: 600
	});
	$('#attachmentDialog').dialog({
		autoOpen: false,
		modal:true,
		position: 'center',
		width: 580,
		height: 130
	});
	$("#sendTestDialog").dialog({
		autoOpen: false
	});
	$('#fieldNoteDialog').dialog({
		autoOpen: false
	});

	$('#data_fields').hide();
	var leaveWarning = true;

	$('#addAttachment').click(function(){
		$('#attachmentDialog').dialog('open'); // open jQuery dialog modal
	});
	$('#field_note').live('click', function(){
		$('#fieldNoteDialog').dialog('open'); // open jQuery dialog modal
	});
	$('#send_test').click(function(event){
		event.preventDefault();
		$("#sendTestDialog").dialog('open'); // open jQuery dialog modal
		$('#sendTestAddress').focus().select();
	});
	$('#testSend').click(function(event){
		$('#testResult').html('<img src="images/ajax-loader.gif"> Sending test...');
		event.preventDefault();
		content = (tinyMCE.get('message')) ? tinyMCE.get('message').getContent(): $('#message').val();
		subject = $('#subject').val();
		to = $('#sendTestAddress').val();
		format = $('#format').val();
		attachments = '';
		$('input[name="attachment[]"]').each(function(){
			if($(this).val() != '') attachments += $(this).val() +',';
		});
		$.post('ajax/test-send.php', {subject:subject,message:content,to:to,attachments:attachments,format:format}, function(data) {
  			if(data == 1) $('#testResult').html('<img src="images/check.png"> Test message sent successfully').show().delay(5000).fadeOut(750);
  			if(data == 0) $('#testResult').html('<img src="images/delete.png"> Error sending message').show().delay(5000).fadeOut(750);
  			if(data == 3) $('#testResult').html('<img src="images/warning.png"> Error adding attachment').show().delay(5000).fadeOut(750);
		});
	});

	// Send message for processing
	$('#send').click(function(){
		// check subject and message for empty. check lists for minimum one selected.
		contentLength = (tinyMCE.get('message')) ? tinyMCE.get('message').getContent().length-200: $('#message').val().length;
		if($('input[name="lists[]"]:checked').length==0)
		{
			alert('You need to select at least one list to send your message to.');
			return false;
		}
		if($('#recipient_count').text() == '0 recipients')
		{
			alert('The list(s) you have chosen have no recipients. Please check your selections and try again.');
			return false;
		}
		if($('#subject').val().length==0 || contentLength==0)
		{
			alert('Please make sure you have filled out both the subject and message.');
			return false;
		}
		if( $('input[name=when]:checked').val() == "later" && $('#datepicker').val()=='' )
		{
			alert('Please enter a date to begin delivery');
			$('#datepicker').focus();
			return false;
		}
		if(!confirm('Send message?'))
		{
			return false;
		}
	});

	///////////////
	// TEMPLATES //
	///////////////

	$('#loadTemplate').click(function(){
		$("#loadTemplateDialog").dialog('open'); // open jQuery dialog modal
		$.get("ajax/list-templates.php", function(data){ // Call ajax/list-templates.php to generate the list of templates and populate dialog box
			$('#loadTemplateDialog').html(data);
		});
	});
	$('#saveTemplate').click(function(){
		$("#saveTemplateDialog").dialog('open'); // open jQuery dialog modal
	});
	$('#save').click(function(event){
		event.preventDefault(); // Stops the save button from submitting the save template form
		$('#save').after('<img id="template_waiting" src="images/ajax-loader.gif">');
		content = (tinyMCE.get('message')) ? tinyMCE.get('message').getContent(): $('#message').val();
		$.get("ajax/save-template.php", {filename:$('#saveTemplateName').val(), content:content}, function(data){
			$('#template_waiting').remove();
			if(data=='success')
			{
				$('#saveTemplateDialog').dialog('close'); // close jQuery dialog modal upon choosing a template
				$('#notification').html('Template successfully saved');
			}
			else if(data=='exists')
			{
				alert('The template name you entered already exists or is blank. Please choose another.');
			}
			else if(data=='unwritable')
			{
				alert('The templates folder cannot be written to. Please check the permissions and try again.');
			}
		});
	});
	$('.template').live('click', function(event) {
		event.preventDefault();
		$('#loadTemplateDialog').dialog('close'); // close jQuery dialog modal upon choosing a template
		if(confirm('Loading a template will overwrite any content currently in the message window. Continue?'))
		{
			if(tinyMCE.get('message')) tinyMCE.get('message').setProgressState(1);
			$.get("ajax/get-template.php", {filename:$(this).text()}, function(data){ // Call ajax/get-template.php to retrieve content of template and populate editor
				if(tinyMCE.get('message'))
				{
					tinyMCE.get('message').setProgressState(0);
					tinyMCE.get('message').setContent(data);
				}
				else $('#message').val(data);
			});
		}
	});
	$('.delete_template').live('click', function(event) {
		event.preventDefault();
		if(confirm('Are you sure you want to delete this template?'))
		{
			$.get("ajax/delete-template.php", {filename:$(this).attr('href')}, function(data){ // Call ajax/delete-template.php to delete template
				// possibly update notification box with message
				$('#loadTemplateDialog').dialog('close'); // close jQuery dialog modal upon success
			});
		}
	});

	///////////////////////
	// DRAFTS & AUTOSAVE //
	///////////////////////

	function autosave(){
		contentLength = (tinyMCE.get('message')) ? tinyMCE.get('message').getContent().length-200: $('#message').val().length;
		if(contentLength>0 && $('#subject').val().length > 0) // Only save a draft if the subject and message are not empty
		{
			attachments = ($('input[name="attachment[]"]').length > 1) ? $('#attachments ul').html():'';
			content = (tinyMCE.get('message')) ? tinyMCE.get('message').getContent(): $('#message').val();
			$.get("ajax/autosave.php", {firstTime:firstTime, id:messageID, subject:$('#subject').val(), content:content, format:$('#format').val(), attachments:attachments}, function(data){
				if(data != 0)
				{
					var now = new Date();
					$('#notification').html('Draft saved: '+now);
					messageID=data;
					$.get("ajax/draft-count.php", function(data){
						$('#drafts').show();
						$('#loadDrafts').html(data);
					});
					if(firstTime)
					{
						firstTime = false;
						$('form:eq(0)').append('<input type="hidden" name="draftID" value="'+messageID+'">');
					}
					return true;
				} else { $('#notification').html('Error saving draft. See log'); }
			});
		}
		else { return false; }
	}
	var firstTime = true;
	var messageID = '';
	intervalID = setInterval(autosave, 60000);

	$('#send').mousedown(function(){ leaveWarning = false; });
	$(window).bind('beforeunload', function() {
		contentLength = (tinyMCE.get('message')) ? tinyMCE.get('message').getContent().length-200: $('#message').val().length;
		if(firstTime && contentLength>0 && leaveWarning) return 'You have entered a message but not yet sent or saved it.';
	});

	$('#saveDraft').click(function(){
		if( autosave()==false ) alert('Nothing to save. Both a subject and message required to save a draft.');
	});
	$('#loadDrafts').click(function(event){
		event.preventDefault();
		$.get("ajax/list-drafts.php", function(data){ // Call ajax/list-drafts.php to generate the list of drafts and populate dialog box
			$('#loadDraftsDialog').html('<p><a id="delete_all_drafts" href="#">Delete all drafts<a/></p>'+data);
			$("#loadDraftsDialog").dialog('open'); // open jQuery dialog modal
		});
	});
	$('.discard_draft').live('click', function(event) {
		event.preventDefault();
		if(confirm('Are you sure you want to discard this draft?'))
		{
			$.get("ajax/delete-draft.php", {id:$(this).attr('href')}, function(data){ // Call ajax/delete-draft.php to delete draft
				// possibly update notification box with message
				$('#loadDraftsDialog').dialog('close'); // close jQuery dialog modal upon success
				// need to update draft count after deleting a draft
				$.get("ajax/draft-count.php", function(data){
					$('#loadDrafts').html(data);
					if(data == 0) $('#drafts').fadeOut();
				});
			});
		}

	});
	$('#delete_all_drafts').live('click', function(event) {
		event.preventDefault();
		if(confirm('Are you sure you want to discard ALL drafts?'))
		{
			$.get("ajax/delete-draft.php", function(data){ // Call ajax/delete-draft.php to delete draft
				// possibly update notification box with message
				$('#loadDraftsDialog').dialog('close'); // close jQuery dialog modal upon success
				// need to update draft count after deleting a draft
				$.get("ajax/draft-count.php", function(data){
					$('#loadDrafts').html(data);
					if(data == 0) $('#drafts').fadeOut();
				});
			});
		}
	});

	$('.draft').live('click', function(event) {
		event.preventDefault();
		$('#loadDraftsDialog').dialog('close'); // close jQuery dialog modal upon choosing a template
		if(confirm('Loading a draft will overwrite any content currently in the message window. Continue?'))
		{
			if(tinyMCE.get('message')) tinyMCE.get('message').setProgressState(1);
			var draftID=$(this).attr('href');
			$.getJSON("ajax/get-draft.php", {id:draftID}, function(data){ // Call ajax/get-draft.php to retrieve content of draft and populate editor
				$('#subject').val(data.subject);
				$('#attachments ul').html(data.attachments);
				// switch editors based on the draft format field (data.format)
				if (!tinyMCE.get('message') && data.format=='html')
				{
					renderTinyMCE();
					$('#format').val('html');
				}
				if (tinyMCE.get('message') && data.format=='text')
				{
					tinyMCE.execCommand('mceRemoveControl', false, 'message');
					$('#format').val('text');
				}

				if(tinyMCE.get('message'))
				{
					tinyMCE.get('message').setProgressState(0);
					tinyMCE.get('message').setContent(data.message);
				}
				else $('#message').val(data.message);
				firstTime = false;
				messageID=draftID;
				$('input[name="draftID"]').remove();
				$('form:eq(0)').append('<input type="hidden" name="draftID" value="'+draftID+'">');
			});
		}
	});

	/////////////////
	// LIST PICKER //
	/////////////////

	// List list all button
	function dome()
	{
		$('#data_fields').slideUp(150);
		$('#recipient_count').html('<img src="images/ajax-loader.gif">');
		var lists = [];
		$('input[name="lists[]"]:checked').each(function(){
			lists.push( $(this).val() );
		});
		$.get("ajax/count-members.php", {list_ids:lists.toString()}, function(data){ // Call ajax/count-members.php to tally recipients
			if(data=='') data = '0 recipients';
			$('#recipient_count').html(data);
		});

		if( $('input[name="lists[]"]:checked').length == 1)
		{
			// one list selected. see if any data fields exist (getListDataFields)
			$.get("ajax/get-listfields.php", {list_id:lists.toString()}, function(data){
				if(data!='') $('#data_fields').html('The following fields may be inserted into your message (<span style="cursor:pointer;color: #00f;" id="field_note">note</span>): '+data).hide().slideDown();
			});
		}
	}
	$('#all').click(function(){
		$('#lists input[name="lists[]"]').prop('checked', ($(this).is(':checked'))?'checked':'' );
		dome();
	});
	$('#lists input[name="lists[]"]').change(function(){
		$('#all').prop('checked',$('#lists input[name="lists[]"]:not(:checked)').length==0?'checked':'');
		dome();
	});

	$('#lists label[id]').each(function(index){
		list_id = $(this).attr('id');
		var list=this;
		$.get('ajax/count-alllistmembers.php', {list_id: list_id.substring(5)}, function(data){
			if(data!='') $(list).append('('+data+')');
		});
	});
	dome();
	/////////////
	// TINYMCE //
	/////////////

	// Initialize TinyMCE
	function renderTinyMCE(){
		tinyMCE.init({
			theme : 'advanced',
			mode : 'exact',
			elements : 'message',
			content_css : "css/tinymce.css",
			height: '250',
			relative_urls : false,
			remove_script_host : false,
			setup : function(ed) {
				ed.onExecCommand.add(function(ed, cmd, ui, val) {
					if(cmd == "mceSetContent") $('#subject').val('');
				});
			},
			pdw_toggle_on : 1,
			pdw_toggle_toolbars : '2,3,4',
			plugins : 'fullpage,pdw,safari,layer,table,save,advhr,advimage,advlink,emotions,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras',
			// Theme options
			theme_advanced_buttons1 : 'newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,fontselect,fontsizeselect,formatselect,forecolor,backcolor,|,print,fullscreen,code,|,pdw_toggle',
			theme_advanced_buttons2 : 'cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,|,sub,sup',
			theme_advanced_buttons3 : 'tablecontrols,|,hr,removeformat,visualaid,|,charmap,emotions,iespell,media,advhr,|,ltr,rtl',
			theme_advanced_buttons4 : 'insertlayer,moveforward,movebackward,absolute,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,blockquote,|,insertdate,inserttime,preview',
			theme_advanced_toolbar_location : 'top',
			theme_advanced_toolbar_align : 'left',
			theme_advanced_statusbar_location : 'bottom',
			theme_advanced_resizing : true,
			theme_advanced_resize_horizontal : false
		});
	}
	$.get("ajax/check-editor.php", function(data){
		if(data == 'tinymce') renderTinyMCE();
		format = (data == 'tinymce') ? 'html':'text';
		$('form:eq(0)').append('<input id="format" name="format" type="hidden" value="'+format+'">');
	});

	$('#toggleRTE').click(function(){
		if (!tinyMCE.get('message'))
		{
			renderTinyMCE(); //tinyMCE.execCommand('mceAddControl', false, 'message');
			$('#format').val('html');
		}
		else
		{
			tinyMCE.execCommand('mceRemoveControl', false, 'message');
			$('#format').val('text');
		}
	});

	/////////////////
	// ATTACHMENTS //
	/////////////////

	// Upon making a selection, automatically upload file(s)
	$('#attachment').change(function(){
		$(this).closest('form').submit();
		$(this).val('');
		$('#attachments').prepend('<p id="attach_loader"><img src="images/ajax-loader.gif"> Attaching...</p>');
		$('#attachmentDialog').dialog('close');
	});

	// When removing an attachment from the compose page, just remove the entire parent <li> element
	$('.removeAttachment').live('click',function(event){
		event.preventDefault();
		tmp = $(this);
		$.get("ajax/delete-attachment.php", {filename: $(this).attr("href") }, function(data){
			tmp.parent().fadeOut(function(){$(this).remove()});
		});
	});

	////////////////
	// SEND LATER //
	////////////////

	$('#schedule').hide();
	$("#datepicker").datepicker({
		minDate: +0,
		changeMonth: true,
		changeYear: true,
		dateFormat: 'MM d, yy',
		dayNamesMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
	});
	$('input[name="when"]').change(function(){
		($(this).val()=='later') ? $('#schedule').slideDown():$('#schedule').slideUp();
	});
	
	// Some basic initialization steps for sending later
	if($('input[name="when"]:checked').val()=='later') $('#schedule').show();
	$.get("ajax/server-time.php",function(data){$('#serverTime').text(data)});
	setInterval(function(){$.get("ajax/server-time.php",function(data){$('#serverTime').text(data)});},60000);

	$('#navlist a').each(function(){
		if($(this).text()=='Compose') $(this).attr('id','current');
	});

	$('#content_container').after('<p id="copyright">&copy; www.notonebit.com</p>');
=======
$(function(){
	/*
	$('#showContent').click(function(){
		content = (tinyMCE.get('message')) ? tinyMCE.get('message').getContent(): $('#message').val();
		console.log(content);
		alert(content.length);
	});
	*/
	$("#loadTemplateDialog").dialog({
		width:400,
		maxWidth:800,
		height:500,
		autoOpen: false,
		modal:true
	});
	$("#saveTemplateDialog").dialog({
		autoOpen: false,
		modal:true
	});
	$('#loadDraftsDialog').dialog({
		autoOpen: false,
		modal:true,
		position: 'center',
		maxWidth:800,
		width: 600
	});
	$('#attachmentDialog').dialog({
		autoOpen: false,
		modal:true,
		position: 'center',
		width: 580,
		height: 130
	});
	$("#sendTestDialog").dialog({
		autoOpen: false
	});
	$('#fieldNoteDialog').dialog({
		autoOpen: false
	});

	$('#data_fields').hide();
	var leaveWarning = true;

	$('#addAttachment').click(function(){
		$('#attachmentDialog').dialog('open'); // open jQuery dialog modal
	});
	$('#field_note').live('click', function(){
		$('#fieldNoteDialog').dialog('open'); // open jQuery dialog modal
	});
	$('#send_test').click(function(event){
		event.preventDefault();
		$("#sendTestDialog").dialog('open'); // open jQuery dialog modal
		$('#sendTestAddress').focus().select();
	});
	$('#testSend').click(function(event){
		$('#testResult').html('<img src="images/ajax-loader.gif"> Sending test...');
		event.preventDefault();
		content = (tinyMCE.get('message')) ? tinyMCE.get('message').getContent(): $('#message').val();
		subject = $('#subject').val();
		to = $('#sendTestAddress').val();
		format = $('#format').val();
		attachments = '';
		$('input[name="attachment[]"]').each(function(){
			if($(this).val() != '') attachments += $(this).val() +',';
		});
		$.post('ajax/test-send.php', {subject:subject,message:content,to:to,attachments:attachments,format:format}, function(data) {
  			if(data == 1) $('#testResult').html('<img src="images/check.png"> Test message sent successfully').show().delay(5000).fadeOut(750);
  			if(data == 0) $('#testResult').html('<img src="images/delete.png"> Error sending message').show().delay(5000).fadeOut(750);
  			if(data == 3) $('#testResult').html('<img src="images/warning.png"> Error adding attachment').show().delay(5000).fadeOut(750);
		});
	});

	// Send message for processing
	$('#send').click(function(){
		// check subject and message for empty. check lists for minimum one selected.
		contentLength = (tinyMCE.get('message')) ? tinyMCE.get('message').getContent().length-200: $('#message').val().length;
		if($('input[name="lists[]"]:checked').length==0)
		{
			alert('You need to select at least one list to send your message to.');
			return false;
		}
		if($('#recipient_count').text() == '0 recipients')
		{
			alert('The list(s) you have chosen have no recipients. Please check your selections and try again.');
			return false;
		}
		if($('#subject').val().length==0 || contentLength==0)
		{
			alert('Please make sure you have filled out both the subject and message.');
			return false;
		}
		if( $('input[name=when]:checked').val() == "later" && $('#datepicker').val()=='' )
		{
			alert('Please enter a date to begin delivery');
			$('#datepicker').focus();
			return false;
		}
		if(!confirm('Send message?'))
		{
			return false;
		}
	});

	///////////////
	// TEMPLATES //
	///////////////

	$('#loadTemplate').click(function(){
		$("#loadTemplateDialog").dialog('open'); // open jQuery dialog modal
		$.get("ajax/list-templates.php", function(data){ // Call ajax/list-templates.php to generate the list of templates and populate dialog box
			$('#loadTemplateDialog').html(data);
		});
	});
	$('#saveTemplate').click(function(){
		$("#saveTemplateDialog").dialog('open'); // open jQuery dialog modal
	});
	$('#save').click(function(event){
		event.preventDefault(); // Stops the save button from submitting the save template form
		$('#save').after('<img id="template_waiting" src="images/ajax-loader.gif">');
		content = (tinyMCE.get('message')) ? tinyMCE.get('message').getContent(): $('#message').val();
		$.get("ajax/save-template.php", {filename:$('#saveTemplateName').val(), content:content}, function(data){
			$('#template_waiting').remove();
			if(data=='success')
			{
				$('#saveTemplateDialog').dialog('close'); // close jQuery dialog modal upon choosing a template
				$('#notification').html('Template successfully saved');
			}
			else if(data=='exists')
			{
				alert('The template name you entered already exists or is blank. Please choose another.');
			}
			else if(data=='unwritable')
			{
				alert('The templates folder cannot be written to. Please check the permissions and try again.');
			}
		});
	});
	$('.template').live('click', function(event) {
		event.preventDefault();
		$('#loadTemplateDialog').dialog('close'); // close jQuery dialog modal upon choosing a template
		if(confirm('Loading a template will overwrite any content currently in the message window. Continue?'))
		{
			if(tinyMCE.get('message')) tinyMCE.get('message').setProgressState(1);
			$.get("ajax/get-template.php", {filename:$(this).text()}, function(data){ // Call ajax/get-template.php to retrieve content of template and populate editor
				if(tinyMCE.get('message'))
				{
					tinyMCE.get('message').setProgressState(0);
					tinyMCE.get('message').setContent(data);
				}
				else $('#message').val(data);
			});
		}
	});
	$('.delete_template').live('click', function(event) {
		event.preventDefault();
		if(confirm('Are you sure you want to delete this template?'))
		{
			$.get("ajax/delete-template.php", {filename:$(this).attr('href')}, function(data){ // Call ajax/delete-template.php to delete template
				// possibly update notification box with message
				$('#loadTemplateDialog').dialog('close'); // close jQuery dialog modal upon success
			});
		}
	});

	///////////////////////
	// DRAFTS & AUTOSAVE //
	///////////////////////

	function autosave(){
		contentLength = (tinyMCE.get('message')) ? tinyMCE.get('message').getContent().length-200: $('#message').val().length;
		if(contentLength>0 && $('#subject').val().length > 0) // Only save a draft if the subject and message are not empty
		{
			attachments = ($('input[name="attachment[]"]').length > 1) ? $('#attachments ul').html():'';
			content = (tinyMCE.get('message')) ? tinyMCE.get('message').getContent(): $('#message').val();
			$.get("ajax/autosave.php", {firstTime:firstTime, id:messageID, subject:$('#subject').val(), content:content, format:$('#format').val(), attachments:attachments}, function(data){
				if(data != 0)
				{
					var now = new Date();
					$('#notification').html('Draft saved: '+now);
					messageID=data;
					$.get("ajax/draft-count.php", function(data){
						$('#drafts').show();
						$('#loadDrafts').html(data);
					});
					if(firstTime)
					{
						firstTime = false;
						$('form:eq(0)').append('<input type="hidden" name="draftID" value="'+messageID+'">');
					}
					return true;
				} else { $('#notification').html('Error saving draft. See log'); }
			});
		}
		else { return false; }
	}
	var firstTime = true;
	var messageID = '';
	intervalID = setInterval(autosave, 60000);

	$('#send').mousedown(function(){ leaveWarning = false; });
	$(window).bind('beforeunload', function() {
		contentLength = (tinyMCE.get('message')) ? tinyMCE.get('message').getContent().length-200: $('#message').val().length;
		if(firstTime && contentLength>0 && leaveWarning) return 'You have entered a message but not yet sent or saved it.';
	});

	$('#saveDraft').click(function(){
		if( autosave()==false ) alert('Nothing to save. Both a subject and message required to save a draft.');
	});
	$('#loadDrafts').click(function(event){
		event.preventDefault();
		$.get("ajax/list-drafts.php", function(data){ // Call ajax/list-drafts.php to generate the list of drafts and populate dialog box
			$('#loadDraftsDialog').html('<p><a id="delete_all_drafts" href="#">Delete all drafts<a/></p>'+data);
			$("#loadDraftsDialog").dialog('open'); // open jQuery dialog modal
		});
	});
	$('.discard_draft').live('click', function(event) {
		event.preventDefault();
		if(confirm('Are you sure you want to discard this draft?'))
		{
			$.get("ajax/delete-draft.php", {id:$(this).attr('href')}, function(data){ // Call ajax/delete-draft.php to delete draft
				// possibly update notification box with message
				$('#loadDraftsDialog').dialog('close'); // close jQuery dialog modal upon success
				// need to update draft count after deleting a draft
				$.get("ajax/draft-count.php", function(data){
					$('#loadDrafts').html(data);
					if(data == 0) $('#drafts').fadeOut();
				});
			});
		}

	});
	$('#delete_all_drafts').live('click', function(event) {
		event.preventDefault();
		if(confirm('Are you sure you want to discard ALL drafts?'))
		{
			$.get("ajax/delete-draft.php", function(data){ // Call ajax/delete-draft.php to delete draft
				// possibly update notification box with message
				$('#loadDraftsDialog').dialog('close'); // close jQuery dialog modal upon success
				// need to update draft count after deleting a draft
				$.get("ajax/draft-count.php", function(data){
					$('#loadDrafts').html(data);
					if(data == 0) $('#drafts').fadeOut();
				});
			});
		}
	});

	$('.draft').live('click', function(event) {
		event.preventDefault();
		$('#loadDraftsDialog').dialog('close'); // close jQuery dialog modal upon choosing a template
		if(confirm('Loading a draft will overwrite any content currently in the message window. Continue?'))
		{
			if(tinyMCE.get('message')) tinyMCE.get('message').setProgressState(1);
			var draftID=$(this).attr('href');
			$.getJSON("ajax/get-draft.php", {id:draftID}, function(data){ // Call ajax/get-draft.php to retrieve content of draft and populate editor
				$('#subject').val(data.subject);
				$('#attachments ul').html(data.attachments);
				// switch editors based on the draft format field (data.format)
				if (!tinyMCE.get('message') && data.format=='html')
				{
					renderTinyMCE();
					$('#format').val('html');
				}
				if (tinyMCE.get('message') && data.format=='text')
				{
					tinyMCE.execCommand('mceRemoveControl', false, 'message');
					$('#format').val('text');
				}

				if(tinyMCE.get('message'))
				{
					tinyMCE.get('message').setProgressState(0);
					tinyMCE.get('message').setContent(data.message);
				}
				else $('#message').val(data.message);
				firstTime = false;
				messageID=draftID;
				$('input[name="draftID"]').remove();
				$('form:eq(0)').append('<input type="hidden" name="draftID" value="'+draftID+'">');
			});
		}
	});

	/////////////////
	// LIST PICKER //
	/////////////////

	// List list all button
	function dome()
	{
		$('#data_fields').slideUp(150);
		$('#recipient_count').html('<img src="images/ajax-loader.gif">');
		var lists = [];
		$('input[name="lists[]"]:checked').each(function(){
			lists.push( $(this).val() );
		});
		$.get("ajax/count-members.php", {list_ids:lists.toString()}, function(data){ // Call ajax/count-members.php to tally recipients
			if(data=='') data = '0 recipients';
			$('#recipient_count').html(data);
		});

		if( $('input[name="lists[]"]:checked').length == 1)
		{
			// one list selected. see if any data fields exist (getListDataFields)
			$.get("ajax/get-listfields.php", {list_id:lists.toString()}, function(data){
				if(data!='') $('#data_fields').html('The following fields may be inserted into your message (<span style="cursor:pointer;color: #00f;" id="field_note">note</span>): '+data).hide().slideDown();
			});
		}
	}
	$('#all').click(function(){
		$('#lists input[name="lists[]"]').prop('checked', ($(this).is(':checked'))?'checked':'' );
		dome();
	});
	$('#lists input[name="lists[]"]').change(function(){
		$('#all').prop('checked',$('#lists input[name="lists[]"]:not(:checked)').length==0?'checked':'');
		dome();
	});

	$('#lists label[id]').each(function(index){
		list_id = $(this).attr('id');
		var list=this;
		$.get('ajax/count-alllistmembers.php', {list_id: list_id.substring(5)}, function(data){
			if(data!='') $(list).append('('+data+')');
		});
	});
	dome();
	/////////////
	// TINYMCE //
	/////////////

	// Initialize TinyMCE
	function renderTinyMCE(){
		tinyMCE.init({
			theme : 'advanced',
			mode : 'exact',
			elements : 'message',
			content_css : "css/tinymce.css",
			height: '250',
			relative_urls : false,
			remove_script_host : false,
			setup : function(ed) {
				ed.onExecCommand.add(function(ed, cmd, ui, val) {
					if(cmd == "mceSetContent") $('#subject').val('');
				});
			},
			pdw_toggle_on : 1,
			pdw_toggle_toolbars : '2,3,4',
			plugins : 'fullpage,pdw,safari,layer,table,save,advhr,advimage,advlink,emotions,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras',
			// Theme options
			theme_advanced_buttons1 : 'newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,fontselect,fontsizeselect,formatselect,forecolor,backcolor,|,print,fullscreen,code,|,pdw_toggle',
			theme_advanced_buttons2 : 'cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,|,sub,sup',
			theme_advanced_buttons3 : 'tablecontrols,|,hr,removeformat,visualaid,|,charmap,emotions,iespell,media,advhr,|,ltr,rtl',
			theme_advanced_buttons4 : 'insertlayer,moveforward,movebackward,absolute,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,blockquote,|,insertdate,inserttime,preview',
			theme_advanced_toolbar_location : 'top',
			theme_advanced_toolbar_align : 'left',
			theme_advanced_statusbar_location : 'bottom',
			theme_advanced_resizing : true,
			theme_advanced_resize_horizontal : false
		});
	}
	$.get("ajax/check-editor.php", function(data){
		if(data == 'tinymce') renderTinyMCE();
		format = (data == 'tinymce') ? 'html':'text';
		$('form:eq(0)').append('<input id="format" name="format" type="hidden" value="'+format+'">');
	});

	$('#toggleRTE').click(function(){
		if (!tinyMCE.get('message'))
		{
			renderTinyMCE(); //tinyMCE.execCommand('mceAddControl', false, 'message');
			$('#format').val('html');
		}
		else
		{
			tinyMCE.execCommand('mceRemoveControl', false, 'message');
			$('#format').val('text');
		}
	});

	/////////////////
	// ATTACHMENTS //
	/////////////////

	// Upon making a selection, automatically upload file(s)
	$('#attachment').change(function(){
		$(this).closest('form').submit();
		$(this).val('');
		$('#attachments').prepend('<p id="attach_loader"><img src="images/ajax-loader.gif"> Attaching...</p>');
		$('#attachmentDialog').dialog('close');
	});

	// When removing an attachment from the compose page, just remove the entire parent <li> element
	$('.removeAttachment').live('click',function(event){
		event.preventDefault();
		tmp = $(this);
		$.get("ajax/delete-attachment.php", {filename: $(this).attr("href") }, function(data){
			tmp.parent().fadeOut(function(){$(this).remove()});
		});
	});

	////////////////
	// SEND LATER //
	////////////////

	$('#schedule').hide();
	$("#datepicker").datepicker({
		minDate: +0,
		changeMonth: true,
		changeYear: true,
		dateFormat: 'MM d, yy',
		dayNamesMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
	});
	$('input[name="when"]').change(function(){
		($(this).val()=='later') ? $('#schedule').slideDown():$('#schedule').slideUp();
	});
	
	// Some basic initialization steps for sending later
	if($('input[name="when"]:checked').val()=='later') $('#schedule').show();
	$.get("ajax/server-time.php",function(data){$('#serverTime').text(data)});
	setInterval(function(){$.get("ajax/server-time.php",function(data){$('#serverTime').text(data)});},60000);

	$('#navlist a').each(function(){
		if($(this).text()=='Compose') $(this).attr('id','current');
	});

	$('#content_container').after('<p id="copyright">&copy; www.notonebit.com</p>');
>>>>>>> b59280b13ce027f89f28f2d12e991e62fe2dd1bd
});