<<<<<<< HEAD
$(function(){

	var stickerTop = parseInt($('#member_data').offset().top);
	var stickerLeft = parseInt($('#member_data').offset().left);
	$(window).scroll(function()
	{
	    $("#member_data").css((parseInt($(window).scrollTop())+parseInt($("#member_data").css('margin-top')) > stickerTop) ? {position:'fixed',top:'0px',left:stickerLeft} : {position:'relative',left:'0px'});
	});
	/////////////////
	// LIST PICKER //
	/////////////////
	// On clicking any list checkbox, send a comma delimited list to retrieve the members of the list(s)
	function dome()
	{
		$('#member_list').html('<img src="images/ajax-loader.gif">');
		selected = '';
		$('#lists input[name="lists[]"]:checked').each(function(index) {
			if(index>0) selected+=',';
			selected+=$(this).val();
		});
		//send a call to an ajax script with the 'selected' list ids
		$.get('ajax/list-members.php', {list_ids: selected}, function(data){
			$('#member_list').html(data).hide().fadeIn();
		});
		$.get('ajax/count-members.php', {list_ids: selected}, function(data){
			$('#member_count').html(data).hide().fadeIn();
		});	
	}
	$('#all').click(function(){
		$('#lists input[name="lists[]"]').prop('checked', ($(this).is(':checked'))?'checked':'' );
		dome();
	});
	$('#lists input[name="lists[]"]').change(function(){
		$('#all').prop('checked',$('#lists input[name="lists[]"]:not(:checked)').length==0?'checked':'');
		dome();
	});
	$('#lists label[id]').each(function(index){
		list_id = $(this).attr('id');
		var list=this;
		$.get('ajax/count-alllistmembers.php', {list_id: list_id.substring(5)}, function(data){
			if(data!='') $(list).append('<span>('+data+')</span>');
		});
	});
	dome();

	///////////////////
	// IMPORT DIALOG //
	///////////////////
	$('.openImport').click(function(event){
		event.preventDefault();
		if($(this).attr('href') != '#') $('#addresses').val($(this).attr('href'));
		$("#importDialog").dialog('open'); // open jQuery dialog modal

	});
	$("#importDialog").dialog({
		width:750,
		autoOpen: false,
		modal:true
	});
	$('#import').click(function(event){
		if($('input:[name=lists[]]:checked').length ==0)
		{
			event.preventDefault();
			alert('You must select at least one list to import addresses to.');
		}
	});	

	/////////////////
	// MEMBER DATA //
	/////////////////
	$('.delete').live('click',function(event){
		event.preventDefault();
		if(confirm('Are you sure you want to delete '+$(this).attr('href')+'? Deleting a member will also remove them from all lists they belong to.'))
		{
			$.get('ajax/delete-member.php', {address: $(this).attr('href')}, function(data){
				message = (data) ? 'Member successfully deleted':'Unable to delete member';
				message += ' <span id="notification_close"><img src="images/close.png" alt="close" style="vertical-align:middle"></span>';
				$('#notification').html( message ).hide().slideDown();
				$('#notification_close').click(function(){ $('#notification').slideUp() });
				$.get('ajax/list-members.php', {list_ids: selected}, function(data){
					$('#member_list').fadeOut().html(data).fadeIn();
					$('#member_data').fadeOut();
					$('#lists label:gt(0)').each(function(index){
						list_id = $(this).attr('id');
						var list=this;
						$('input',list).attr('checked','');
						$.get('ajax/count-alllistmembers.php', {list_id: list_id.substring(5)}, function(data){
							$('span',list).replaceWith('');
							if(data!='') $(list).append('<span>('+data+')</span>');
						});
					});
				});
			});
		}
	});

	$('.removemember').live('click',function(event){
		event.preventDefault();
		list_id = $(this).attr('href');
		address = $(this).attr('id');
		$.get('ajax/get-listname.php', {list_id: list_id}, function(data){
			if(confirm('Are you sure you want to remove '+address+' from '+data+'?'))
			{
				$.get('ajax/delete-listmember.php', {address:address,list:list_id}, function(data){
					$.get('ajax/cleanup-members.php',function(data){
// slideup the list item and display text to refresh the page
// checkout code above for delete within $.get('ajax/delete-member.php to see if we can reuse to update the page
					});
				});
			}
		});
	});

	$('#navlist a').each(function(){
		if($(this).text()=='Members') $(this).attr('id','current');
	});

	function memberInfo($address)
	{
		$.getJSON('ajax/member-info.php', {address: $address}, function(data){
			$('#member_address').html('<img style="vertical-align:top" src="images/envelope.png"> '+data.address);
			$('#member_bounces').html('Bounces: '+data.bounces);
			$('#member_lists').html('Lists:<ul>'+data.lists+'</ul>');
			$('#member_ban').attr('href',$address);
			$('#member_delete').attr('href',$address);
			$('#profile_import').attr('href',$address);
			$('#member_data').fadeIn();
		});
	}

	$('#member_data').hide();
	$('.member').live('click',function(event){
		memberInfo($(this).attr('id'));
	});

	$('#member_lists span').live('click',function(event){
		addresslist = $(this).attr('id').split('-');
		newstatus = ($(this).attr('class')==0) ? '1':'0';
		tmp = $(this);
		$.get('ajax/toggle-status.php', {address:addresslist[0],list:addresslist[1],newstatus:newstatus}, function(data){
			//need to cache reference to span outside of function call so we can used within this anonymous function since "this" is relative
			newsrc = (newstatus==1) ? 'images/bullet-green.png':'images/bullet-red.png';
			$(' > img',tmp).attr('src',newsrc);
			$(tmp).removeClass().addClass(newstatus);
		});
	});

	////////////////////////////////////////////////////////
	// DEADBEAT (BOUNCE) CLEANUP DIALOG & Bounce CHECKING //
	////////////////////////////////////////////////////////
	$('#cleanup').click(function(event){
		event.preventDefault();
		$("#cleanupDialog").dialog('open'); // open jQuery dialog modal
	});
	$("#cleanupDialog").dialog({
		width:410,
		height: 200,
		autoOpen: false,
		modal:true
	});
	$('input[name="bounceSubmit"]').click(function(event){
		event.preventDefault();
		if(confirm('Are you sure you want to remove these members?'))
		{
			var bounceThreshold = parseInt($('input[name="bounceCleanup"]').val());
			$('input[name="bounceSubmit"]').after('<img src="images/ajax-loader.gif" style="margin-left:4px">');
			if(!isNaN( bounceThreshold ))
			{
				$.get('ajax/cleanup-deadbeats.php', {threshold: bounceThreshold }, function(data){
					$('#cleanupDialog img').remove();
					$('input[name="bounceSubmit"]').after('<p>Deadbeat cleanup complete. <a href="members.php">Reload page</a> to refresh member list.</p>');
				});
			}
			else
			{
				alert('Please enter an integer.');
				$('input[name="bounceCleanup"]').select();
			}
		}
	});
	// Do bounce check
	$.get('ajax/check-bounces.php');

	/////////////////
	// AUTOSUGGEST //
	/////////////////
	var cache = {};
	$("#query").autocomplete({
		minLength: 2,
		select: function(event, ui) {
			// console.log(ui.item.id);
			// upon selecting something, bring up info for the user (lists, join, etc)
			$.getJSON('ajax/member-info.php', {address: ui.item.id}, function(data){
				memberInfo(ui.item.id);
			});
		},
		source: function(request, response) {
			if ( request.term in cache ) {
				response( cache[ request.term ] );
				return;
			}		
			$.ajax({
				url: "ajax/member-search.php",
				dataType: "json",
				data: request,
				success: function( data ) {
					cache[ request.term ] = data;
					response( data );
				}
			});
		}
	});
=======
$(function(){

	var stickerTop = parseInt($('#member_data').offset().top);
	var stickerLeft = parseInt($('#member_data').offset().left);
	$(window).scroll(function()
	{
	    $("#member_data").css((parseInt($(window).scrollTop())+parseInt($("#member_data").css('margin-top')) > stickerTop) ? {position:'fixed',top:'0px',left:stickerLeft} : {position:'relative',left:'0px'});
	});
	/////////////////
	// LIST PICKER //
	/////////////////
	// On clicking any list checkbox, send a comma delimited list to retrieve the members of the list(s)
	function dome()
	{
		$('#member_list').html('<img src="images/ajax-loader.gif">');
		selected = '';
		$('#lists input[name="lists[]"]:checked').each(function(index) {
			if(index>0) selected+=',';
			selected+=$(this).val();
		});
		//send a call to an ajax script with the 'selected' list ids
		$.get('ajax/list-members.php', {list_ids: selected}, function(data){
			$('#member_list').html(data).hide().fadeIn();
		});
		$.get('ajax/count-members.php', {list_ids: selected}, function(data){
			$('#member_count').html(data).hide().fadeIn();
		});	
	}
	$('#all').click(function(){
		$('#lists input[name="lists[]"]').prop('checked', ($(this).is(':checked'))?'checked':'' );
		dome();
	});
	$('#lists input[name="lists[]"]').change(function(){
		$('#all').prop('checked',$('#lists input[name="lists[]"]:not(:checked)').length==0?'checked':'');
		dome();
	});
	$('#lists label[id]').each(function(index){
		list_id = $(this).attr('id');
		var list=this;
		$.get('ajax/count-alllistmembers.php', {list_id: list_id.substring(5)}, function(data){
			if(data!='') $(list).append('<span>('+data+')</span>');
		});
	});
	dome();

	///////////////////
	// IMPORT DIALOG //
	///////////////////
	$('.openImport').click(function(event){
		event.preventDefault();
		if($(this).attr('href') != '#') $('#addresses').val($(this).attr('href'));
		$("#importDialog").dialog('open'); // open jQuery dialog modal

	});
	$("#importDialog").dialog({
		width:750,
		autoOpen: false,
		modal:true
	});
	$('#import').click(function(event){
		if($('input:[name=lists[]]:checked').length ==0)
		{
			event.preventDefault();
			alert('You must select at least one list to import addresses to.');
		}
	});	

	/////////////////
	// MEMBER DATA //
	/////////////////
	$('.delete').live('click',function(event){
		event.preventDefault();
		if(confirm('Are you sure you want to delete '+$(this).attr('href')+'? Deleting a member will also remove them from all lists they belong to.'))
		{
			$.get('ajax/delete-member.php', {address: $(this).attr('href')}, function(data){
				message = (data) ? 'Member successfully deleted':'Unable to delete member';
				message += ' <span id="notification_close"><img src="images/close.png" alt="close" style="vertical-align:middle"></span>';
				$('#notification').html( message ).hide().slideDown();
				$('#notification_close').click(function(){ $('#notification').slideUp() });
				$.get('ajax/list-members.php', {list_ids: selected}, function(data){
					$('#member_list').fadeOut().html(data).fadeIn();
					$('#member_data').fadeOut();
					$('#lists label:gt(0)').each(function(index){
						list_id = $(this).attr('id');
						var list=this;
						$('input',list).attr('checked','');
						$.get('ajax/count-alllistmembers.php', {list_id: list_id.substring(5)}, function(data){
							$('span',list).replaceWith('');
							if(data!='') $(list).append('<span>('+data+')</span>');
						});
					});
				});
			});
		}
	});

	$('.removemember').live('click',function(event){
		event.preventDefault();
		list_id = $(this).attr('href');
		address = $(this).attr('id');
		$.get('ajax/get-listname.php', {list_id: list_id}, function(data){
			if(confirm('Are you sure you want to remove '+address+' from '+data+'?'))
			{
				$.get('ajax/delete-listmember.php', {address:address,list:list_id}, function(data){
					$.get('ajax/cleanup-members.php',function(data){
// slideup the list item and display text to refresh the page
// checkout code above for delete within $.get('ajax/delete-member.php to see if we can reuse to update the page
					});
				});
			}
		});
	});

	$('#navlist a').each(function(){
		if($(this).text()=='Members') $(this).attr('id','current');
	});

	function memberInfo($address)
	{
		$.getJSON('ajax/member-info.php', {address: $address}, function(data){
			$('#member_address').html('<img style="vertical-align:top" src="images/envelope.png"> '+data.address);
			$('#member_bounces').html('Bounces: '+data.bounces);
			$('#member_lists').html('Lists:<ul>'+data.lists+'</ul>');
			$('#member_ban').attr('href',$address);
			$('#member_delete').attr('href',$address);
			$('#profile_import').attr('href',$address);
			$('#member_data').fadeIn();
		});
	}

	$('#member_data').hide();
	$('.member').live('click',function(event){
		memberInfo($(this).attr('id'));
	});

	$('#member_lists span').live('click',function(event){
		addresslist = $(this).attr('id').split('-');
		newstatus = ($(this).attr('class')==0) ? '1':'0';
		tmp = $(this);
		$.get('ajax/toggle-status.php', {address:addresslist[0],list:addresslist[1],newstatus:newstatus}, function(data){
			//need to cache reference to span outside of function call so we can used within this anonymous function since "this" is relative
			newsrc = (newstatus==1) ? 'images/bullet-green.png':'images/bullet-red.png';
			$(' > img',tmp).attr('src',newsrc);
			$(tmp).removeClass().addClass(newstatus);
		});
	});

	////////////////////////////////////////////////////////
	// DEADBEAT (BOUNCE) CLEANUP DIALOG & Bounce CHECKING //
	////////////////////////////////////////////////////////
	$('#cleanup').click(function(event){
		event.preventDefault();
		$("#cleanupDialog").dialog('open'); // open jQuery dialog modal
	});
	$("#cleanupDialog").dialog({
		width:410,
		height: 200,
		autoOpen: false,
		modal:true
	});
	$('input[name="bounceSubmit"]').click(function(event){
		event.preventDefault();
		if(confirm('Are you sure you want to remove these members?'))
		{
			var bounceThreshold = parseInt($('input[name="bounceCleanup"]').val());
			$('input[name="bounceSubmit"]').after('<img src="images/ajax-loader.gif" style="margin-left:4px">');
			if(!isNaN( bounceThreshold ))
			{
				$.get('ajax/cleanup-deadbeats.php', {threshold: bounceThreshold }, function(data){
					$('#cleanupDialog img').remove();
					$('input[name="bounceSubmit"]').after('<p>Deadbeat cleanup complete. <a href="members.php">Reload page</a> to refresh member list.</p>');
				});
			}
			else
			{
				alert('Please enter an integer.');
				$('input[name="bounceCleanup"]').select();
			}
		}
	});
	// Do bounce check
	$.get('ajax/check-bounces.php');

	/////////////////
	// AUTOSUGGEST //
	/////////////////
	var cache = {};
	$("#query").autocomplete({
		minLength: 2,
		select: function(event, ui) {
			// console.log(ui.item.id);
			// upon selecting something, bring up info for the user (lists, join, etc)
			$.getJSON('ajax/member-info.php', {address: ui.item.id}, function(data){
				memberInfo(ui.item.id);
			});
		},
		source: function(request, response) {
			if ( request.term in cache ) {
				response( cache[ request.term ] );
				return;
			}		
			$.ajax({
				url: "ajax/member-search.php",
				dataType: "json",
				data: request,
				success: function( data ) {
					cache[ request.term ] = data;
					response( data );
				}
			});
		}
	});
>>>>>>> b59280b13ce027f89f28f2d12e991e62fe2dd1bd
});