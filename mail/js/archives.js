<<<<<<< HEAD
$(function(){
	var message_id = '';
	var sendInterval;
	var timer = 30;
	$("#processDialog").dialog({
		autoOpen: false,
		modal:true,
		close: function(event,ui){ 
			clearInterval(timerInterval);
			location.reload();
		}
	});
	$("#viewDialog").dialog({
		width:750,
		autoOpen: false,
		modal:true,
		height: 600
	});
	$("#listDialog").dialog({
		width:300,
		autoOpen: false,
		modal:true
	});
	$('.message').live('click',function(event){
		event.preventDefault();
		message_id = $(this).attr('href');
		$.getJSON('ajax/view-message.php', {id: message_id}, function(data){
			$('#subject').text(data.subject);
			$('#message').replaceWith('<div id="message">'+data.message+'</div>');
			$("#viewDialog").dialog('open'); // open jQuery dialog modal
		});
	});
	$('.delete').live('click',function(event){
		event.preventDefault();

//alert( $('input[name="paginator_ipp"]').val() +' '+  $('input[name="paginator_page"]').val() +' '+  $('input[name="paginator_total"]').val() );

		if(confirm('Are you sure you want to permanently delete this message?'))
		{
			var item = $(this);
			$.get('ajax/delete-attachments.php', {id: $(this).attr('href')});
			$.get('ajax/delete-message.php', {id: $(this).attr('href')}, function(data){
				$(item).parents('tr').fadeOut(function(){
					$(this).remove();
					$('tr').css('background','#fff');
					$('tr:odd').css('background','#f0f0f0');
				});
				message = (data) ? 'Message successfully deleted':'Unable to delete message';
				message += '<span id="notification_close"><img src="images/close.png" style="vertical-align:middle" alt="close"></span>';
				$('#notification').html( message ).hide().slideDown();
				$('#notification_close').click(function(){ $('#notification').slideUp() });
				$('#archive_count').html($('#archive_count').html()-1);
			});
		}

	});
	$('.lists').live('click',function(event){
		event.preventDefault();
		message_id = $(this).attr('href');
		$("#listDialog").dialog('open'); // open jQuery dialog modal

		$.get('ajax/view-lists.php', {id: message_id}, function(data){
			$("#listDialog").html(data).dialog('open'); // open jQuery dialog modal
		});

	});
	$('.prog').progressbar().each(function(index){
		var pct = parseInt($(this).text());
		$(this).progressbar("option", "value", pct);
		$(this).children().css({'font-size':'0','color':'transparent'});
		if(pct == 100) $('div',this).css('background-image','url(images/archive_progress.png)');
	});

	$('#resend').click(function(event){
		$(this).attr('href','compose.php?id='+message_id);
	});

	$('#navlist a').each(function(){
		if($(this).text()=='Archives') $(this).attr('id','current');
	});

	$('.stop').click(function(event){ //$(this).attr('id').substring(5)
		if(confirm('Are you sure you want to stop the delivery of the rest of these messages?\nStopping delivery can\'t be undone and will show the message as 100% delivered.'))
		{

		} else event.preventDefault();
	});
	$('.send').click(function(event){ //$(this).attr('id').substring(14)
		event.preventDefault();
		$("#processDialog").dialog('open'); // open jQuery dialog modal
		var message_id = $(this).attr('id').substring(14);

		//set a countdown timer and clear interval. when the timeout hits, call an ajax script to process the queue
		timerInterval = setInterval(function(){
			$('#countdown').text(timer);
			timer--;
			if(timer<0)
			{
				$.get("ajax/manual-delivery.php", { p: message_id }, function(data){
					$('#remaining').text(data);
					if(data==0) clearAndClose();
				});
				timer=30;
				$('#countdown_message').hide().html('Queue processed').fadeIn(2000).fadeOut(4000);
			}
		}, 1000);
	});

	$('#processDialog a').click(clearAndClose);

	function clearAndClose()
	{
		clearInterval(timerInterval);
		$('#processDialog').dialog( "close" ); 
	}

	$('#force').click(function(){ timer=0; });

	$('tr:odd').css('background','#f0f0f0');

	var cache = {};
	$("#query").autocomplete({
		minLength: 5,
		select: function(event, ui) {
			 //alert(ui.item.id);
			$.getJSON('ajax/view-message.php', {id: ui.item.id}, function(data){
				$('#subject').text(data.subject);
				$('#message').replaceWith('<div id="message">'+data.message+'</div>');
				$("#viewDialog").dialog('open'); // open jQuery dialog modal
			});
		},
		source: function(request, response) {
			if ( request.term in cache ) {
				response( cache[ request.term ] );
				return;
			}		
			$.ajax({
				url: "ajax/archive-search.php",
				dataType: "json",
				data: request,
				success: function( data ) {
					cache[ request.term ] = data;
					response( data );
				}
			});
		}
	});

=======
$(function(){
	var message_id = '';
	var sendInterval;
	var timer = 30;
	$("#processDialog").dialog({
		autoOpen: false,
		modal:true,
		close: function(event,ui){ 
			clearInterval(timerInterval);
			location.reload();
		}
	});
	$("#viewDialog").dialog({
		width:750,
		autoOpen: false,
		modal:true,
		height: 600
	});
	$("#listDialog").dialog({
		width:300,
		autoOpen: false,
		modal:true
	});
	$('.message').live('click',function(event){
		event.preventDefault();
		message_id = $(this).attr('href');
		$.getJSON('ajax/view-message.php', {id: message_id}, function(data){
			$('#subject').text(data.subject);
			$('#message').replaceWith('<div id="message">'+data.message+'</div>');
			$("#viewDialog").dialog('open'); // open jQuery dialog modal
		});
	});
	$('.delete').live('click',function(event){
		event.preventDefault();

//alert( $('input[name="paginator_ipp"]').val() +' '+  $('input[name="paginator_page"]').val() +' '+  $('input[name="paginator_total"]').val() );

		if(confirm('Are you sure you want to permanently delete this message?'))
		{
			var item = $(this);
			$.get('ajax/delete-attachments.php', {id: $(this).attr('href')});
			$.get('ajax/delete-message.php', {id: $(this).attr('href')}, function(data){
				$(item).parents('tr').fadeOut(function(){
					$(this).remove();
					$('tr').css('background','#fff');
					$('tr:odd').css('background','#f0f0f0');
				});
				message = (data) ? 'Message successfully deleted':'Unable to delete message';
				message += '<span id="notification_close"><img src="images/close.png" style="vertical-align:middle" alt="close"></span>';
				$('#notification').html( message ).hide().slideDown();
				$('#notification_close').click(function(){ $('#notification').slideUp() });
				$('#archive_count').html($('#archive_count').html()-1);
			});
		}

	});
	$('.lists').live('click',function(event){
		event.preventDefault();
		message_id = $(this).attr('href');
		$("#listDialog").dialog('open'); // open jQuery dialog modal

		$.get('ajax/view-lists.php', {id: message_id}, function(data){
			$("#listDialog").html(data).dialog('open'); // open jQuery dialog modal
		});

	});
	$('.prog').progressbar().each(function(index){
		var pct = parseInt($(this).text());
		$(this).progressbar("option", "value", pct);
		$(this).children().css({'font-size':'0','color':'transparent'});
		if(pct == 100) $('div',this).css('background-image','url(images/archive_progress.png)');
	});

	$('#resend').click(function(event){
		$(this).attr('href','compose.php?id='+message_id);
	});

	$('#navlist a').each(function(){
		if($(this).text()=='Archives') $(this).attr('id','current');
	});

	$('.stop').click(function(event){ //$(this).attr('id').substring(5)
		if(confirm('Are you sure you want to stop the delivery of the rest of these messages?\nStopping delivery can\'t be undone and will show the message as 100% delivered.'))
		{

		} else event.preventDefault();
	});
	$('.send').click(function(event){ //$(this).attr('id').substring(14)
		event.preventDefault();
		$("#processDialog").dialog('open'); // open jQuery dialog modal
		var message_id = $(this).attr('id').substring(14);

		//set a countdown timer and clear interval. when the timeout hits, call an ajax script to process the queue
		timerInterval = setInterval(function(){
			$('#countdown').text(timer);
			timer--;
			if(timer<0)
			{
				$.get("ajax/manual-delivery.php", { p: message_id }, function(data){
					$('#remaining').text(data);
					if(data==0) clearAndClose();
				});
				timer=30;
				$('#countdown_message').hide().html('Queue processed').fadeIn(2000).fadeOut(4000);
			}
		}, 1000);
	});

	$('#processDialog a').click(clearAndClose);

	function clearAndClose()
	{
		clearInterval(timerInterval);
		$('#processDialog').dialog( "close" ); 
	}

	$('#force').click(function(){ timer=0; });

	$('tr:odd').css('background','#f0f0f0');

	var cache = {};
	$("#query").autocomplete({
		minLength: 5,
		select: function(event, ui) {
			 //alert(ui.item.id);
			$.getJSON('ajax/view-message.php', {id: ui.item.id}, function(data){
				$('#subject').text(data.subject);
				$('#message').replaceWith('<div id="message">'+data.message+'</div>');
				$("#viewDialog").dialog('open'); // open jQuery dialog modal
			});
		},
		source: function(request, response) {
			if ( request.term in cache ) {
				response( cache[ request.term ] );
				return;
			}		
			$.ajax({
				url: "ajax/archive-search.php",
				dataType: "json",
				data: request,
				success: function( data ) {
					cache[ request.term ] = data;
					response( data );
				}
			});
		}
	});

>>>>>>> b59280b13ce027f89f28f2d12e991e62fe2dd1bd
});