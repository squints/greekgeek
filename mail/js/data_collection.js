<<<<<<< HEAD
function genID()
{
	return Math.round(Math.random()*100000000);
}
$(function(){
	$('.collector a').click(function(){return confirm("Are you sure you want to delete this data field? Removing a data field will also remove it from any lists that use it.")});

	// give unique id's to every generated radio, checkbox, and drop down element
	// then tie the state of being selected/checked to the id

	var textItems=0,textareaItems=0,radioItems=0,checkboxItems=0,dropdownItems=0,option=0;
	$('#item').change(function()
		{
			option = 0;
			if($(this).val()=='text')
			{
				newitem = '<div id="text-'+textItems+'"><fieldset name="text"><legend>Text field</legend>';
				newitem += 'Name <input name="form-text-'+textItems+'" type="text" /> ';
				newitem += 'Required?<input name="form-text-'+textItems+'-required" type="checkbox" /> ';
				newitem += '<a class="removeitem" href="javascript:void(0)"><img src="images/delete.png" alt="delete"></a></fieldset></div>';
				$('#fields').append(newitem);
				$('#text-'+textItems).css('display','none').fadeIn();
				textItems++;
				$(this).attr('selectedIndex',0);
			}
			if($(this).val()=='textarea')
			{
				newitem = '<div id="textarea-'+textareaItems+'"><fieldset name="textarea"><legend>Text Area field</legend>';
				newitem += 'Name <input name="form-textarea-'+textareaItems+'" type="text" /> ';
				newitem += 'Required?<input name="form-textarea-'+textareaItems+'-required" type="checkbox" /> ';
				newitem += '<a class="removeitem" href="javascript:void(0)"><img src="images/delete.png" alt="delete"></a></fieldset></div>';
				$('#fields').append(newitem);
				$('#textarea-'+textareaItems).css('display','none').fadeIn();
				textareaItems++;
				$(this).attr('selectedIndex',0);
			}
			if($(this).val()=='radio')
			{
				id=genID();
				newitem = '<div id="radio-'+radioItems+'"><fieldset name="radio"><legend>Radio Button field</legend>';
				newitem += 'Name <input name="form-radio-'+radioItems+'" type="text" /> ';
				newitem += 'Required?<input name="form-radio-'+radioItems+'-required" type="checkbox" /> ';
				newitem += '<a class="removeitem" href="javascript:void(0)"><img src="images/delete.png" alt="delete"></a>';
				newitem += '<ul><li><input name="form-radio-'+radioItems+'-select" value="'+id+'" type="radio" /> ';
				newitem += '<input id="'+id+'" name="form-radio-'+radioItems+'-option['+id+']" type="text" /></li>';
				newitem += '<li><a class="addradiooption" id="radio'+radioItems+'" href="javascript:void(0)">Add option</a></li></ul></fieldset></div>';
				$('#fields').append(newitem);
				$('#radio-'+radioItems).css('display','none').fadeIn();
				radioItems++;
				$(this).attr('selectedIndex',0);
			}
			if($(this).val()=='checkbox')
			{
				id=genID();
				newitem = '<div id="checkbox-'+checkboxItems+'"><fieldset name="checkbox"><legend>Check Box field</legend>';
				newitem += 'Name <input name="form-checkbox-'+checkboxItems+'" type="text" /> ';
				newitem += 'Required?<input name="form-checkbox-'+checkboxItems+'-required" type="checkbox" /> ';
				newitem += '<a class="removeitem" href="javascript:void(0)"><img src="images/delete.png" alt="delete"></a>';
				newitem += '<ul><li><input name="form-checkbox-'+checkboxItems+'-select[]" value="'+id+'" type="checkbox" /> ';
				newitem += '<input id="'+id+'" name="form-checkbox-'+checkboxItems+'-option['+id+']" type="text" /></li>';
				newitem += '<li><a class="addcheckboxoption" id="checkbox'+checkboxItems+'" href="javascript:void(0)">Add option</a></li></ul></fieldset></div>';
				$('#fields').append(newitem);
				$('#checkbox-'+checkboxItems).css('display','none').fadeIn();
				checkboxItems++;
				$(this).attr('selectedIndex',0);
			}
			if($(this).val()=='dropdown')
			{
				id=genID();
				newitem = '<div id="dropdown-'+dropdownItems+'"><fieldset name="dropdown"><legend>Drop Down Menu field</legend>';
				newitem += 'Name <input name="form-dropdown-'+dropdownItems+'" type="text" /> ';
				newitem += 'Required?<input name="form-dropdown-'+dropdownItems+'-required" type="checkbox" /> ';
				newitem += 'Allow multiple selections? <input name="form-dropdown-'+dropdownItems+'-multiple" type="checkbox" /> ';
				newitem += '<a class="removeitem" href="javascript:void(0)"><img src="images/delete.png" alt="delete"></a>';
				newitem += '<ul><li><input name="form-dropdown-'+dropdownItems+'-select[]" value="'+id+'" type="checkbox" /> ';
				newitem += '<input id="'+id+'" name="form-dropdown-'+dropdownItems+'-option['+id+']" type="text" /></li>';
				newitem += '<li><a class="adddropdownoption" id="dropdown'+dropdownItems+'" href="javascript:void(0)">Add option</a></li></ul></fieldset></div>';
				$('#fields').append(newitem);
				$('#dropdown-'+dropdownItems).css('display','none').fadeIn();
				dropdownItems++;
				$(this).attr('selectedIndex',0);
			}

		});
	$('.removeitem').live('click',function()
		{
//			console.log($(this).parents()); //  [fieldset, div#text-1, div#fields, body, html]
			$(this).parent().parent().slideUp(500,function(){$(this).remove();});
		});
	$('.removeoption').live('click',function()
		{
			$(this).parent().slideUp(500,function(){$(this).remove();})
		});
	$('.addradiooption').live('click',function()
		{
			//create a new list item and insert after the current list item
			option++;
			group = $(this).attr('id').substr(-1);
			id = genID();
			newoption = '<li><input name="form-radio-'+group+'-select" value="'+id+'" type="radio" /> ';
			newoption += '<input id="'+id+'" name="form-radio-'+group+'-option['+id+']" type="text" /> ';
			newoption += '<a class="removeoption" href="javascript:void(0)"><img src="images/delete.png" alt="delete"></a></li>';
			$(this).parent().before(newoption);
		});
	$('.addcheckboxoption').live('click',function()
		{
			//create a new list item and insert after the current list item
			option++;
			group = $(this).attr('id').substr(-1);
			id = genID();
			newoption = '<li><input name="form-checkbox-'+group+'-select[]" value="'+id+'" type="checkbox" /> ';
			newoption += '<input id="'+id+'" name="form-checkbox-'+group+'-option['+id+']" type="text" /> ';
			newoption += '<a class="removeoption" href="javascript:void(0)"><img src="images/delete.png" alt="delete"></a></li>';
			$(this).parent().before(newoption);
		});
	$('.adddropdownoption').live('click',function()
		{
			//create a new list item and insert after the current list item
			option++;
			group = $(this).attr('id').substr(-1);
			id = genID();
			newoption = '<li><input name="form-dropdown-'+group+'-select[]" value="'+id+'" type="checkbox" /> ';
			newoption += '<input id="'+id+'" name="form-dropdown-'+group+'-option['+id+']" type="text" /> ';
			newoption += '<a class="removeoption" href="javascript:void(0)"><img src="images/delete.png" alt="delete"></a></li>';
			$(this).parent().before(newoption);
		});
		
	$('#reveal').click(function(){
		console.log();
	});

	$('#navlist a').each(function(){
		if($(this).text()=='Lists') $(this).attr('id','current');
	});
});
=======
function genID()
{
	return Math.round(Math.random()*100000000);
}
$(function(){
	$('.collector a').click(function(){return confirm("Are you sure you want to delete this data field? Removing a data field will also remove it from any lists that use it.")});

	// give unique id's to every generated radio, checkbox, and drop down element
	// then tie the state of being selected/checked to the id

	var textItems=0,textareaItems=0,radioItems=0,checkboxItems=0,dropdownItems=0,option=0;
	$('#item').change(function()
		{
			option = 0;
			if($(this).val()=='text')
			{
				newitem = '<div id="text-'+textItems+'"><fieldset name="text"><legend>Text field</legend>';
				newitem += 'Name <input name="form-text-'+textItems+'" type="text" /> ';
				newitem += 'Required?<input name="form-text-'+textItems+'-required" type="checkbox" /> ';
				newitem += '<a class="removeitem" href="javascript:void(0)"><img src="images/delete.png" alt="delete"></a></fieldset></div>';
				$('#fields').append(newitem);
				$('#text-'+textItems).css('display','none').fadeIn();
				textItems++;
				$(this).attr('selectedIndex',0);
			}
			if($(this).val()=='textarea')
			{
				newitem = '<div id="textarea-'+textareaItems+'"><fieldset name="textarea"><legend>Text Area field</legend>';
				newitem += 'Name <input name="form-textarea-'+textareaItems+'" type="text" /> ';
				newitem += 'Required?<input name="form-textarea-'+textareaItems+'-required" type="checkbox" /> ';
				newitem += '<a class="removeitem" href="javascript:void(0)"><img src="images/delete.png" alt="delete"></a></fieldset></div>';
				$('#fields').append(newitem);
				$('#textarea-'+textareaItems).css('display','none').fadeIn();
				textareaItems++;
				$(this).attr('selectedIndex',0);
			}
			if($(this).val()=='radio')
			{
				id=genID();
				newitem = '<div id="radio-'+radioItems+'"><fieldset name="radio"><legend>Radio Button field</legend>';
				newitem += 'Name <input name="form-radio-'+radioItems+'" type="text" /> ';
				newitem += 'Required?<input name="form-radio-'+radioItems+'-required" type="checkbox" /> ';
				newitem += '<a class="removeitem" href="javascript:void(0)"><img src="images/delete.png" alt="delete"></a>';
				newitem += '<ul><li><input name="form-radio-'+radioItems+'-select" value="'+id+'" type="radio" /> ';
				newitem += '<input id="'+id+'" name="form-radio-'+radioItems+'-option['+id+']" type="text" /></li>';
				newitem += '<li><a class="addradiooption" id="radio'+radioItems+'" href="javascript:void(0)">Add option</a></li></ul></fieldset></div>';
				$('#fields').append(newitem);
				$('#radio-'+radioItems).css('display','none').fadeIn();
				radioItems++;
				$(this).attr('selectedIndex',0);
			}
			if($(this).val()=='checkbox')
			{
				id=genID();
				newitem = '<div id="checkbox-'+checkboxItems+'"><fieldset name="checkbox"><legend>Check Box field</legend>';
				newitem += 'Name <input name="form-checkbox-'+checkboxItems+'" type="text" /> ';
				newitem += 'Required?<input name="form-checkbox-'+checkboxItems+'-required" type="checkbox" /> ';
				newitem += '<a class="removeitem" href="javascript:void(0)"><img src="images/delete.png" alt="delete"></a>';
				newitem += '<ul><li><input name="form-checkbox-'+checkboxItems+'-select[]" value="'+id+'" type="checkbox" /> ';
				newitem += '<input id="'+id+'" name="form-checkbox-'+checkboxItems+'-option['+id+']" type="text" /></li>';
				newitem += '<li><a class="addcheckboxoption" id="checkbox'+checkboxItems+'" href="javascript:void(0)">Add option</a></li></ul></fieldset></div>';
				$('#fields').append(newitem);
				$('#checkbox-'+checkboxItems).css('display','none').fadeIn();
				checkboxItems++;
				$(this).attr('selectedIndex',0);
			}
			if($(this).val()=='dropdown')
			{
				id=genID();
				newitem = '<div id="dropdown-'+dropdownItems+'"><fieldset name="dropdown"><legend>Drop Down Menu field</legend>';
				newitem += 'Name <input name="form-dropdown-'+dropdownItems+'" type="text" /> ';
				newitem += 'Required?<input name="form-dropdown-'+dropdownItems+'-required" type="checkbox" /> ';
				newitem += 'Allow multiple selections? <input name="form-dropdown-'+dropdownItems+'-multiple" type="checkbox" /> ';
				newitem += '<a class="removeitem" href="javascript:void(0)"><img src="images/delete.png" alt="delete"></a>';
				newitem += '<ul><li><input name="form-dropdown-'+dropdownItems+'-select[]" value="'+id+'" type="checkbox" /> ';
				newitem += '<input id="'+id+'" name="form-dropdown-'+dropdownItems+'-option['+id+']" type="text" /></li>';
				newitem += '<li><a class="adddropdownoption" id="dropdown'+dropdownItems+'" href="javascript:void(0)">Add option</a></li></ul></fieldset></div>';
				$('#fields').append(newitem);
				$('#dropdown-'+dropdownItems).css('display','none').fadeIn();
				dropdownItems++;
				$(this).attr('selectedIndex',0);
			}

		});
	$('.removeitem').live('click',function()
		{
//			console.log($(this).parents()); //  [fieldset, div#text-1, div#fields, body, html]
			$(this).parent().parent().slideUp(500,function(){$(this).remove();});
		});
	$('.removeoption').live('click',function()
		{
			$(this).parent().slideUp(500,function(){$(this).remove();})
		});
	$('.addradiooption').live('click',function()
		{
			//create a new list item and insert after the current list item
			option++;
			group = $(this).attr('id').substr(-1);
			id = genID();
			newoption = '<li><input name="form-radio-'+group+'-select" value="'+id+'" type="radio" /> ';
			newoption += '<input id="'+id+'" name="form-radio-'+group+'-option['+id+']" type="text" /> ';
			newoption += '<a class="removeoption" href="javascript:void(0)"><img src="images/delete.png" alt="delete"></a></li>';
			$(this).parent().before(newoption);
		});
	$('.addcheckboxoption').live('click',function()
		{
			//create a new list item and insert after the current list item
			option++;
			group = $(this).attr('id').substr(-1);
			id = genID();
			newoption = '<li><input name="form-checkbox-'+group+'-select[]" value="'+id+'" type="checkbox" /> ';
			newoption += '<input id="'+id+'" name="form-checkbox-'+group+'-option['+id+']" type="text" /> ';
			newoption += '<a class="removeoption" href="javascript:void(0)"><img src="images/delete.png" alt="delete"></a></li>';
			$(this).parent().before(newoption);
		});
	$('.adddropdownoption').live('click',function()
		{
			//create a new list item and insert after the current list item
			option++;
			group = $(this).attr('id').substr(-1);
			id = genID();
			newoption = '<li><input name="form-dropdown-'+group+'-select[]" value="'+id+'" type="checkbox" /> ';
			newoption += '<input id="'+id+'" name="form-dropdown-'+group+'-option['+id+']" type="text" /> ';
			newoption += '<a class="removeoption" href="javascript:void(0)"><img src="images/delete.png" alt="delete"></a></li>';
			$(this).parent().before(newoption);
		});
		
	$('#reveal').click(function(){
		console.log();
	});

	$('#navlist a').each(function(){
		if($(this).text()=='Lists') $(this).attr('id','current');
	});
});
>>>>>>> b59280b13ce027f89f28f2d12e991e62fe2dd1bd
