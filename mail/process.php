<<<<<<< HEAD
<?php
include('database.php');
include('functions.php');
//print_r($_REQUEST);

$addToLists = array();
$isListOpen = false;
$address = mysql_real_escape_string(trim($_REQUEST['address']));

function redirect($result, $action, $address)
{
	// When a subscribe/unsubscribe request is processed, the last step is to send the user to a page with some info.
	// This info, set in session vars, is the result (success/fail), action (subscribe/unsubscribe), and email address.
	// It's up to the site owner to take advantage of these sessions vars and display a message to the visitor.
	// An owner can also set a hidden form field in their subscribe/unsubscribe form named return_to with the URL of a
	// page that the visitor should be sent to upon submitting the form. If omitted or empty, they will be returned to
	// the page they began at.
	session_start();
	$_SESSION['result'] = $result;
	$_SESSION['action'] = $action;
	$_SESSION['address'] = $address;
	// check to see if a url was sent to forward the user to. if no, return to referring page
	$goto = (!empty($_REQUEST['return_to'])) ? $_REQUEST['return_to']:$_SERVER['HTTP_REFERER'] ;
	header('Location: '. $goto);
	exit();
}

// Validate address
if(validate_address($address))
{
	// Check what lists were selected
	if(count($_REQUEST['lists'])>0)
	{
		// Iterate through all the list ids that were requested to be subscribed to. Check for closed lists and non-existant lists.
		// If any lists passed are closed or don't exist, redirect user with fail message and do not process *any* of their lists.
		foreach($_REQUEST['lists'] as $list)
		{
			$listInfo = $database->getListInfo($list);
			if($listInfo['status'])
			{
				$isListOpen=true;
				$addToLists[] = $list; // Build an array of open list ids that can be subscribed or unsubscribed to
			}
			else
			{
				$log['address']=$_REQUEST['address'];
				$log['lists']=$_REQUEST['lists'];
				$log['IP']=$_SERVER['REMOTE_ADDR'];
				$database->log(time(),'Invalid, closed, or no lists selected (process.php)',serialize($log));
				redirect('fail','subscribe',$address);
			}
		}
		if($isListOpen)
		{
			foreach($addToLists as $list)
			{
				// Iterate through the open lists the user has requested, check blacklist rules, and insert into list_members table
				$blacklisted = blacklist($address,$list);
				if(!$blacklisted)
				{
					// get list id, see if there are data fields, and serialize the input sanitzed name fields
					$member_data = null;
					$result = $database->getListDataFields($list); // Returns a comma separated list of data field ids or null

					$memberLists = $database->getMemberLists($address);
					if(!empty($memberLists)) $memberListInfo = mysql_fetch_array($memberLists);

					if(!is_null($result))
					{
						$row=mysql_fetch_array($result);
						if(!empty($row['data_fields']))
						{
							$fields = explode(',',$row['data_fields']);
							foreach($fields as $field)
							{
								$field_name_result = mysql_fetch_array($database->getDataField($field)); // Get all info for data field
								// see if field is required in db. if it is and its empty, set session vars and redirect
								if(empty($memberLists) And $field_name_result['required'] And empty($_REQUEST[$field_name_result[sanitized_name]])) redirect('fail','subscribe-reqd field',$address);
								if( !empty($_REQUEST[$field_name_result[sanitized_name]]) ) $member_data[$field_name_result[sanitized_name]]=$_REQUEST[$field_name_result[sanitized_name]];
							}
						}
						if(count($member_data)>0) $member_data = serialize($member_data);
					}
					$okToInsert = (REQUIRE_USER_CONFIRMATION) ? $database->insertAddressList($address,$list,'0',$member_data):$database->insertAddressList($address,$list,'1',$member_data);
				}
				else
				{
					// Address blacklisted
					$database->log(time(),'Address blacklisted (process.php)',serialize($_REQUEST));
					redirect('fail','subscribe',$address);
				}
			}
			// See if a member already. if not, insert member and send confirm email. if so, send unsub email
			$memberInfo = $database->getMemberInfo($address);
			$lists = implode(',',$addToLists);
			if(is_null($memberInfo))
			{
				// Not an existing member. Add and email. Form submission from people not in the members table are always treated as sub requests
				if($database->insertAddress($address,''))
				{
					$memberInfo = mysql_fetch_array($database->getMemberInfo($address));

					$auth_link = "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF'] . "?address=$address&key=$memberInfo[memberkey]&c=1&l=$lists";
					$auth_link = str_replace('process.php','confirm.php',$auth_link);

					$message = "To confirm your subscription request to the following list(s):\n\n";
					foreach($addToLists as $list)
					{
						$message .= "* " . $database->getListName($list). "\n";
					}
					$message .= "\nwe ask that you follow this link:\n\n$auth_link\n\nThank you,\n";
					$message .= ADMIN_EMAIL."\n";
					$subject = "Please confirm your subscription request";
					// uncomment next line when testing
					if(REQUIRE_USER_CONFIRMATION) $result = sendmail($address, ADMIN_EMAIL, $subject, $message, '','text');

					redirect('success','subscribe',$address);
				}
				else
				{
					// log error inserting
					$database->log(time(),'Insert address (process.php)',serialize($_REQUEST));
				}
			}
			else
			{
				// An existing member. Two parts before sending email. One to build sub requests for lists they're not on, one to build unsub requests for lists they are on.
				$memberInfo = mysql_fetch_array($memberInfo);
				foreach($addToLists as $list)
				{
					//query confirmed for address/list id.
					$confirmed_status = $database->getConfirmedStatus($address,$list);
					if($confirmed_status == 0) {
						$sub[]=$list;
					}
					else { $unsub[]=$list;
					}
				}

				// should probably get rid of the next couple of lines and also add in a subject line specific to sub or unsub
				//				$mail = Mail::factory("mail");
				$headers = array("From"=>ADMIN_EMAIL, "Subject"=>"Please confirm your unsubscription request");
				$subject = "Please confirm your subscription change request";
				$message = '';
					
				if(is_array($sub))
				{
					$sub_lists = implode(',',$sub);
					$sub_link = "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF'] . "?address=$address&key=$memberInfo[memberkey]&c=1&l=$sub_lists";
					$sub_link = str_replace('process.php','confirm.php',$sub_link);
					$message .= "To confirm your subscription request to the following list(s):\n\n";
					foreach($sub as $list)
					{
						$message .= "* " . $database->getListName($list). "\n";
					}
					$message .= "\nwe ask that you follow this link:\n\n$sub_link\n\n";

				}
				if(is_array($unsub))
				{
					$unsub_lists = implode(',',$unsub);
					$unsub_link = "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF'] . "?address=$address&key=$memberInfo[memberkey]&c=0&l=$unsub_lists";
					$unsub_link = str_replace('process.php','confirm.php',$unsub_link);
					$message .= "To confirm your unsubscription request to the following list(s):\n\n";
					foreach($unsub as $list)
					{
						$message .= "* " . $database->getListName($list). "\n";

						if(!REQUIRE_USER_CONFIRMATION)
						{
							$database->deleteConfirmedListMember($address,$list);
							$database->cleanupMembers();
						}
					}
					$message .= "\nwe ask that you follow this link:\n\n$unsub_link\n\n";
				}

				$message .= "Thank you,\n".ADMIN_EMAIL."\n";
				//replace with sendmail function call
				//				$mail->send($address, $headers, $message);
				// uncomment next line when testing
				if(REQUIRE_USER_CONFIRMATION) $result = sendmail($address, ADMIN_EMAIL, $subject, $message, '','text');

				redirect('success','unsubscribe',$address);
			}
		}
		else
		{
			// no lists were open and/or valid. log and return
			$log['address']=$_REQUEST['address'];
			$log['lists']=$_REQUEST['lists'];
			$log['IP']=$_SERVER['REMOTE_ADDR'];

			$database->log(time(),'Invalid or no lists selected (process.php)',serialize($log));
			redirect('fail','subscribe',$address);
		}
	}
	else
	{
		// no lists passed. log and return
		$database->log(time(),'No lists passed (process.php)',serialize($_REQUEST));
		redirect('fail','subscribe',$address);
	}
}
else
{
	// Address didn't pass basic validation
	$database->log(time(),'Invalid address format (process.php)',serialize($_REQUEST));
	redirect('fail','subscribe',$address);
}
=======
<?php
include('database.php');
include('functions.php');
//print_r($_REQUEST);

$addToLists = array();
$isListOpen = false;
$address = mysql_real_escape_string(trim($_REQUEST['address']));

function redirect($result, $action, $address)
{
	// When a subscribe/unsubscribe request is processed, the last step is to send the user to a page with some info.
	// This info, set in session vars, is the result (success/fail), action (subscribe/unsubscribe), and email address.
	// It's up to the site owner to take advantage of these sessions vars and display a message to the visitor.
	// An owner can also set a hidden form field in their subscribe/unsubscribe form named return_to with the URL of a
	// page that the visitor should be sent to upon submitting the form. If omitted or empty, they will be returned to
	// the page they began at.
	session_start();
	$_SESSION['result'] = $result;
	$_SESSION['action'] = $action;
	$_SESSION['address'] = $address;
	// check to see if a url was sent to forward the user to. if no, return to referring page
	$goto = (!empty($_REQUEST['return_to'])) ? $_REQUEST['return_to']:$_SERVER['HTTP_REFERER'] ;
	header('Location: '. $goto);
	exit();
}

// Validate address
if(validate_address($address))
{
	// Check what lists were selected
	if(count($_REQUEST['lists'])>0)
	{
		// Iterate through all the list ids that were requested to be subscribed to. Check for closed lists and non-existant lists.
		// If any lists passed are closed or don't exist, redirect user with fail message and do not process *any* of their lists.
		foreach($_REQUEST['lists'] as $list)
		{
			$listInfo = $database->getListInfo($list);
			if($listInfo['status'])
			{
				$isListOpen=true;
				$addToLists[] = $list; // Build an array of open list ids that can be subscribed or unsubscribed to
			}
			else
			{
				$log['address']=$_REQUEST['address'];
				$log['lists']=$_REQUEST['lists'];
				$log['IP']=$_SERVER['REMOTE_ADDR'];
				$database->log(time(),'Invalid, closed, or no lists selected (process.php)',serialize($log));
				redirect('fail','subscribe',$address);
			}
		}
		if($isListOpen)
		{
			foreach($addToLists as $list)
			{
				// Iterate through the open lists the user has requested, check blacklist rules, and insert into list_members table
				$blacklisted = blacklist($address,$list);
				if(!$blacklisted)
				{
					// get list id, see if there are data fields, and serialize the input sanitzed name fields
					$member_data = null;
					$result = $database->getListDataFields($list); // Returns a comma separated list of data field ids or null

					$memberLists = $database->getMemberLists($address);
					if(!empty($memberLists)) $memberListInfo = mysql_fetch_array($memberLists);

					if(!is_null($result))
					{
						$row=mysql_fetch_array($result);
						if(!empty($row['data_fields']))
						{
							$fields = explode(',',$row['data_fields']);
							foreach($fields as $field)
							{
								$field_name_result = mysql_fetch_array($database->getDataField($field)); // Get all info for data field
								// see if field is required in db. if it is and its empty, set session vars and redirect
								if(empty($memberLists) And $field_name_result['required'] And empty($_REQUEST[$field_name_result[sanitized_name]])) redirect('fail','subscribe-reqd field',$address);
								if( !empty($_REQUEST[$field_name_result[sanitized_name]]) ) $member_data[$field_name_result[sanitized_name]]=$_REQUEST[$field_name_result[sanitized_name]];
							}
						}
						if(count($member_data)>0) $member_data = serialize($member_data);
					}
					$okToInsert = (REQUIRE_USER_CONFIRMATION) ? $database->insertAddressList($address,$list,'0',$member_data):$database->insertAddressList($address,$list,'1',$member_data);
				}
				else
				{
					// Address blacklisted
					$database->log(time(),'Address blacklisted (process.php)',serialize($_REQUEST));
					redirect('fail','subscribe',$address);
				}
			}
			// See if a member already. if not, insert member and send confirm email. if so, send unsub email
			$memberInfo = $database->getMemberInfo($address);
			$lists = implode(',',$addToLists);
			if(is_null($memberInfo))
			{
				// Not an existing member. Add and email. Form submission from people not in the members table are always treated as sub requests
				if($database->insertAddress($address,''))
				{
					$memberInfo = mysql_fetch_array($database->getMemberInfo($address));

					$auth_link = "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF'] . "?address=$address&key=$memberInfo[memberkey]&c=1&l=$lists";
					$auth_link = str_replace('process.php','confirm.php',$auth_link);

					$message = "To confirm your subscription request to the following list(s):\n\n";
					foreach($addToLists as $list)
					{
						$message .= "* " . $database->getListName($list). "\n";
					}
					$message .= "\nwe ask that you follow this link:\n\n$auth_link\n\nThank you,\n";
					$message .= ADMIN_EMAIL."\n";
					$subject = "Please confirm your subscription request";
					// uncomment next line when testing
					if(REQUIRE_USER_CONFIRMATION) $result = sendmail($address, ADMIN_EMAIL, $subject, $message, '','text');

					redirect('success','subscribe',$address);
				}
				else
				{
					// log error inserting
					$database->log(time(),'Insert address (process.php)',serialize($_REQUEST));
				}
			}
			else
			{
				// An existing member. Two parts before sending email. One to build sub requests for lists they're not on, one to build unsub requests for lists they are on.
				$memberInfo = mysql_fetch_array($memberInfo);
				foreach($addToLists as $list)
				{
					//query confirmed for address/list id.
					$confirmed_status = $database->getConfirmedStatus($address,$list);
					if($confirmed_status == 0) {
						$sub[]=$list;
					}
					else { $unsub[]=$list;
					}
				}

				// should probably get rid of the next couple of lines and also add in a subject line specific to sub or unsub
				//				$mail = Mail::factory("mail");
				$headers = array("From"=>ADMIN_EMAIL, "Subject"=>"Please confirm your unsubscription request");
				$subject = "Please confirm your subscription change request";
				$message = '';
					
				if(is_array($sub))
				{
					$sub_lists = implode(',',$sub);
					$sub_link = "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF'] . "?address=$address&key=$memberInfo[memberkey]&c=1&l=$sub_lists";
					$sub_link = str_replace('process.php','confirm.php',$sub_link);
					$message .= "To confirm your subscription request to the following list(s):\n\n";
					foreach($sub as $list)
					{
						$message .= "* " . $database->getListName($list). "\n";
					}
					$message .= "\nwe ask that you follow this link:\n\n$sub_link\n\n";

				}
				if(is_array($unsub))
				{
					$unsub_lists = implode(',',$unsub);
					$unsub_link = "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF'] . "?address=$address&key=$memberInfo[memberkey]&c=0&l=$unsub_lists";
					$unsub_link = str_replace('process.php','confirm.php',$unsub_link);
					$message .= "To confirm your unsubscription request to the following list(s):\n\n";
					foreach($unsub as $list)
					{
						$message .= "* " . $database->getListName($list). "\n";

						if(!REQUIRE_USER_CONFIRMATION)
						{
							$database->deleteConfirmedListMember($address,$list);
							$database->cleanupMembers();
						}
					}
					$message .= "\nwe ask that you follow this link:\n\n$unsub_link\n\n";
				}

				$message .= "Thank you,\n".ADMIN_EMAIL."\n";
				//replace with sendmail function call
				//				$mail->send($address, $headers, $message);
				// uncomment next line when testing
				if(REQUIRE_USER_CONFIRMATION) $result = sendmail($address, ADMIN_EMAIL, $subject, $message, '','text');

				redirect('success','unsubscribe',$address);
			}
		}
		else
		{
			// no lists were open and/or valid. log and return
			$log['address']=$_REQUEST['address'];
			$log['lists']=$_REQUEST['lists'];
			$log['IP']=$_SERVER['REMOTE_ADDR'];

			$database->log(time(),'Invalid or no lists selected (process.php)',serialize($log));
			redirect('fail','subscribe',$address);
		}
	}
	else
	{
		// no lists passed. log and return
		$database->log(time(),'No lists passed (process.php)',serialize($_REQUEST));
		redirect('fail','subscribe',$address);
	}
}
else
{
	// Address didn't pass basic validation
	$database->log(time(),'Invalid address format (process.php)',serialize($_REQUEST));
	redirect('fail','subscribe',$address);
}
>>>>>>> b59280b13ce027f89f28f2d12e991e62fe2dd1bd
?>