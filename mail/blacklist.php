<<<<<<< HEAD
<?php
include('database.php');
if(!empty($_POST['rule']))
{
	$database->addBlacklistRule($_POST['rule'],$_POST['lists']);
	$database->applyBlacklistRule($_POST['rule'],$_POST['lists']);
	$database->cleanupMembers();

	// use this query to get any people in the members table that no longer belong to any lists
	/*
	SELECT *
	FROM sml_members AS m
	LEFT OUTER JOIN sml_list_members AS l ON l.address = m.address
	WHERE l.address IS NULL
	*/
}

if($_GET[r])	$database->deleteBlacklistRule($_GET['r'],$_GET['l']);
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Simple Mailing List - Blacklist Rules</title>

<link type="text/css" href="css/custom-theme/jquery-ui-1.8.2.custom.css"
	rel="stylesheet">
<link type="text/css" href="css/blacklist.css" rel="stylesheet">

</head>

<body>
	<div id="content_container">
		<?php
		include('nav.php');
		?>
		<div id="content">
			<p class="helpText">Blacklist rules are used to prevent specific
				addresses, or groups of addresses, from signing up for your list.
				New blacklist rules may affect existing members and can potentially
				remove them from your list. It is strongly recommended that you test
				your new rule first via the 'Check Rule' button before adding it to
				see if it will impact any existing members. New rules will be
				applied to your existing member list.</p>

			<h3>Current blacklist rules:</h3>

			<?php

			// query for existing rules and display rule and delete
			$result = $database->getBlacklistRules();
			if($result==NULL)
			{
				echo "<p class=\"help\">You currently have no blacklist rules.</p>";
			}
			else
			{
				echo "<table border=\"0\">\n";
				while($row = mysql_fetch_array($result))
				{
					echo "<tr><td>$row[rule] (".$database->getListName($row[list_id]).")</td><td><a class=\"delete\" href=\"blacklist.php?r=$row[rule]&l=$row[list_id]\"><img src=\"images/delete.png\" alt=\"delete\"></a></td></tr>\n";
				}
				echo "</table>\n";
			}
			$result = $database->getListNames();
			if(is_null($result)) exit("<p>You'll need to <a href=\"list_management_add.php\">create at least one list</a> prior to adding any blacklist rules.</p></body></html>");
			?>

			<h3>Add a blacklist rule:</h3>
			<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
				<p>To prevent a specific user from signing up, enter their email
					address. You can also prevent an entire domain from signing up by
					entering their domain name (e.g. @aol.com, @yahoo.com), or even an
					entire top level domain (e.g. .com, .net, .org). Use the 'Check
					Rule' button to see if the rule you want to create will effect any
					existing members.</p>
				<p>
					<input type="text" id="rule_input" name="rule" size="20">
					<button type="button" id="testRule" name="testRule">
						Check Rule <img style="vertical-align: top" src="images/check.png">
					</button>
				
				
				<p>Apply this rule to the following lists:</p>
				<div id="lists">
					<?php
					$num_lists = $database->getListCount();
					if($num_lists>1) echo "<label><input type=\"checkbox\" id=\"all\" value=\"\">Select All/None</label><br>\n";
					if(!is_null($result))
					{
						$checked = ($num_lists == 1) ? ' checked="checked"':'';
						while($row=mysql_fetch_array($result))
						{
							echo "<label style=\"margin-left:16px\"><input type=\"checkbox\" name=\"lists[]\" value=\"$row[id]\"$checked>$row[name]";
							echo " - $row[owner]</label><br>\n";
						}
					}
					?>
				</div>
				<p>
					<button id="submit" name="submit" style="font-size: 1.6em">
						<img style="vertical-align: middle;" src="images/bullet-add.png">Add
						Rule
					</button>
				</p>
			</form>
		</div>
		<!-- #content -->
	</div>
	<!-- #content_container -->
	<p id="copyright">&copy; www.notonebit.com</p>

	<div id="blacklistDialog" title="Blacklist Rule Preview">
		<p></p>
	</div>

	<script src="js/jquery-1.6.2.min.js"></script>
	<script src="js/jquery-ui-1.8.custom.min.js"></script>
	<script src="js/blacklist.js"></script>
</body>
=======
<?php
include('database.php');
if(!empty($_POST['rule']))
{
	$database->addBlacklistRule($_POST['rule'],$_POST['lists']);
	$database->applyBlacklistRule($_POST['rule'],$_POST['lists']);
	$database->cleanupMembers();

	// use this query to get any people in the members table that no longer belong to any lists
	/*
	SELECT *
	FROM sml_members AS m
	LEFT OUTER JOIN sml_list_members AS l ON l.address = m.address
	WHERE l.address IS NULL
	*/
}

if($_GET[r])	$database->deleteBlacklistRule($_GET['r'],$_GET['l']);
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Simple Mailing List - Blacklist Rules</title>

<link type="text/css" href="css/custom-theme/jquery-ui-1.8.2.custom.css"
	rel="stylesheet">
<link type="text/css" href="css/blacklist.css" rel="stylesheet">

</head>

<body>
	<div id="content_container">
		<?php
		include('nav.php');
		?>
		<div id="content">
			<p class="helpText">Blacklist rules are used to prevent specific
				addresses, or groups of addresses, from signing up for your list.
				New blacklist rules may affect existing members and can potentially
				remove them from your list. It is strongly recommended that you test
				your new rule first via the 'Check Rule' button before adding it to
				see if it will impact any existing members. New rules will be
				applied to your existing member list.</p>

			<h3>Current blacklist rules:</h3>

			<?php

			// query for existing rules and display rule and delete
			$result = $database->getBlacklistRules();
			if($result==NULL)
			{
				echo "<p class=\"help\">You currently have no blacklist rules.</p>";
			}
			else
			{
				echo "<table border=\"0\">\n";
				while($row = mysql_fetch_array($result))
				{
					echo "<tr><td>$row[rule] (".$database->getListName($row[list_id]).")</td><td><a class=\"delete\" href=\"blacklist.php?r=$row[rule]&l=$row[list_id]\"><img src=\"images/delete.png\" alt=\"delete\"></a></td></tr>\n";
				}
				echo "</table>\n";
			}
			$result = $database->getListNames();
			if(is_null($result)) exit("<p>You'll need to <a href=\"list_management_add.php\">create at least one list</a> prior to adding any blacklist rules.</p></body></html>");
			?>

			<h3>Add a blacklist rule:</h3>
			<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
				<p>To prevent a specific user from signing up, enter their email
					address. You can also prevent an entire domain from signing up by
					entering their domain name (e.g. @aol.com, @yahoo.com), or even an
					entire top level domain (e.g. .com, .net, .org). Use the 'Check
					Rule' button to see if the rule you want to create will effect any
					existing members.</p>
				<p>
					<input type="text" id="rule_input" name="rule" size="20">
					<button type="button" id="testRule" name="testRule">
						Check Rule <img style="vertical-align: top" src="images/check.png">
					</button>
				
				
				<p>Apply this rule to the following lists:</p>
				<div id="lists">
					<?php
					$num_lists = $database->getListCount();
					if($num_lists>1) echo "<label><input type=\"checkbox\" id=\"all\" value=\"\">Select All/None</label><br>\n";
					if(!is_null($result))
					{
						$checked = ($num_lists == 1) ? ' checked="checked"':'';
						while($row=mysql_fetch_array($result))
						{
							echo "<label style=\"margin-left:16px\"><input type=\"checkbox\" name=\"lists[]\" value=\"$row[id]\"$checked>$row[name]";
							echo " - $row[owner]</label><br>\n";
						}
					}
					?>
				</div>
				<p>
					<button id="submit" name="submit" style="font-size: 1.6em">
						<img style="vertical-align: middle;" src="images/bullet-add.png">Add
						Rule
					</button>
				</p>
			</form>
		</div>
		<!-- #content -->
	</div>
	<!-- #content_container -->
	<p id="copyright">&copy; www.notonebit.com</p>

	<div id="blacklistDialog" title="Blacklist Rule Preview">
		<p></p>
	</div>

	<script src="js/jquery-1.6.2.min.js"></script>
	<script src="js/jquery-ui-1.8.custom.min.js"></script>
	<script src="js/blacklist.js"></script>
</body>
>>>>>>> b59280b13ce027f89f28f2d12e991e62fe2dd1bd
</html>
