<<<<<<< HEAD
<?php
include('database.php');
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Simple Mailing List - Utilities</title>

<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.custom.css"
	rel="stylesheet">
<link type="text/css" href="css/utilities.css" rel="stylesheet">

</head>

<body>
	<div id="content_container">
		<?php
		include('nav.php');
		?>
		<div id="content">
			<div class="helpText">
				<p>SML has some optional functions which can be less than simple to
					configure, especially for those that haven&#39;t dealt with the
					inner workings of email before (these features rely on standards
					that we didn&#39;t create - so don&#39;t blame us if they&#39;re
					not simple). For example, SML uses PHP&#39;s mail() function to
					send mail which works fine for most people; however some folks
					would prefer to use SMTP to send mail. In order to do this you need
					to be able to connect to an SMTP server via PHP. To check for
					bounced mailings, you&#39;ll need to have a mailbox setup to
					receive bounced messages and PHP needs to be able to read the
					mailbox via IMAP or POP3.</p>
				<p>Below you&#39;ll see find links to utilities to help you
					determine the proper settings should you wish to use these optional
					SML features. When testing SMTP, you&#39;ll know you&#39;re
					successful when a test message is delivered to the address you
					specify. When testing IMAP, you&#39;ll know you&#39;re successful
					when you see a listing of the mailbox your specify (blank if there
					aren&#39;t any messages in it). There&#39;s also a utility to show
					you any error messages that SML has generated, as well as a link to
					SML&#39;s RSS feed.</p>
			</div>
			<ul>
				<li><a href="utilities_smtp.php">SMTP Diagnostic</a> - used for
					sending messages via an SMTP server</li>
				<li><a href="utilities_imap.php">IMAP Diagnostic</a> - used for
					checking for returned/undeliverable messages</li>
				<li><a href="utilities_log.php">Logs</a></li>
				<li><a href="rss.php">RSS</a> - Note: Messages in the RSS feed show
					up exactly as they're entered. If you include any data fields in
					your messages the field names will be displayed in the RSS feed.</li>
			</ul>
			<h3 class="expand">Config.ini.php Settings</h3>
			<div id="settings">
				<div style="float: left; width: 48%">
					<?php
					$consts = array_slice(get_defined_constants(),-30);
					$counter = 0;
					foreach($consts as $key=>$val)
					{
						$val = ($val===false) ? 'false':$val;
						$val = ($val===true) ? 'true':$val;
						$val = (empty($val)) ? '&lt;empty&gt;':$val;
						echo "<p style=\"margin-bottom:.2em\"><b>$key</b>:&nbsp;<code>$val</code></p>\n";
						$counter++;
						if($counter%16==0) echo "</div><div style=\"float:left;width:48%\">";
					}
					?>
				</div>
			</div>

		</div>
	</div>
	<p id="copyright">&copy; www.notonebit.com</p>
	<script src="js/jquery-1.6.2.min.js"></script>
	<script src="js/jquery-ui-1.8.custom.min.js"></script>
	<script src="js/utilities.js"></script>
</body>
=======
<?php
include('database.php');
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Simple Mailing List - Utilities</title>

<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.custom.css"
	rel="stylesheet">
<link type="text/css" href="css/utilities.css" rel="stylesheet">

</head>

<body>
	<div id="content_container">
		<?php
		include('nav.php');
		?>
		<div id="content">
			<div class="helpText">
				<p>SML has some optional functions which can be less than simple to
					configure, especially for those that haven&#39;t dealt with the
					inner workings of email before (these features rely on standards
					that we didn&#39;t create - so don&#39;t blame us if they&#39;re
					not simple). For example, SML uses PHP&#39;s mail() function to
					send mail which works fine for most people; however some folks
					would prefer to use SMTP to send mail. In order to do this you need
					to be able to connect to an SMTP server via PHP. To check for
					bounced mailings, you&#39;ll need to have a mailbox setup to
					receive bounced messages and PHP needs to be able to read the
					mailbox via IMAP or POP3.</p>
				<p>Below you&#39;ll see find links to utilities to help you
					determine the proper settings should you wish to use these optional
					SML features. When testing SMTP, you&#39;ll know you&#39;re
					successful when a test message is delivered to the address you
					specify. When testing IMAP, you&#39;ll know you&#39;re successful
					when you see a listing of the mailbox your specify (blank if there
					aren&#39;t any messages in it). There&#39;s also a utility to show
					you any error messages that SML has generated, as well as a link to
					SML&#39;s RSS feed.</p>
			</div>
			<ul>
				<li><a href="utilities_smtp.php">SMTP Diagnostic</a> - used for
					sending messages via an SMTP server</li>
				<li><a href="utilities_imap.php">IMAP Diagnostic</a> - used for
					checking for returned/undeliverable messages</li>
				<li><a href="utilities_log.php">Logs</a></li>
				<li><a href="rss.php">RSS</a> - Note: Messages in the RSS feed show
					up exactly as they're entered. If you include any data fields in
					your messages the field names will be displayed in the RSS feed.</li>
			</ul>
			<h3 class="expand">Config.ini.php Settings</h3>
			<div id="settings">
				<div style="float: left; width: 48%">
					<?php
					$consts = array_slice(get_defined_constants(),-30);
					$counter = 0;
					foreach($consts as $key=>$val)
					{
						$val = ($val===false) ? 'false':$val;
						$val = ($val===true) ? 'true':$val;
						$val = (empty($val)) ? '&lt;empty&gt;':$val;
						echo "<p style=\"margin-bottom:.2em\"><b>$key</b>:&nbsp;<code>$val</code></p>\n";
						$counter++;
						if($counter%16==0) echo "</div><div style=\"float:left;width:48%\">";
					}
					?>
				</div>
			</div>

		</div>
	</div>
	<p id="copyright">&copy; www.notonebit.com</p>
	<script src="js/jquery-1.6.2.min.js"></script>
	<script src="js/jquery-ui-1.8.custom.min.js"></script>
	<script src="js/utilities.js"></script>
</body>
>>>>>>> b59280b13ce027f89f28f2d12e991e62fe2dd1bd
</html>
