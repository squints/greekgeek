<<<<<<< HEAD
<?php
include('database.php');
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Simple Mailing List - Add a list</title>

<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.custom.css"
	rel="stylesheet">
<link type="text/css" href="css/list_management.css" rel="stylesheet">

</head>

<body>
	<div id="content_container">
		<?php
		include('nav.php');
		?>
		<div id="content">

			<p id="subnav">
				<a class="noborder" href="list_management_add.php"><img
					src="images/add-list.png" alt="Add a list" /> </a> <a
					class="noborder" href="list_management_edit.php"><img
					src="images/edit-delete-list.png" alt="Edit/Delete a list" /> </a>
				<a class="noborder" href="data_collection.php"><img
					src="images/data-collection.png" alt="Add a list" /> </a>
			</p>
			<p id="notification"></p>

			<p class="helpText">While one lists is all that&#39;s required to use
				SML, you can create as many lists as you like. You can create a list
				for anything and each list can have it&#39;s own owner
				(&quot;from&quot; address), description, and data collection form
				variables. Closing a list prevents anyone from sining up for the
				lists however you can still send messages to members on the list and
				add new users visa the members page. To collect data from your
				subscribers, drag a field from the available column to the selected
				column.When you create a list, the HTML form code for it will be
				generated automatically and displayed below the list.</p>

			<form method="post" action="">
				<div id="list_info">
					<h3 id="add_list_heading">Add a List</h3>
					<div class="left" id="list_name_container">
						<label class="label">List Name<br> <input name="list_name"
							id="list_name" type="text">
						</label>
					</div>
					<div class="left">
						<label class="label">List Owner<br> <input name="list_owner"
							id="list_owner" type="text" value="<?php echo ADMIN_EMAIL;?>">
						</label>
					</div>
					<div class="clear">
						<label class="label">List Description (optional)<br> <textarea
								name="list_description" cols="40" rows="4"></textarea>
						</label>
					</div>
					<div class="left" style="margin-right: 20px">
						<p>
							List Open? <input name="list_status" type="checkbox" value="1"
								checked="checked">
						</p>
					</div>
					<button type="button" class="clear" id="save">
						<img src="images/save24x24.png" alt="save"> Save
					</button>
				</div>

				<div style="float: left; margin: 0 0 0 30px;">
					<h3 id="data_collection_heading" class="expand">Data Collection</h3>
					<div id="data_collection">
						<p style="float: right;">Selected</p>
						<p>Available</p>
						<ul id="available" class='droptrue'>
							<?php
							$result = $database->getDataCollectionFields('ORDER BY name, type ASC');
							if(!is_null($result))
							{
								while($row=mysql_fetch_array($result))
								{
									echo "<li id=\"$row[id]\" class=\"ui-state-default\">";
									echo "<img src=\"images/form-$row[type].png\"> $row[name]";
									if($row['required']) echo " (required)";
									echo "</li>\n";
								}
							}
							?>
						</ul>
						<ul id="chosen" class='droptrue'></ul>
					</div>
				</div>
			</form>
			<div id="list_add_html"></div>
		</div>
		<!-- #content -->
	</div>
	<!-- #content_container -->
	<p id="copyright">&copy; www.notonebit.com</p>
	<script src="js/jquery-1.6.2.min.js"></script>
	<script src="js/jquery-ui-1.8.custom.min.js"></script>
	<script src="js/list_management.js"></script>
</body>
=======
<?php
include('database.php');
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Simple Mailing List - Add a list</title>

<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.custom.css"
	rel="stylesheet">
<link type="text/css" href="css/list_management.css" rel="stylesheet">

</head>

<body>
	<div id="content_container">
		<?php
		include('nav.php');
		?>
		<div id="content">

			<p id="subnav">
				<a class="noborder" href="list_management_add.php"><img
					src="images/add-list.png" alt="Add a list" /> </a> <a
					class="noborder" href="list_management_edit.php"><img
					src="images/edit-delete-list.png" alt="Edit/Delete a list" /> </a>
				<a class="noborder" href="data_collection.php"><img
					src="images/data-collection.png" alt="Add a list" /> </a>
			</p>
			<p id="notification"></p>

			<p class="helpText">While one lists is all that&#39;s required to use
				SML, you can create as many lists as you like. You can create a list
				for anything and each list can have it&#39;s own owner
				(&quot;from&quot; address), description, and data collection form
				variables. Closing a list prevents anyone from sining up for the
				lists however you can still send messages to members on the list and
				add new users visa the members page. To collect data from your
				subscribers, drag a field from the available column to the selected
				column.When you create a list, the HTML form code for it will be
				generated automatically and displayed below the list.</p>

			<form method="post" action="">
				<div id="list_info">
					<h3 id="add_list_heading">Add a List</h3>
					<div class="left" id="list_name_container">
						<label class="label">List Name<br> <input name="list_name"
							id="list_name" type="text">
						</label>
					</div>
					<div class="left">
						<label class="label">List Owner<br> <input name="list_owner"
							id="list_owner" type="text" value="<?php echo ADMIN_EMAIL;?>">
						</label>
					</div>
					<div class="clear">
						<label class="label">List Description (optional)<br> <textarea
								name="list_description" cols="40" rows="4"></textarea>
						</label>
					</div>
					<div class="left" style="margin-right: 20px">
						<p>
							List Open? <input name="list_status" type="checkbox" value="1"
								checked="checked">
						</p>
					</div>
					<button type="button" class="clear" id="save">
						<img src="images/save24x24.png" alt="save"> Save
					</button>
				</div>

				<div style="float: left; margin: 0 0 0 30px;">
					<h3 id="data_collection_heading" class="expand">Data Collection</h3>
					<div id="data_collection">
						<p style="float: right;">Selected</p>
						<p>Available</p>
						<ul id="available" class='droptrue'>
							<?php
							$result = $database->getDataCollectionFields('ORDER BY name, type ASC');
							if(!is_null($result))
							{
								while($row=mysql_fetch_array($result))
								{
									echo "<li id=\"$row[id]\" class=\"ui-state-default\">";
									echo "<img src=\"images/form-$row[type].png\"> $row[name]";
									if($row['required']) echo " (required)";
									echo "</li>\n";
								}
							}
							?>
						</ul>
						<ul id="chosen" class='droptrue'></ul>
					</div>
				</div>
			</form>
			<div id="list_add_html"></div>
		</div>
		<!-- #content -->
	</div>
	<!-- #content_container -->
	<p id="copyright">&copy; www.notonebit.com</p>
	<script src="js/jquery-1.6.2.min.js"></script>
	<script src="js/jquery-ui-1.8.custom.min.js"></script>
	<script src="js/list_management.js"></script>
</body>
>>>>>>> b59280b13ce027f89f28f2d12e991e62fe2dd1bd
</html>
