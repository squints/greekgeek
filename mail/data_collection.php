<<<<<<< HEAD
<?php
include('database.php');

if($_GET['d'])
{
	$database->deleteDataField($_GET['d']);

	// Remove deleted field from all lists.
	$result = $database->getListNames();
	while($row = mysql_fetch_array($result))
	{
		$fields=explode(',',$row['data_fields']);
		if(in_array($_GET['d'],$fields))
		{
			$key=array_search($_GET['d'],$fields);
			unset($fields[$key]);
		}
		$imploded = implode(',',$fields);
		$database->updateListDataField($row[id],$imploded);
	}
}
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Simple Mailing List - Data Collection</title>
<script src="js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="js/data_collection.js"></script>

<link type="text/css" href="css/data_collection.css" rel="stylesheet">

</head>

<body>
	<div id="content_container">
		<?php
		include('nav.php');
		?>
		<div id="content">

			<p id="subnav">
				<a class="noborder" href="list_management_add.php"><img
					src="images/add-list.png" alt="Add a list" /> </a> <a
					class="noborder" href="list_management_edit.php"><img
					src="images/edit-delete-list.png" alt="Edit/Delete a list" /> </a>
				<a class="noborder" href="data_collection.php"><img
					src="images/data-collection.png" alt="Add a list" /> </a>
			</p>
			<div class="helpText">
				<p>While building a subscriber base and sending messages are the
					core of SML, you may find yourself looking to collect additional
					data on your subscribers. Whether it&#39;s just their name or more
					detailed information, SML allows you to collect any kind of data on
					your subscribers. By using the Data Collection feature, you can
					create a variety of input fields which you can present to your
					subscribers to complete. Fields can be required or optional and can
					be used when sending messages as well (e.g. Dear [[first-name]],).</p>
				<p>Here is where you will create the fields which you can pick from
					in the lists that you create. You create and delete your fields
					here, and then add or remove them to a list on the list edit page.
					Multiple fields can be added simultaneously. Note that when you
					give your data field a name, you may want to make it human readable
					as SML will generate the subscription form HTML code for you
					automatically using the name you create. So while you might want to
					name a field firstName, users signing up for your form might find
					that a bit confusing. Instead you can use First Name or first name,
					and SML will generate the proper code accordingly.</p>
			</div>
			<div id="build">
				<h2>Add Fields</h2>
				<form method="post" action="data_collection_check.php">
					<select name="Select1" id="item">
						<option>Select a field</option>
						<option value="text">Text</option>
						<option value="textarea">Textarea</option>
						<option value="radio">Radio Button Group</option>
						<option value="checkbox">Checkbox Group</option>
						<option value="dropdown">Drop Down Menu</option>
					</select>
					<div id="fields"></div>
					<button type="submit" id="submit" name="submit"
						style="margin-top: 10px; font-size: 1.6em">
						<img style="vertical-align: middle;" src="images/bullet-add.png">Add
						Field(s)
					</button>
				</form>
			</div>
			<div id="display">
				<h2>Available Fields</h2>
				<?php
				$result = $database->getDataCollectionFields();
				if(!is_null($result))
				{
					while($row=mysql_fetch_array($result))
					{
						echo "<div class=\"collector\">";
						echo "<img src=\"images/form-$row[type].png\" style=\"vertical-align:middle\" title=\"$row[type]\" alt=\"$row[type]\"> $row[name]";
						if($row['required']) echo " (required)";
						echo " <a href=\"$_SERVER[PHP_SELF]?d=$row[id]\"><img src=\"images/delete.png\" style=\"vertical-align:middle;border:none\" alt=\"Delete\" title=\"Delete\"></a></div>\n";
					}
				}
				?>
			</div>
		</div>
		<!-- #content -->
	</div>
	<!-- #content_container -->
	<p id="copyright">&copy; www.notonebit.com</p>
</body>
=======
<?php
include('database.php');

if($_GET['d'])
{
	$database->deleteDataField($_GET['d']);

	// Remove deleted field from all lists.
	$result = $database->getListNames();
	while($row = mysql_fetch_array($result))
	{
		$fields=explode(',',$row['data_fields']);
		if(in_array($_GET['d'],$fields))
		{
			$key=array_search($_GET['d'],$fields);
			unset($fields[$key]);
		}
		$imploded = implode(',',$fields);
		$database->updateListDataField($row[id],$imploded);
	}
}
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Simple Mailing List - Data Collection</title>
<script src="js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="js/data_collection.js"></script>

<link type="text/css" href="css/data_collection.css" rel="stylesheet">

</head>

<body>
	<div id="content_container">
		<?php
		include('nav.php');
		?>
		<div id="content">

			<p id="subnav">
				<a class="noborder" href="list_management_add.php"><img
					src="images/add-list.png" alt="Add a list" /> </a> <a
					class="noborder" href="list_management_edit.php"><img
					src="images/edit-delete-list.png" alt="Edit/Delete a list" /> </a>
				<a class="noborder" href="data_collection.php"><img
					src="images/data-collection.png" alt="Add a list" /> </a>
			</p>
			<div class="helpText">
				<p>While building a subscriber base and sending messages are the
					core of SML, you may find yourself looking to collect additional
					data on your subscribers. Whether it&#39;s just their name or more
					detailed information, SML allows you to collect any kind of data on
					your subscribers. By using the Data Collection feature, you can
					create a variety of input fields which you can present to your
					subscribers to complete. Fields can be required or optional and can
					be used when sending messages as well (e.g. Dear [[first-name]],).</p>
				<p>Here is where you will create the fields which you can pick from
					in the lists that you create. You create and delete your fields
					here, and then add or remove them to a list on the list edit page.
					Multiple fields can be added simultaneously. Note that when you
					give your data field a name, you may want to make it human readable
					as SML will generate the subscription form HTML code for you
					automatically using the name you create. So while you might want to
					name a field firstName, users signing up for your form might find
					that a bit confusing. Instead you can use First Name or first name,
					and SML will generate the proper code accordingly.</p>
			</div>
			<div id="build">
				<h2>Add Fields</h2>
				<form method="post" action="data_collection_check.php">
					<select name="Select1" id="item">
						<option>Select a field</option>
						<option value="text">Text</option>
						<option value="textarea">Textarea</option>
						<option value="radio">Radio Button Group</option>
						<option value="checkbox">Checkbox Group</option>
						<option value="dropdown">Drop Down Menu</option>
					</select>
					<div id="fields"></div>
					<button type="submit" id="submit" name="submit"
						style="margin-top: 10px; font-size: 1.6em">
						<img style="vertical-align: middle;" src="images/bullet-add.png">Add
						Field(s)
					</button>
				</form>
			</div>
			<div id="display">
				<h2>Available Fields</h2>
				<?php
				$result = $database->getDataCollectionFields();
				if(!is_null($result))
				{
					while($row=mysql_fetch_array($result))
					{
						echo "<div class=\"collector\">";
						echo "<img src=\"images/form-$row[type].png\" style=\"vertical-align:middle\" title=\"$row[type]\" alt=\"$row[type]\"> $row[name]";
						if($row['required']) echo " (required)";
						echo " <a href=\"$_SERVER[PHP_SELF]?d=$row[id]\"><img src=\"images/delete.png\" style=\"vertical-align:middle;border:none\" alt=\"Delete\" title=\"Delete\"></a></div>\n";
					}
				}
				?>
			</div>
		</div>
		<!-- #content -->
	</div>
	<!-- #content_container -->
	<p id="copyright">&copy; www.notonebit.com</p>
</body>
>>>>>>> b59280b13ce027f89f28f2d12e991e62fe2dd1bd
</html>
