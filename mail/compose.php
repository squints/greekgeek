<<<<<<< HEAD
<?php
include('database.php');
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Simple Mailing List - Compose Message</title>

<link type="text/css" href="css/custom-theme/jquery-ui-1.8.2.custom.css"
	rel="stylesheet">
<link type="text/css" href="css/compose.css" rel="stylesheet">

</head>

<body>

	<div id="content_container">
		<?php
		include('nav.php');
		?>
		<div id="content">
			<p id="notification"></p>
			<p class="helpText">Compose your message, select a list to send it
				to, and let SML do the rest. Of course you can do more than that as
				well. Add attachments, save drafts, load templates, schedule
				delivery for a future date, toggle between the rich text editor and
				plain text editor, send a test mailing, and more.</p>
			<?php
			$num_lists = $database->getListCount();
			if($num_lists == 1) $one_list=' class="oneList"';
			if($num_lists == NULL Or $num_lists == 0) exit("Hey, you don't have any lists! Please <a href=\"list_management_add.php\">create at least one list</a> prior to composing a message.\n</body>\n</html>");

			// draft check
			$num_drafts = $database->getNumDrafts();
			echo ($num_drafts > 0) ? "<p id=\"drafts\">":"<p id=\"drafts\" style=\"display:none\">";
			echo "Drafts: <a id=\"loadDrafts\" href=\"#\">$num_drafts</a></p>";
			?>

			<form method="post" action="compose_process.php">
				<div id="lists" <?php echo $one_list;?>>
					<h3>
						<img src="images/lists.png" alt="Lists" />
					</h3>
					<p>
						<?php
						if($num_lists > 1) echo "<label><input type=\"checkbox\" id=\"all\" name=\"selectall\" value=\"\">Select All/None</label><br>\n";
						$result = $database->getListNames();
						while($row=mysql_fetch_array($result))
						{
							$checked = ($num_lists == 1) ? ' checked="checked"':'';
							echo "<label style=\"margin-left:16px;\" id=\"list-$row[id]\"";
							if(!$row[status]) echo " title=\"closed list\" class=\"closed\" ";
							echo "><input type=\"checkbox\" name=\"lists[]\" value=\"$row[id]\"$checked>$row[name]";
							//echo (!empty($row[description])) ? " - $row[description]":"";
							echo " $member_count</label><br>\n";
						}
						?>
					</p>
				</div>
				<!-- #lists -->

				<div id="compose" <?php echo $one_list;?>>
					<?php
					if($_GET['id']>0) // Resending a message from the archive. Populate fields.
					{
						$message=$database->getArchivedMessage($_GET['id']);
						$row=mysql_fetch_array($message);
					}
					?>
					<label for="subject">Subject</label><input <?php echo $one_list;?>
						id="subject" name="subject" type="text" size="100" tabindex="1"
						value="<?php echo $row[subject]; ?>"><br>
					<button type="button" id="toggleRTE" tabindex="6"
						style="float: right; margin-top: 8px">
						<img src="images/toggle.png" alt="toggle editor"
							style="vertical-align: middle"> Toggle Editor
					</button>
					<button type="button" id="addAttachment" tabindex="5"
						style="float: right; margin-top: 8px">
						<img src="images/attachment.png" alt="add attachment"
							style="vertical-align: middle"> Add Attachment
					</button>
					<button type="button" id="loadTemplate" tabindex="4"
						style="float: right; margin-top: 8px">
						<img src="images/load.png" alt="load template"
							style="vertical-align: middle"> Load Template
					</button>
					<button type="button" id="saveTemplate" tabindex="3"
						style="float: right; margin-top: 8px">
						<img src="images/save.png" alt="save template"
							style="vertical-align: middle"> Save Template
					</button>
					<button type="button" id="saveDraft" tabindex="2"
						style="float: right; margin-top: 8px">
						<img src="images/save.png" alt="save draft"
							style="vertical-align: middle"> Save Draft
					</button>
					<label for="message" style="margin-top: 8px;">Message</label>
					<textarea <?php echo $one_list;?> id="message" name="message"
						tabindex="7"><?php echo $row[message]; ?></textarea>
					<p id="data_fields"></p>
					<p style="float: right; font-size: .9em">
						<a id="send_test" href="">Send test</a>
					</p>
					<p>
						Message will be sent to <span id="recipient_count">0 recipients</span>.
					</p>
					<div id="attachments">
						<ul></ul>
					</div>
					<div id="nownlater">
						Begin delivery: <label><input name="when" type="radio" value="now"
							checked="checked">Now</label> <label><input name="when"
							type="radio" value="later">Later</label>
					</div>
					<div id="schedule">
						Date: <input type="text" id="datepicker" name="datepicker"> Time:
						<select name="hour">
							<option>12</option>
							<option>1</option>
							<option>2</option>
							<option>3</option>
							<option>4</option>
							<option>5</option>
							<option>6</option>
							<option>7</option>
							<option>8</option>
							<option>9</option>
							<option>10</option>
							<option>11</option>
						</select> <select name="minute">
							<option>00</option>
							<option>15</option>
							<option>30</option>
							<option>45</option>
						</select> <select name="ampm">
							<option>am</option>
							<option>pm</option>
						</select><br> <span id="serverTimeContainer">(current server time:
							<span id="serverTime"></span>)
						</span>
					</div>
					<button id="send" name="send">Send</button>
				</div>
				<!-- #compose -->
			</form>
			<div>
				<!-- #container -->
			</div>
			<!-- #content_container -->

			<div id="loadTemplateDialog" title="Template Chooser">
				<p>No template data loaded</p>
			</div>
			<div id="saveTemplateDialog" title="Save Template">
				<form>
					<p>
						File name to save current content as: <input id="saveTemplateName"
							type="text">
						<button id="save">Save</button>
					</p>
				</form>
			</div>
			<div id="loadDraftsDialog" title="Drafts">
				<p>No draft data loaded</p>
			</div>
			<div id="attachmentDialog" title="Add Attachments">
				<form method="post" action="upload.php" target="process_attachments"
					enctype="multipart/form-data">
					Attachments:<input name="attachment[]" id="attachment" type="file"
						multiple="true">
					<iframe name="process_attachments" id="process_attachments"
						style="display: none"></iframe>
					<input type="submit" value="Add Attachment(s)"><br>Maximum file
					size:
					<?php echo ini_get('upload_max_filesize');?>
				</form>
			</div>
			<div id="sendTestDialog" title="Send Test Message">
				<form>
					<p>
						Send a copy of the message to: <input id="sendTestAddress"
							type="text" style="width: 200px"
							value="<?php echo ADMIN_EMAIL;?>">
						<button type="button" id="testSend">Send</button>
					</p>
				</form>
				<p id="testResult"></p>
			</div>
			<div id="fieldNoteDialog" title="Data Fields Note">
				<p>When you send a message that has data fields in it, SML will
					replace the fields with the data collected on your subscribers.
					Note however that when there is no subscriber data available for
					someone, that a data field will be replaced with a blank space. For
					example, if you send a message with the text:
				
				
				<pre>Dear [[first-name]],</pre>
				and there is no data for the subscriber, they would receive the text
				as
				<pre>Dear ,</pre>
				</p>
				<p>Also note that fields are not replaced during a test send.</p>
			</div>
			<script src="js/jquery-1.6.2.min.js"></script>
			<script src="js/jquery-ui-1.8.custom.min.js"></script>
			<script src="js/compose.js"></script>
			<script src="js/editor/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>

</body>
=======
<?php
include('database.php');
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Simple Mailing List - Compose Message</title>

<link type="text/css" href="css/custom-theme/jquery-ui-1.8.2.custom.css"
	rel="stylesheet">
<link type="text/css" href="css/compose.css" rel="stylesheet">

</head>

<body>

	<div id="content_container">
		<?php
		include('nav.php');
		?>
		<div id="content">
			<p id="notification"></p>
			<p class="helpText">Compose your message, select a list to send it
				to, and let SML do the rest. Of course you can do more than that as
				well. Add attachments, save drafts, load templates, schedule
				delivery for a future date, toggle between the rich text editor and
				plain text editor, send a test mailing, and more.</p>
			<?php
			$num_lists = $database->getListCount();
			if($num_lists == 1) $one_list=' class="oneList"';
			if($num_lists == NULL Or $num_lists == 0) exit("Hey, you don't have any lists! Please <a href=\"list_management_add.php\">create at least one list</a> prior to composing a message.\n</body>\n</html>");

			// draft check
			$num_drafts = $database->getNumDrafts();
			echo ($num_drafts > 0) ? "<p id=\"drafts\">":"<p id=\"drafts\" style=\"display:none\">";
			echo "Drafts: <a id=\"loadDrafts\" href=\"#\">$num_drafts</a></p>";
			?>

			<form method="post" action="compose_process.php">
				<div id="lists" <?php echo $one_list;?>>
					<h3>
						<img src="images/lists.png" alt="Lists" />
					</h3>
					<p>
						<?php
						if($num_lists > 1) echo "<label><input type=\"checkbox\" id=\"all\" name=\"selectall\" value=\"\">Select All/None</label><br>\n";
						$result = $database->getListNames();
						while($row=mysql_fetch_array($result))
						{
							$checked = ($num_lists == 1) ? ' checked="checked"':'';
							echo "<label style=\"margin-left:16px;\" id=\"list-$row[id]\"";
							if(!$row[status]) echo " title=\"closed list\" class=\"closed\" ";
							echo "><input type=\"checkbox\" name=\"lists[]\" value=\"$row[id]\"$checked>$row[name]";
							//echo (!empty($row[description])) ? " - $row[description]":"";
							echo " $member_count</label><br>\n";
						}
						?>
					</p>
				</div>
				<!-- #lists -->

				<div id="compose" <?php echo $one_list;?>>
					<?php
					if($_GET['id']>0) // Resending a message from the archive. Populate fields.
					{
						$message=$database->getArchivedMessage($_GET['id']);
						$row=mysql_fetch_array($message);
					}
					?>
					<label for="subject">Subject</label><input <?php echo $one_list;?>
						id="subject" name="subject" type="text" size="100" tabindex="1"
						value="<?php echo $row[subject]; ?>"><br>
					<button type="button" id="toggleRTE" tabindex="6"
						style="float: right; margin-top: 8px">
						<img src="images/toggle.png" alt="toggle editor"
							style="vertical-align: middle"> Toggle Editor
					</button>
					<button type="button" id="addAttachment" tabindex="5"
						style="float: right; margin-top: 8px">
						<img src="images/attachment.png" alt="add attachment"
							style="vertical-align: middle"> Add Attachment
					</button>
					<button type="button" id="loadTemplate" tabindex="4"
						style="float: right; margin-top: 8px">
						<img src="images/load.png" alt="load template"
							style="vertical-align: middle"> Load Template
					</button>
					<button type="button" id="saveTemplate" tabindex="3"
						style="float: right; margin-top: 8px">
						<img src="images/save.png" alt="save template"
							style="vertical-align: middle"> Save Template
					</button>
					<button type="button" id="saveDraft" tabindex="2"
						style="float: right; margin-top: 8px">
						<img src="images/save.png" alt="save draft"
							style="vertical-align: middle"> Save Draft
					</button>
					<label for="message" style="margin-top: 8px;">Message</label>
					<textarea <?php echo $one_list;?> id="message" name="message"
						tabindex="7"><?php echo $row[message]; ?></textarea>
					<p id="data_fields"></p>
					<p style="float: right; font-size: .9em">
						<a id="send_test" href="">Send test</a>
					</p>
					<p>
						Message will be sent to <span id="recipient_count">0 recipients</span>.
					</p>
					<div id="attachments">
						<ul></ul>
					</div>
					<div id="nownlater">
						Begin delivery: <label><input name="when" type="radio" value="now"
							checked="checked">Now</label> <label><input name="when"
							type="radio" value="later">Later</label>
					</div>
					<div id="schedule">
						Date: <input type="text" id="datepicker" name="datepicker"> Time:
						<select name="hour">
							<option>12</option>
							<option>1</option>
							<option>2</option>
							<option>3</option>
							<option>4</option>
							<option>5</option>
							<option>6</option>
							<option>7</option>
							<option>8</option>
							<option>9</option>
							<option>10</option>
							<option>11</option>
						</select> <select name="minute">
							<option>00</option>
							<option>15</option>
							<option>30</option>
							<option>45</option>
						</select> <select name="ampm">
							<option>am</option>
							<option>pm</option>
						</select><br> <span id="serverTimeContainer">(current server time:
							<span id="serverTime"></span>)
						</span>
					</div>
					<button id="send" name="send">Send</button>
				</div>
				<!-- #compose -->
			</form>
			<div>
				<!-- #container -->
			</div>
			<!-- #content_container -->

			<div id="loadTemplateDialog" title="Template Chooser">
				<p>No template data loaded</p>
			</div>
			<div id="saveTemplateDialog" title="Save Template">
				<form>
					<p>
						File name to save current content as: <input id="saveTemplateName"
							type="text">
						<button id="save">Save</button>
					</p>
				</form>
			</div>
			<div id="loadDraftsDialog" title="Drafts">
				<p>No draft data loaded</p>
			</div>
			<div id="attachmentDialog" title="Add Attachments">
				<form method="post" action="upload.php" target="process_attachments"
					enctype="multipart/form-data">
					Attachments:<input name="attachment[]" id="attachment" type="file"
						multiple="true">
					<iframe name="process_attachments" id="process_attachments"
						style="display: none"></iframe>
					<input type="submit" value="Add Attachment(s)"><br>Maximum file
					size:
					<?php echo ini_get('upload_max_filesize');?>
				</form>
			</div>
			<div id="sendTestDialog" title="Send Test Message">
				<form>
					<p>
						Send a copy of the message to: <input id="sendTestAddress"
							type="text" style="width: 200px"
							value="<?php echo ADMIN_EMAIL;?>">
						<button type="button" id="testSend">Send</button>
					</p>
				</form>
				<p id="testResult"></p>
			</div>
			<div id="fieldNoteDialog" title="Data Fields Note">
				<p>When you send a message that has data fields in it, SML will
					replace the fields with the data collected on your subscribers.
					Note however that when there is no subscriber data available for
					someone, that a data field will be replaced with a blank space. For
					example, if you send a message with the text:
				
				
				<pre>Dear [[first-name]],</pre>
				and there is no data for the subscriber, they would receive the text
				as
				<pre>Dear ,</pre>
				</p>
				<p>Also note that fields are not replaced during a test send.</p>
			</div>
			<script src="js/jquery-1.6.2.min.js"></script>
			<script src="js/jquery-ui-1.8.custom.min.js"></script>
			<script src="js/compose.js"></script>
			<script src="js/editor/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>

</body>
>>>>>>> b59280b13ce027f89f28f2d12e991e62fe2dd1bd
</html>
