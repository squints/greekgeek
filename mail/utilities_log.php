<<<<<<< HEAD
<?php
include('database.php');
include('paginator.class.php');
if($_GET['a']=='d') // Delete a log entry
{
	$database->deleteLogItem($_GET['ts']);
}
if( (int) $_POST['purge_amt'] > 0) // Purge logs
{
	$database->purgeLogs($_POST['purge_amt']);
}
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Simple Mailing List - Log</title>

<link type="text/css" href="css/custom-theme/jquery-ui-1.8.2.custom.css"
	rel="stylesheet">
<link type="text/css" href="css/utilities.css" rel="stylesheet">

</head>

<body>
	<div id="content_container">
		<?php
		include('nav.php');
		?>
		<div id="content">
			<p id="subnav">
				<a class="noborder" href="utilities_log.php"><img
					src="images/logs.png" alt="Logs" /> </a> <a class="noborder"
					href="utilities_smtp.php"><img src="images/smtp-diagnostic.png"
					alt="SMTP diagnostic" /> </a> <a class="noborder"
					href="utilities_imap.php"><img src="images/imap-diagnostic.png"
					alt="IMAP diagnostic" /> </a>
			</p>
			<p id="notification"></p>

			<?php
			$num_messages = $database->getLogCount();
			$pages = new Paginator;
			$pages->items_total = $num_messages;
			$pages->mid_range = 5;
			$pages->paginate();
			if($num_messages > $pages->items_per_page)
			{
				echo "<p>".$pages->display_pages();
				echo $pages->display_jump_menu();
				echo $pages->display_items_per_page()."</p>\n";
			}
			echo "<input type=\"hidden\" name=\"paginator_ipp\" value=\"$pages->items_per_page\">\n";
			echo "<input type=\"hidden\" name=\"paginator_page\" value=\"$pages->current_page\">\n";
			echo "<input type=\"hidden\" name=\"paginator_total\" value=\"$pages->items_total\">\n";
			echo "<form method=\"post\" action=\"$_SERVER[PHP_SELF]\">Purge logs? Delete entries older than <input type=\"text\" name=\"purge_amt\" size=\"4\" /> days <input name=\"purge\" value=\"Purge\" type=\"submit\" /></form>\n";
			?>
			<table style="width: 100%" id="log_table">
				<tr>
					<th>Date</th>
					<th>Description</th>
					<th></th>
					<th></th>
				</tr>
				<?php
				// grab all log entries and display from newest to oldest
				// $messages = $database->getArchiveList($pages->limit);
				$result = $database->getLogContents($pages->limit);
				if(!is_null($result))
				{
					while($row=mysql_fetch_array($result))
					{
						echo "<tr><td>" .date("F j, Y, g:i a", $row['timestamp']) . "</td>";
						echo "<td>${row['event']}</td>";
						echo "<td class=\"center\"><a class=\"detail\" href=\"{$row['timestamp']}\"><img src=\"images/detail.png\" title=\"view\" alt=\"view\"></a></td>";
						echo "<td class=\"center\"><a class=\"delete\" href=\"{$row['timestamp']}\"><img src=\"images/delete.png\" title=\"delete\" alt=\"delete\"></a></td></tr>\n";
				}
}
else
{
	echo "<p>Log is empty.</p>\n";
}
?>
			</table>
			<?php
			if($num_messages > $pages->items_per_page)
			{
				echo "<p>".$pages->display_pages();
				echo $pages->display_jump_menu();
				echo $pages->display_items_per_page()."</p>";
			}
			?>

		</div>
		<!-- #content -->
	</div>
	<!-- #content_container -->
	<p id="copyright">&copy; www.notonebit.com</p>

	<div id="logDetail" title="Log Item Detail">
		<p>No data loaded</p>
	</div>
	<script src="js/jquery-1.6.2.min.js"></script>
	<script src="js/jquery-ui-1.8.custom.min.js"></script>
	<script src="js/utilities.js"></script>
</body>
=======
<?php
include('database.php');
include('paginator.class.php');
if($_GET['a']=='d') // Delete a log entry
{
	$database->deleteLogItem($_GET['ts']);
}
if( (int) $_POST['purge_amt'] > 0) // Purge logs
{
	$database->purgeLogs($_POST['purge_amt']);
}
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Simple Mailing List - Log</title>

<link type="text/css" href="css/custom-theme/jquery-ui-1.8.2.custom.css"
	rel="stylesheet">
<link type="text/css" href="css/utilities.css" rel="stylesheet">

</head>

<body>
	<div id="content_container">
		<?php
		include('nav.php');
		?>
		<div id="content">
			<p id="subnav">
				<a class="noborder" href="utilities_log.php"><img
					src="images/logs.png" alt="Logs" /> </a> <a class="noborder"
					href="utilities_smtp.php"><img src="images/smtp-diagnostic.png"
					alt="SMTP diagnostic" /> </a> <a class="noborder"
					href="utilities_imap.php"><img src="images/imap-diagnostic.png"
					alt="IMAP diagnostic" /> </a>
			</p>
			<p id="notification"></p>

			<?php
			$num_messages = $database->getLogCount();
			$pages = new Paginator;
			$pages->items_total = $num_messages;
			$pages->mid_range = 5;
			$pages->paginate();
			if($num_messages > $pages->items_per_page)
			{
				echo "<p>".$pages->display_pages();
				echo $pages->display_jump_menu();
				echo $pages->display_items_per_page()."</p>\n";
			}
			echo "<input type=\"hidden\" name=\"paginator_ipp\" value=\"$pages->items_per_page\">\n";
			echo "<input type=\"hidden\" name=\"paginator_page\" value=\"$pages->current_page\">\n";
			echo "<input type=\"hidden\" name=\"paginator_total\" value=\"$pages->items_total\">\n";
			echo "<form method=\"post\" action=\"$_SERVER[PHP_SELF]\">Purge logs? Delete entries older than <input type=\"text\" name=\"purge_amt\" size=\"4\" /> days <input name=\"purge\" value=\"Purge\" type=\"submit\" /></form>\n";
			?>
			<table style="width: 100%" id="log_table">
				<tr>
					<th>Date</th>
					<th>Description</th>
					<th></th>
					<th></th>
				</tr>
				<?php
				// grab all log entries and display from newest to oldest
				// $messages = $database->getArchiveList($pages->limit);
				$result = $database->getLogContents($pages->limit);
				if(!is_null($result))
				{
					while($row=mysql_fetch_array($result))
					{
						echo "<tr><td>" .date("F j, Y, g:i a", $row['timestamp']) . "</td>";
						echo "<td>${row['event']}</td>";
						echo "<td class=\"center\"><a class=\"detail\" href=\"{$row['timestamp']}\"><img src=\"images/detail.png\" title=\"view\" alt=\"view\"></a></td>";
						echo "<td class=\"center\"><a class=\"delete\" href=\"{$row['timestamp']}\"><img src=\"images/delete.png\" title=\"delete\" alt=\"delete\"></a></td></tr>\n";
				}
}
else
{
	echo "<p>Log is empty.</p>\n";
}
?>
			</table>
			<?php
			if($num_messages > $pages->items_per_page)
			{
				echo "<p>".$pages->display_pages();
				echo $pages->display_jump_menu();
				echo $pages->display_items_per_page()."</p>";
			}
			?>

		</div>
		<!-- #content -->
	</div>
	<!-- #content_container -->
	<p id="copyright">&copy; www.notonebit.com</p>

	<div id="logDetail" title="Log Item Detail">
		<p>No data loaded</p>
	</div>
	<script src="js/jquery-1.6.2.min.js"></script>
	<script src="js/jquery-ui-1.8.custom.min.js"></script>
	<script src="js/utilities.js"></script>
</body>
>>>>>>> b59280b13ce027f89f28f2d12e991e62fe2dd1bd
</html>
