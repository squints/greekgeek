<<<<<<< HEAD
<?php
include('database.php');
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Simple Mailing List - List Management</title>

<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.custom.css"
	rel="stylesheet">
<link type="text/css" href="css/lists.css" rel="stylesheet">

</head>

<body>
	<div id="content_container">
		<?php
		include('nav.php');
		?>
		<div id="content">
			<p id="subnav">
				<a class="noborder" href="list_management_add.php"><img
					src="images/add-list.png" alt="Add a list" /> </a> <a
					class="noborder" href="list_management_edit.php"><img
					src="images/edit-delete-list.png" alt="Edit/Delete a list" /> </a>
				<a class="noborder" href="data_collection.php"><img
					src="images/data-collection.png" alt="Add a list" /> </a>
			</p>
			<p id="notification"></p>
			<h1>
				You have
				<?php
				$list_count = $database->getListCount();
				echo ($list_count == 1) ? "one list":"$list_count lists";

				$confirmedMembers = $database->countTotalConfirmedMembers();
				if($confirmedMembers >=2) $plural = "s";
				echo ($confirmedMembers == 0) ? " serving absolutely no one.":" serving $confirmedMembers confirmed member$plural.";
				?>
			</h1>
			<div class="helpText">
				<p>While one lists is all that&#39;s required to use SML, you can
					create as many lists as you like. You can create a list for
					anything and each list can have it&#39;s own owner
					(&quot;from&quot; address), description, and form variables. You
					can close lists so that no new signups are allowed unless you
					import address yourself. Messages you write can be sent to any of
					your lists and anyone subscribing to multiple lists will only
					receive one copy of the message.</p>
				<p>You can also create data fields to collect data from subscribers.
					For example, you could build a small form using SML&#39;s data
					collection feature to ask your subscribers for their first name and
					the country they live in. Fields can be setup as required or
					optional and all HTML form fields can be used. SML does the heavy
					lifting and build the forms for you which you can just copy and
					paste into any web page. When you send a message to a list that
					collects data, SML will show you any data fields you can use within
					the message. When you send a message, SML will merge the data in
					your database with the fields in your message which enables you to
					among other things create personalized messaged.</p>
			</div>
		</div>
	</div>
	<p id="copyright">&copy; www.notonebit.com</p>
	<script src="js/jquery-1.6.2.min.js"></script>
	<script src="js/jquery-ui-1.8.custom.min.js"></script>
	<script src="js/lists.js"></script>
</body>
=======
<?php
include('database.php');
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Simple Mailing List - List Management</title>

<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.custom.css"
	rel="stylesheet">
<link type="text/css" href="css/lists.css" rel="stylesheet">

</head>

<body>
	<div id="content_container">
		<?php
		include('nav.php');
		?>
		<div id="content">
			<p id="subnav">
				<a class="noborder" href="list_management_add.php"><img
					src="images/add-list.png" alt="Add a list" /> </a> <a
					class="noborder" href="list_management_edit.php"><img
					src="images/edit-delete-list.png" alt="Edit/Delete a list" /> </a>
				<a class="noborder" href="data_collection.php"><img
					src="images/data-collection.png" alt="Add a list" /> </a>
			</p>
			<p id="notification"></p>
			<h1>
				You have
				<?php
				$list_count = $database->getListCount();
				echo ($list_count == 1) ? "one list":"$list_count lists";

				$confirmedMembers = $database->countTotalConfirmedMembers();
				if($confirmedMembers >=2) $plural = "s";
				echo ($confirmedMembers == 0) ? " serving absolutely no one.":" serving $confirmedMembers confirmed member$plural.";
				?>
			</h1>
			<div class="helpText">
				<p>While one lists is all that&#39;s required to use SML, you can
					create as many lists as you like. You can create a list for
					anything and each list can have it&#39;s own owner
					(&quot;from&quot; address), description, and form variables. You
					can close lists so that no new signups are allowed unless you
					import address yourself. Messages you write can be sent to any of
					your lists and anyone subscribing to multiple lists will only
					receive one copy of the message.</p>
				<p>You can also create data fields to collect data from subscribers.
					For example, you could build a small form using SML&#39;s data
					collection feature to ask your subscribers for their first name and
					the country they live in. Fields can be setup as required or
					optional and all HTML form fields can be used. SML does the heavy
					lifting and build the forms for you which you can just copy and
					paste into any web page. When you send a message to a list that
					collects data, SML will show you any data fields you can use within
					the message. When you send a message, SML will merge the data in
					your database with the fields in your message which enables you to
					among other things create personalized messaged.</p>
			</div>
		</div>
	</div>
	<p id="copyright">&copy; www.notonebit.com</p>
	<script src="js/jquery-1.6.2.min.js"></script>
	<script src="js/jquery-ui-1.8.custom.min.js"></script>
	<script src="js/lists.js"></script>
</body>
>>>>>>> b59280b13ce027f89f28f2d12e991e62fe2dd1bd
</html>
