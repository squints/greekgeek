<<<<<<< HEAD
<?php
include('database.php');
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Simple Mailing List - IMAP diagnostics</title>

<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.custom.css"
	rel="stylesheet">
<link type="text/css" href="css/utilities.css" rel="stylesheet">

</head>

<body>
	<div id="content_container">
		<?php
		include('nav.php');
		?>
		<div id="content">
			<?php
			if (!extension_loaded('imap'))
			{
				echo "<strong>Warning: Your PHP installation does not appear to have the IMAP extension installed or enabled.</strong>";
				// exit('</body></html>');
			}
			?>
			<p id="subnav">
				<a class="noborder" href="utilities_log.php"><img
					src="images/logs.png" alt="Logs" /> </a> <a class="noborder"
					href="utilities_smtp.php"><img src="images/smtp-diagnostic.png"
					alt="SMTP diagnostic" /> </a> <a class="noborder"
					href="utilities_imap.php"><img src="images/imap-diagnostic.png"
					alt="IMAP diagnostic" /> </a>
			</p>
			<p id="notification"></p>
			<div class="helpText">
				<p>
					By setting up an email account that you can access via IMAP, you
					can check for bounced messages. <acronym
						title="Internet Message Access Protocol">IMAP</acronym> is a way
					to retrieve email. IMAP and <acronym title="Post Office Protocal">POP</acronym>
					are the two most popular methods for retrieve Internet email.
				</p>
				<p>
					This utility is designed to help you determine your IMAP settings.
					SML uses a mailbox you designate to check for returned email based
					on your IMAP settings. <strong>The most common problem with not
						being able to access your bounce mailbox via PHP is not having the
						proper IMAP flags set.</strong> Your IMAP flags are the specific
					options that your IMAP server is configured with. Flags are most
					often needed when you&#39;re connecting to a POP3 server instead of
					an IMAP server and when you&#39;re connecting using SSL. The most
					common IMAP ports are 143, 993 (IMAP with SSL), and 995 (POP3 with
					SSL). Visit <a href="#">this topic at notonebit.com's forum</a> for
					tips on configuring your IMAP mailbox.
				</p>
			</div>

			<table>
				<tr>
					<td>IMAP Server</td>
					<td><input name="imap_server" type="text"></td>
					<td id="results" rowspan="5"
						style="vertical-align: top; overflow: auto;"></td>
				</tr>
				<tr>
					<td>IMAP Port</td>
					<td><input name="imap_port" type="text"></td>
				</tr>
				<tr>
					<td>IMAP Folder</td>
					<td><input name="imap_folder" value="INBOX" type="text"></td>
				</tr>
				<tr>
					<td>IMAP Username (address)</td>
					<td><input name="imap_user" type="text"></td>
				</tr>
				<tr>
					<td>IMAP Password</td>
					<td><input name="imap_pass" type="text"></td>
				</tr>
				<tr>
					<td>IMAP Flags</td>
					<td colspan="2"><label><input name="imap_flags[]" value="/pop3"
							type="checkbox">Connect using POP3 instead of IMAP</label><br> <label><input
							name="imap_flags[]" value="/ssl" type="checkbox">SSL</label><br>
						<label><input name="imap_flags[]" value="/secure" type="checkbox">Secure</label><br>
						<label><input name="imap_flags[]" value="/novalidate-cert"
							type="checkbox">Self-signed Certificate(for TLS/SSL. if server
							uses self-signed, check this)</label><br> <label><input
							name="imap_flags[]" value="/tls" type="checkbox">Force start-TLS</label><br>
						<label><input name="imap_flags[]" value="/notls" type="checkbox">Do
							no use start-TLS</label><br> <label><input name="imap_flags[]"
							value="/debug" type="checkbox">Debug</label></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td colspan="2"><input name="imap_submit" value="Test"
						type="submit">
					</td>
				</tr>
			</table>


		</div>
	</div>
	<p id="copyright">&copy; www.notonebit.com</p>
	<script src="js/jquery-1.6.2.min.js"></script>
	<script src="js/jquery-ui-1.8.custom.min.js"></script>
	<script src="js/utilities.js"></script>
</body>
=======
<?php
include('database.php');
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Simple Mailing List - IMAP diagnostics</title>

<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.custom.css"
	rel="stylesheet">
<link type="text/css" href="css/utilities.css" rel="stylesheet">

</head>

<body>
	<div id="content_container">
		<?php
		include('nav.php');
		?>
		<div id="content">
			<?php
			if (!extension_loaded('imap'))
			{
				echo "<strong>Warning: Your PHP installation does not appear to have the IMAP extension installed or enabled.</strong>";
				// exit('</body></html>');
			}
			?>
			<p id="subnav">
				<a class="noborder" href="utilities_log.php"><img
					src="images/logs.png" alt="Logs" /> </a> <a class="noborder"
					href="utilities_smtp.php"><img src="images/smtp-diagnostic.png"
					alt="SMTP diagnostic" /> </a> <a class="noborder"
					href="utilities_imap.php"><img src="images/imap-diagnostic.png"
					alt="IMAP diagnostic" /> </a>
			</p>
			<p id="notification"></p>
			<div class="helpText">
				<p>
					By setting up an email account that you can access via IMAP, you
					can check for bounced messages. <acronym
						title="Internet Message Access Protocol">IMAP</acronym> is a way
					to retrieve email. IMAP and <acronym title="Post Office Protocal">POP</acronym>
					are the two most popular methods for retrieve Internet email.
				</p>
				<p>
					This utility is designed to help you determine your IMAP settings.
					SML uses a mailbox you designate to check for returned email based
					on your IMAP settings. <strong>The most common problem with not
						being able to access your bounce mailbox via PHP is not having the
						proper IMAP flags set.</strong> Your IMAP flags are the specific
					options that your IMAP server is configured with. Flags are most
					often needed when you&#39;re connecting to a POP3 server instead of
					an IMAP server and when you&#39;re connecting using SSL. The most
					common IMAP ports are 143, 993 (IMAP with SSL), and 995 (POP3 with
					SSL). Visit <a href="#">this topic at notonebit.com's forum</a> for
					tips on configuring your IMAP mailbox.
				</p>
			</div>

			<table>
				<tr>
					<td>IMAP Server</td>
					<td><input name="imap_server" type="text"></td>
					<td id="results" rowspan="5"
						style="vertical-align: top; overflow: auto;"></td>
				</tr>
				<tr>
					<td>IMAP Port</td>
					<td><input name="imap_port" type="text"></td>
				</tr>
				<tr>
					<td>IMAP Folder</td>
					<td><input name="imap_folder" value="INBOX" type="text"></td>
				</tr>
				<tr>
					<td>IMAP Username (address)</td>
					<td><input name="imap_user" type="text"></td>
				</tr>
				<tr>
					<td>IMAP Password</td>
					<td><input name="imap_pass" type="text"></td>
				</tr>
				<tr>
					<td>IMAP Flags</td>
					<td colspan="2"><label><input name="imap_flags[]" value="/pop3"
							type="checkbox">Connect using POP3 instead of IMAP</label><br> <label><input
							name="imap_flags[]" value="/ssl" type="checkbox">SSL</label><br>
						<label><input name="imap_flags[]" value="/secure" type="checkbox">Secure</label><br>
						<label><input name="imap_flags[]" value="/novalidate-cert"
							type="checkbox">Self-signed Certificate(for TLS/SSL. if server
							uses self-signed, check this)</label><br> <label><input
							name="imap_flags[]" value="/tls" type="checkbox">Force start-TLS</label><br>
						<label><input name="imap_flags[]" value="/notls" type="checkbox">Do
							no use start-TLS</label><br> <label><input name="imap_flags[]"
							value="/debug" type="checkbox">Debug</label></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td colspan="2"><input name="imap_submit" value="Test"
						type="submit">
					</td>
				</tr>
			</table>


		</div>
	</div>
	<p id="copyright">&copy; www.notonebit.com</p>
	<script src="js/jquery-1.6.2.min.js"></script>
	<script src="js/jquery-ui-1.8.custom.min.js"></script>
	<script src="js/utilities.js"></script>
</body>
>>>>>>> b59280b13ce027f89f28f2d12e991e62fe2dd1bd
</html>
