<<<<<<< HEAD
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Table structure for table `sml_blacklist`
--

CREATE TABLE IF NOT EXISTS `sml_blacklist` (
  `rule` varchar(255) NOT NULL DEFAULT '',
  `list_id` int(11) NOT NULL DEFAULT '0',
  UNIQUE KEY `rule` (`rule`,`list_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sml_data_collection`
--

CREATE TABLE IF NOT EXISTS `sml_data_collection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(10) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `sanitized_name` varchar(255) NOT NULL DEFAULT '',
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `options` text NOT NULL,
  `selected` text NOT NULL,
  `multiple` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=46 ;

-- --------------------------------------------------------

--
-- Table structure for table `sml_delivery_queue`
--

CREATE TABLE IF NOT EXISTS `sml_delivery_queue` (
  `email` varchar(255) NOT NULL DEFAULT '',
  `message_id` int(11) NOT NULL DEFAULT '0',
  `timestamp` varchar(10) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sml_drafts`
--

CREATE TABLE IF NOT EXISTS `sml_drafts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` tinytext NOT NULL,
  `message` text NOT NULL,
  `timestamp` varchar(10) NOT NULL DEFAULT '',
  `format` varchar(4) NOT NULL DEFAULT '',
  `attachments` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=62 ;

-- --------------------------------------------------------

--
-- Table structure for table `sml_lists`
--

CREATE TABLE IF NOT EXISTS `sml_lists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `owner` varchar(100) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `data_fields` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=122 ;

-- --------------------------------------------------------

--
-- Table structure for table `sml_list_members`
--

CREATE TABLE IF NOT EXISTS `sml_list_members` (
  `address` varchar(255) NOT NULL DEFAULT '',
  `list_id` int(11) NOT NULL DEFAULT '0',
  `confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `data_collection` text,
  UNIQUE KEY `address` (`address`,`list_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sml_log`
--

CREATE TABLE IF NOT EXISTS `sml_log` (
  `timestamp` int(10) NOT NULL DEFAULT '0',
  `event` text CHARACTER SET utf8 NOT NULL,
  `result` text CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sml_members`
--

CREATE TABLE IF NOT EXISTS `sml_members` (
  `address` varchar(255) NOT NULL DEFAULT '',
  `memberkey` varchar(32) NOT NULL DEFAULT '',
  `bounces` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`address`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sml_message_archive`
--

CREATE TABLE IF NOT EXISTS `sml_message_archive` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) NOT NULL DEFAULT '',
  `message` text NOT NULL,
  `attachments` text NOT NULL,
  `lists` text NOT NULL,
  `start_send` varchar(10) NOT NULL DEFAULT '',
  `recipients` int(11) NOT NULL DEFAULT '0',
  `format` varchar(4) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  FULLTEXT KEY `subject` (`subject`,`message`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=171 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
=======
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Table structure for table `sml_blacklist`
--

CREATE TABLE IF NOT EXISTS `sml_blacklist` (
  `rule` varchar(255) NOT NULL DEFAULT '',
  `list_id` int(11) NOT NULL DEFAULT '0',
  UNIQUE KEY `rule` (`rule`,`list_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sml_data_collection`
--

CREATE TABLE IF NOT EXISTS `sml_data_collection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(10) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `sanitized_name` varchar(255) NOT NULL DEFAULT '',
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `options` text NOT NULL,
  `selected` text NOT NULL,
  `multiple` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=46 ;

-- --------------------------------------------------------

--
-- Table structure for table `sml_delivery_queue`
--

CREATE TABLE IF NOT EXISTS `sml_delivery_queue` (
  `email` varchar(255) NOT NULL DEFAULT '',
  `message_id` int(11) NOT NULL DEFAULT '0',
  `timestamp` varchar(10) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sml_drafts`
--

CREATE TABLE IF NOT EXISTS `sml_drafts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` tinytext NOT NULL,
  `message` text NOT NULL,
  `timestamp` varchar(10) NOT NULL DEFAULT '',
  `format` varchar(4) NOT NULL DEFAULT '',
  `attachments` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=62 ;

-- --------------------------------------------------------

--
-- Table structure for table `sml_lists`
--

CREATE TABLE IF NOT EXISTS `sml_lists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `owner` varchar(100) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `data_fields` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=122 ;

-- --------------------------------------------------------

--
-- Table structure for table `sml_list_members`
--

CREATE TABLE IF NOT EXISTS `sml_list_members` (
  `address` varchar(255) NOT NULL DEFAULT '',
  `list_id` int(11) NOT NULL DEFAULT '0',
  `confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `data_collection` text,
  UNIQUE KEY `address` (`address`,`list_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sml_log`
--

CREATE TABLE IF NOT EXISTS `sml_log` (
  `timestamp` int(10) NOT NULL DEFAULT '0',
  `event` text CHARACTER SET utf8 NOT NULL,
  `result` text CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sml_members`
--

CREATE TABLE IF NOT EXISTS `sml_members` (
  `address` varchar(255) NOT NULL DEFAULT '',
  `memberkey` varchar(32) NOT NULL DEFAULT '',
  `bounces` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`address`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sml_message_archive`
--

CREATE TABLE IF NOT EXISTS `sml_message_archive` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) NOT NULL DEFAULT '',
  `message` text NOT NULL,
  `attachments` text NOT NULL,
  `lists` text NOT NULL,
  `start_send` varchar(10) NOT NULL DEFAULT '',
  `recipients` int(11) NOT NULL DEFAULT '0',
  `format` varchar(4) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  FULLTEXT KEY `subject` (`subject`,`message`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=171 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
>>>>>>> b59280b13ce027f89f28f2d12e991e62fe2dd1bd
