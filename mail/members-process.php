<<<<<<< HEAD
<?php
include('database.php');
include('functions.php');
ini_set('max_execution_time',180); // You may need to increase this for importing very large numbers of members.

$invalid_addresses = array();
$valid_addresses = array();
$delimiter = (!empty($_POST['delimiter'])) ? $_POST['delimiter']:' ';
$count = 0;

if($_FILES['importFile']['error']==0)
{
	// file successfully uploaded
	$fileContent = file($_FILES['importFile']['tmp_name']);
}

if (!empty($_POST[addresses]))
{
	$addresses = explode($delimiter,$_POST[addresses]);
}
if(count($fileContent) > 0)
{
	$addresses = explode($delimiter,$fileContent[0]);
}

// Check to see if addresses were separated by newlines
preg_match_all('/@/',$addresses[0],$matches);
if(count($matches[0]) >=2 ) $addresses = explode("\n",$_POST[addresses]);
$total = count($addresses);
ob_start();
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title></title>
<script src="js/jquery-1.6.2.min.js"></script>
<script src="js/jquery-ui-1.8.custom.min.js"></script>

<link type="text/css" href="css/custom-theme/jquery-ui-1.8.2.custom.css"
	rel="stylesheet">
<link type="text/css" href="css/members.css" rel="stylesheet">
</head>
<body>
	<div id="content_container">
		<?php
		include('nav.php');
		?>
		<div id="content">

			<?php
			if($total >= 1000) echo "<p>Processing <span id=\"pct\"></span>% (<span id=\"count\"></span> of $total). Please wait. Keep this page open until processing is complete.</p>";
			myflush();
			foreach($addresses as $address)
			{
				if(!empty($address))
				{
					insertAddress(trim($address));
					$count++;
				}
				if($count%500==0) myflush();
			}
			myflush();

			function insertAddress($address)
			{
				global $database,$invalid_addresses,$valid_addresses;
				if (validate_address(trim($address)))
				{
					$okToInsert = false;
					foreach($_POST['lists'] as $list)
					{
						$blacklisted = false;
						if($_POST['bypass'] != 'on') $blacklisted = blacklist($address,$list);
						if(!$blacklisted)
						{
							$okToInsert = $database->insertAddressList($address,$list,1,'');
						}
						else
						{
							$database->log(time(),'Blacklist rule (members-process.php)',serialize($_POST));
						}
					}
					if($okToInsert)
					{
						if( is_null($database->getMemberInfo($address)) )
						{
							$result = $database->insertAddress($address,'');
							($result) ? $valid_addresses[] = $address:$invalid_addresses[$address] = 'Address already in database';
						}
						else $valid_addresses[] = $address; // Address already existed in members table. Insert skipped.
					}
					else
					{
						$invalid_addresses[$address] = 'Address may already exist in database or be blacklisted.';
						$database->log(time(),'Unable to insert address in sml_list_members table. May already exist or be blacklisted. (members-process.php)',serialize($_POST));
					}
				}
				else
				{
					//		$invalid_addresses[] = $address;
					$invalid_addresses[$address]='Not a valid email address format.';
					// log address as not passing regex
					$database->log(time(),'Regex fail (members-process.php)',$address);
				}
			}
			function myflush()
			{
				global $count,$total;
				echo(str_repeat(' ',256));
				$pct = intval( ($count/$total)*100 );
				echo "<script>document.getElementById('count').innerHTML = $count;document.getElementById('pct').innerHTML = $pct;</script>\n";

				if (ob_get_length())
				{
					@ob_flush();
					@flush();
					@ob_end_flush();
				}
				@ob_start();
			}

			if(count($valid_addresses) > 0)
			{
				echo "The following ".count($valid_addresses)." addresses were successfully imported:<br><ul>";
				foreach($valid_addresses as $address)
				{
					echo "<li>$address</li>\n";
				}
				echo "</ul>\n";
			}
			if(count($invalid_addresses) > 0)
			{
				echo "The following ".count($invalid_addresses)." addresses were not imported:<br><ul>";
				foreach($invalid_addresses as $address=>$reason)
				{
					echo "<li>$address - $reason</li>\n";
				}
				echo "</ul>\n";
			}
			?>
			<p>
				<a href="members.php">Click here</a> to return to the members page.
			</p>
		</div>
		<!-- #content -->
	</div>
	<!-- #content_container -->
</body>
=======
<?php
include('database.php');
include('functions.php');
ini_set('max_execution_time',180); // You may need to increase this for importing very large numbers of members.

$invalid_addresses = array();
$valid_addresses = array();
$delimiter = (!empty($_POST['delimiter'])) ? $_POST['delimiter']:' ';
$count = 0;

if($_FILES['importFile']['error']==0)
{
	// file successfully uploaded
	$fileContent = file($_FILES['importFile']['tmp_name']);
}

if (!empty($_POST[addresses]))
{
	$addresses = explode($delimiter,$_POST[addresses]);
}
if(count($fileContent) > 0)
{
	$addresses = explode($delimiter,$fileContent[0]);
}

// Check to see if addresses were separated by newlines
preg_match_all('/@/',$addresses[0],$matches);
if(count($matches[0]) >=2 ) $addresses = explode("\n",$_POST[addresses]);
$total = count($addresses);
ob_start();
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title></title>
<script src="js/jquery-1.6.2.min.js"></script>
<script src="js/jquery-ui-1.8.custom.min.js"></script>

<link type="text/css" href="css/custom-theme/jquery-ui-1.8.2.custom.css"
	rel="stylesheet">
<link type="text/css" href="css/members.css" rel="stylesheet">
</head>
<body>
	<div id="content_container">
		<?php
		include('nav.php');
		?>
		<div id="content">

			<?php
			if($total >= 1000) echo "<p>Processing <span id=\"pct\"></span>% (<span id=\"count\"></span> of $total). Please wait. Keep this page open until processing is complete.</p>";
			myflush();
			foreach($addresses as $address)
			{
				if(!empty($address))
				{
					insertAddress(trim($address));
					$count++;
				}
				if($count%500==0) myflush();
			}
			myflush();

			function insertAddress($address)
			{
				global $database,$invalid_addresses,$valid_addresses;
				if (validate_address(trim($address)))
				{
					$okToInsert = false;
					foreach($_POST['lists'] as $list)
					{
						$blacklisted = false;
						if($_POST['bypass'] != 'on') $blacklisted = blacklist($address,$list);
						if(!$blacklisted)
						{
							$okToInsert = $database->insertAddressList($address,$list,1,'');
						}
						else
						{
							$database->log(time(),'Blacklist rule (members-process.php)',serialize($_POST));
						}
					}
					if($okToInsert)
					{
						if( is_null($database->getMemberInfo($address)) )
						{
							$result = $database->insertAddress($address,'');
							($result) ? $valid_addresses[] = $address:$invalid_addresses[$address] = 'Address already in database';
						}
						else $valid_addresses[] = $address; // Address already existed in members table. Insert skipped.
					}
					else
					{
						$invalid_addresses[$address] = 'Address may already exist in database or be blacklisted.';
						$database->log(time(),'Unable to insert address in sml_list_members table. May already exist or be blacklisted. (members-process.php)',serialize($_POST));
					}
				}
				else
				{
					//		$invalid_addresses[] = $address;
					$invalid_addresses[$address]='Not a valid email address format.';
					// log address as not passing regex
					$database->log(time(),'Regex fail (members-process.php)',$address);
				}
			}
			function myflush()
			{
				global $count,$total;
				echo(str_repeat(' ',256));
				$pct = intval( ($count/$total)*100 );
				echo "<script>document.getElementById('count').innerHTML = $count;document.getElementById('pct').innerHTML = $pct;</script>\n";

				if (ob_get_length())
				{
					@ob_flush();
					@flush();
					@ob_end_flush();
				}
				@ob_start();
			}

			if(count($valid_addresses) > 0)
			{
				echo "The following ".count($valid_addresses)." addresses were successfully imported:<br><ul>";
				foreach($valid_addresses as $address)
				{
					echo "<li>$address</li>\n";
				}
				echo "</ul>\n";
			}
			if(count($invalid_addresses) > 0)
			{
				echo "The following ".count($invalid_addresses)." addresses were not imported:<br><ul>";
				foreach($invalid_addresses as $address=>$reason)
				{
					echo "<li>$address - $reason</li>\n";
				}
				echo "</ul>\n";
			}
			?>
			<p>
				<a href="members.php">Click here</a> to return to the members page.
			</p>
		</div>
		<!-- #content -->
	</div>
	<!-- #content_container -->
</body>
>>>>>>> b59280b13ce027f89f28f2d12e991e62fe2dd1bd
</html>
