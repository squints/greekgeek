<<<<<<< HEAD
<?php
$folder = "attachments";
$result = array();
$return = "";
$return_fail = "";

function format_size($size) {
	$sizes = array(" Bytes", " KB", " MB", " GB", " TB", " PB", " EB", " ZB", " YB");
	if ($size == 0) {
		return('n/a');
	} else {
		return (round($size/pow(1024, ($i = floor(log($size, 1024)))), 2) . $sizes[$i]);
	}
}

// First, see if the attachments folder is writable
if (is_writable($folder))
{
	// Iterate through files to upload
	foreach ($_FILES["attachment"]["error"] as $key => $error) // Loop through every uploaded file
	{
		$timestamp = time();
		$tmp_name = $_FILES["attachment"]["tmp_name"][$key];
		$name = $_FILES["attachment"]["name"][$key];
		$size = format_size($_FILES["attachment"]["size"][$key]);

		if ($error == UPLOAD_ERR_OK)
		{
			move_uploaded_file($tmp_name, $folder."/$timestamp-$name");
			$result["success"][]=array($name,$size);
		}
		else // The file had an error (http://us.php.net/manual/en/features.file-upload.errors.php)
		{
			$result["fail"][]=array($name,$size,file_upload_error_message($error));
			$database->log(time(),'Unable to upload file - (upload.php)',serialize($result));
		}
	}
	function file_upload_error_message($error_code)
	{
		switch ($error_code)
		{
			case UPLOAD_ERR_INI_SIZE:
				return 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
			case UPLOAD_ERR_FORM_SIZE:
				return 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
			case UPLOAD_ERR_PARTIAL:
				return 'The uploaded file was only partially uploaded';
			case UPLOAD_ERR_NO_FILE:
				return 'No file was uploaded';
			case UPLOAD_ERR_NO_TMP_DIR:
				return 'Missing a temporary folder';
			case UPLOAD_ERR_CANT_WRITE:
				return 'Failed to write file to disk';
			case UPLOAD_ERR_EXTENSION:
				return 'File upload stopped by extension';
			default:
				return 'Unknown upload error';
		}
	}

	// add loading image in the #attachments div when submitting

	if(is_array($result["success"]))
	{
		foreach($result["success"] as $key)
		{
			$return .= "<li>". $key[0] ." (". $key[1] . ") <a class=\"removeAttachment\" href=\"$timestamp-$key[0]\"><img src=\"images/delete.png\" alt=\"remove\"></a> <input type=\"hidden\" name=\"attachment[]\" value=\"$timestamp-$key[0]\"></li>";
		}
	}

	if(is_array($result["fail"]))
	{
		$return_fail .= "The following file(s) were not uploaded:<ul>";
		foreach($result["fail"] as $key)
		{
			$return_fail .= "<li>". $key[0] ."-". $key[1] . "($key[2])</li>";
		}
		$return_fail .= "</ul>";
	}

	echo "<script src=\"js/jquery-1.6.2.min.js\"></script>\n";
	echo "<script>$(parent.document).find('#attach_loader').remove();$(parent.document).find('#attachments ul').html($(parent.document).find('#attachments ul').html()+'$return').fadeIn();";
	echo "$(parent.document).find('#notification').html('$return_fail')</script>";
}
else
{
	echo "<script src=\"js/jquery-1.6.2.min.js\"></script>\n";
	echo "<script>$(parent.document).find('#attach_loader').remove();$(parent.document).find('#notification').html('<li><img style=\"vertical-align:text-bottom\" src=\"images/warning.png\"> The attachments folder is not writeable by the web server.</li>').slideDown();</script>";
	/*
	 incorporate the following js to close the message in the notifications area:
	message = (data) ? 'Member successfully deleted':'Unable to delete member';
	message += ' <span id="notification_close"><img src="images/close.png" alt="close" style="vertical-align:middle"></span>';
	$('#notification').html( message ).hide().slideDown();
	$('#notification_close').click(function(){ $('#notification').slideUp() });
	*/
}
=======
<?php
$folder = "attachments";
$result = array();
$return = "";
$return_fail = "";

function format_size($size) {
	$sizes = array(" Bytes", " KB", " MB", " GB", " TB", " PB", " EB", " ZB", " YB");
	if ($size == 0) {
		return('n/a');
	} else {
		return (round($size/pow(1024, ($i = floor(log($size, 1024)))), 2) . $sizes[$i]);
	}
}

// First, see if the attachments folder is writable
if (is_writable($folder))
{
	// Iterate through files to upload
	foreach ($_FILES["attachment"]["error"] as $key => $error) // Loop through every uploaded file
	{
		$timestamp = time();
		$tmp_name = $_FILES["attachment"]["tmp_name"][$key];
		$name = $_FILES["attachment"]["name"][$key];
		$size = format_size($_FILES["attachment"]["size"][$key]);

		if ($error == UPLOAD_ERR_OK)
		{
			move_uploaded_file($tmp_name, $folder."/$timestamp-$name");
			$result["success"][]=array($name,$size);
		}
		else // The file had an error (http://us.php.net/manual/en/features.file-upload.errors.php)
		{
			$result["fail"][]=array($name,$size,file_upload_error_message($error));
			$database->log(time(),'Unable to upload file - (upload.php)',serialize($result));
		}
	}
	function file_upload_error_message($error_code)
	{
		switch ($error_code)
		{
			case UPLOAD_ERR_INI_SIZE:
				return 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
			case UPLOAD_ERR_FORM_SIZE:
				return 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
			case UPLOAD_ERR_PARTIAL:
				return 'The uploaded file was only partially uploaded';
			case UPLOAD_ERR_NO_FILE:
				return 'No file was uploaded';
			case UPLOAD_ERR_NO_TMP_DIR:
				return 'Missing a temporary folder';
			case UPLOAD_ERR_CANT_WRITE:
				return 'Failed to write file to disk';
			case UPLOAD_ERR_EXTENSION:
				return 'File upload stopped by extension';
			default:
				return 'Unknown upload error';
		}
	}

	// add loading image in the #attachments div when submitting

	if(is_array($result["success"]))
	{
		foreach($result["success"] as $key)
		{
			$return .= "<li>". $key[0] ." (". $key[1] . ") <a class=\"removeAttachment\" href=\"$timestamp-$key[0]\"><img src=\"images/delete.png\" alt=\"remove\"></a> <input type=\"hidden\" name=\"attachment[]\" value=\"$timestamp-$key[0]\"></li>";
		}
	}

	if(is_array($result["fail"]))
	{
		$return_fail .= "The following file(s) were not uploaded:<ul>";
		foreach($result["fail"] as $key)
		{
			$return_fail .= "<li>". $key[0] ."-". $key[1] . "($key[2])</li>";
		}
		$return_fail .= "</ul>";
	}

	echo "<script src=\"js/jquery-1.6.2.min.js\"></script>\n";
	echo "<script>$(parent.document).find('#attach_loader').remove();$(parent.document).find('#attachments ul').html($(parent.document).find('#attachments ul').html()+'$return').fadeIn();";
	echo "$(parent.document).find('#notification').html('$return_fail')</script>";
}
else
{
	echo "<script src=\"js/jquery-1.6.2.min.js\"></script>\n";
	echo "<script>$(parent.document).find('#attach_loader').remove();$(parent.document).find('#notification').html('<li><img style=\"vertical-align:text-bottom\" src=\"images/warning.png\"> The attachments folder is not writeable by the web server.</li>').slideDown();</script>";
	/*
	 incorporate the following js to close the message in the notifications area:
	message = (data) ? 'Member successfully deleted':'Unable to delete member';
	message += ' <span id="notification_close"><img src="images/close.png" alt="close" style="vertical-align:middle"></span>';
	$('#notification').html( message ).hide().slideDown();
	$('#notification_close').click(function(){ $('#notification').slideUp() });
	*/
}
>>>>>>> b59280b13ce027f89f28f2d12e991e62fe2dd1bd
?>