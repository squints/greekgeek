<<<<<<< HEAD
<?php
// Used by js/compose_process.js and js/archives.js
include('../database.php');
include('../functions.php');

// Request to send (process) a specific message. Take the message id and process the delivery queue for it alone.
if (defined('QUEUE_SIZE'))
{
	$curr_time = time();
	$limit = (QUEUE_SIZE == 0) ? '':'limit 0,'.QUEUE_SIZE;
	$result = $database->getQueue($curr_time,$limit,$_GET['p']);
	if(!is_null($result))
	{
		while ($row = mysql_fetch_array($result))
		{
			$message_result = $database->getArchivedMessage($row['message_id']);
			$message = mysql_fetch_array($message_result);
			$to = $row['email'];
			$from = ADMIN_EMAIL;
			$subject = $message['subject'];
			$body = $message['message'];
			$format = $message['format'];
			//			$attachments = (!empty($message['attachments'])) ? explode(',',substr($message['attachments'],0,-1)):'';
			//			$attachments = (!empty($message['attachments'])) ? explode(',',$message['attachments']):'';
			$attachments = (!empty($message['attachments'])) ? unserialize($message['attachments']):'';
			//$attachments = unserialize($attachments[0]);
			$lists = unserialize($message['lists']);
			// If sending the message to only one list, use that list owner's address, else use ADMIN_EMAIL. We do this because if we send a message to
			// multiple lists, the lists may have different owners and we can't send a message from more than one address, therefore we default
			// to the main address in the config file. Also look for any data fields to replace.
			if(count($lists)==1)
			{
				$list_info = $database->getListInfo($lists[0]);
				$from = $list_info['owner'];
			}

			// Data field replacement
			$message_lists_result_row = mysql_fetch_array($database->getArchivedMessage($row['message_id'])); // Given a message id, get the list of list id's that it was sent to
			$message_lists = unserialize($message_lists_result_row['lists']);
			if(count($message_lists)==1) // When we send a message to only one list, we can also utilize any data fields
			{
				$fields_result = $database->getListDataFields($message_lists[0]); // Get a list of data field ids for the given list
				if(!is_null($fields_result))
				{
					$field_list_row = mysql_fetch_array($fields_result);
					$field_list = explode(',',$field_list_row[0]);
					foreach($field_list as $field_id) // For each data field id, get the corresponding sanitized name and build an array of them
					{
						// build array of field names for list with getDataField
						$field_info_row = mysql_fetch_array($database->getDataField($field_id));
						$field_names[] = $field_info_row['sanitized_name'];
					}
					$recipient_data = $database->getMemberDataCollection($row['email'],$message_lists[0]); // Get the data collection field for the person we're sending to
					if(!is_null($recipient_data))
					{
						$recipient_data_row = mysql_fetch_array($recipient_data);
						$recipient_data_array = unserialize($recipient_data_row['data_collection']);
						if($recipient_data_array===FALSE) $recipient_data_array = array(); // If the user has no data fields, we still need to feed array_key_exists an array below
						foreach($field_names as $field)
						{
							$search = '[['.$field.']]';
							$replace = (array_key_exists($field, $recipient_data_array)) ? $recipient_data_array[$field]:''; // If the user doesn't have a matching data field, replace it with nothing
							$body = str_replace($search,$replace,$body);
						}
					}
				}
				/*
				 $handle = fopen('output.txt', 'a');
				fwrite($handle, $body);
				fclose($handle);
				*/
			}
			// END data field replacement

			sendmail($to,$from,$subject,$body,$attachments,$format);
			// mail sent, remove from queue
			$database->dequeueMessage($row['email'],$row['message_id']);
		}
		$remaining = $database->getQueue($curr_time,'',$_GET['p']);
		echo (is_null($remaining)) ? 0:mysql_num_rows($remaining);
	}
	else
	{
		echo "0";
	}
}
else
{
	$database->log(time(),'PROCESS QUEUE','QUEUE_SIZE not defined in config.inc.php'); // log error
	echo "ERROR: QUEUE_SIZE not defined in config.inc.php.";
}
=======
<?php
// Used by js/compose_process.js and js/archives.js
include('../database.php');
include('../functions.php');

// Request to send (process) a specific message. Take the message id and process the delivery queue for it alone.
if (defined('QUEUE_SIZE'))
{
	$curr_time = time();
	$limit = (QUEUE_SIZE == 0) ? '':'limit 0,'.QUEUE_SIZE;
	$result = $database->getQueue($curr_time,$limit,$_GET['p']);
	if(!is_null($result))
	{
		while ($row = mysql_fetch_array($result))
		{
			$message_result = $database->getArchivedMessage($row['message_id']);
			$message = mysql_fetch_array($message_result);
			$to = $row['email'];
			$from = ADMIN_EMAIL;
			$subject = $message['subject'];
			$body = $message['message'];
			$format = $message['format'];
			//			$attachments = (!empty($message['attachments'])) ? explode(',',substr($message['attachments'],0,-1)):'';
			//			$attachments = (!empty($message['attachments'])) ? explode(',',$message['attachments']):'';
			$attachments = (!empty($message['attachments'])) ? unserialize($message['attachments']):'';
			//$attachments = unserialize($attachments[0]);
			$lists = unserialize($message['lists']);
			// If sending the message to only one list, use that list owner's address, else use ADMIN_EMAIL. We do this because if we send a message to
			// multiple lists, the lists may have different owners and we can't send a message from more than one address, therefore we default
			// to the main address in the config file. Also look for any data fields to replace.
			if(count($lists)==1)
			{
				$list_info = $database->getListInfo($lists[0]);
				$from = $list_info['owner'];
			}

			// Data field replacement
			$message_lists_result_row = mysql_fetch_array($database->getArchivedMessage($row['message_id'])); // Given a message id, get the list of list id's that it was sent to
			$message_lists = unserialize($message_lists_result_row['lists']);
			if(count($message_lists)==1) // When we send a message to only one list, we can also utilize any data fields
			{
				$fields_result = $database->getListDataFields($message_lists[0]); // Get a list of data field ids for the given list
				if(!is_null($fields_result))
				{
					$field_list_row = mysql_fetch_array($fields_result);
					$field_list = explode(',',$field_list_row[0]);
					foreach($field_list as $field_id) // For each data field id, get the corresponding sanitized name and build an array of them
					{
						// build array of field names for list with getDataField
						$field_info_row = mysql_fetch_array($database->getDataField($field_id));
						$field_names[] = $field_info_row['sanitized_name'];
					}
					$recipient_data = $database->getMemberDataCollection($row['email'],$message_lists[0]); // Get the data collection field for the person we're sending to
					if(!is_null($recipient_data))
					{
						$recipient_data_row = mysql_fetch_array($recipient_data);
						$recipient_data_array = unserialize($recipient_data_row['data_collection']);
						if($recipient_data_array===FALSE) $recipient_data_array = array(); // If the user has no data fields, we still need to feed array_key_exists an array below
						foreach($field_names as $field)
						{
							$search = '[['.$field.']]';
							$replace = (array_key_exists($field, $recipient_data_array)) ? $recipient_data_array[$field]:''; // If the user doesn't have a matching data field, replace it with nothing
							$body = str_replace($search,$replace,$body);
						}
					}
				}
				/*
				 $handle = fopen('output.txt', 'a');
				fwrite($handle, $body);
				fclose($handle);
				*/
			}
			// END data field replacement

			sendmail($to,$from,$subject,$body,$attachments,$format);
			// mail sent, remove from queue
			$database->dequeueMessage($row['email'],$row['message_id']);
		}
		$remaining = $database->getQueue($curr_time,'',$_GET['p']);
		echo (is_null($remaining)) ? 0:mysql_num_rows($remaining);
	}
	else
	{
		echo "0";
	}
}
else
{
	$database->log(time(),'PROCESS QUEUE','QUEUE_SIZE not defined in config.inc.php'); // log error
	echo "ERROR: QUEUE_SIZE not defined in config.inc.php.";
}
>>>>>>> b59280b13ce027f89f28f2d12e991e62fe2dd1bd
?>