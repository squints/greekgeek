<<<<<<< HEAD
<?php
include('database.php');
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Simple Mailing List - SMTP diagnostic</title>

<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.custom.css"
	rel="stylesheet">
<link type="text/css" href="css/utilities.css" rel="stylesheet">

</head>

<body>
	<div id="content_container">
		<?php
		include('nav.php');
		?>
		<div id="content">
			<p id="subnav">
				<a class="noborder" href="utilities_log.php"><img
					src="images/logs.png" alt="Logs" /> </a> <a class="noborder"
					href="utilities_smtp.php"><img src="images/smtp-diagnostic.png"
					alt="SMTP diagnostic" /> </a> <a class="noborder"
					href="utilities_imap.php"><img src="images/imap-diagnostic.png"
					alt="IMAP diagnostic" /> </a>
			</p>
			<p id="notification"></p>
			<div class="helpText">
				<p>
					<acronym title="Simple Mail Transfer Protocol">SMTP</acronym> is a
					way to send email and is not used to receive email. Typically you
					connect to an SMTP server with a username and password and send
					email via that account. The default SMTP port is 25, although ports
					465 and 587 are also used.
				</p>
				<p>By default, SML uses PHP's mail() function to send email, which
					is typically configured to use sendmail, however you can configure
					SML to send email via an SMTP server. This utility is designed to
					help you determine your SMTP settings by attempting to send an
					email based on the settings you provide. If you receive the test
					email that is sent, you can enter the settings used here in
					config.inc.php. All diagnostic information will be displayed upon
					pressing the test button.</p>
			</div>
			<table>
				<tr>
					<td>SMTP server</td>
					<td><input name="smtp_server" type="text" />
					</td>
					<td id="results" rowspan="7"
						style="vertical-align: top; overflow: auto"></td>
				</tr>
				<tr>
					<td>SMTP port</td>
					<td><input name="smtp_port" type="text" /></td>
				</tr>
				<tr>
					<td>From address</td>
					<td><input name="smtp_from" type="text" /></td>
				</tr>
				<tr>
					<td>To address</td>
					<td><input name="smtp_to" type="text" /></td>
				</tr>
				<tr>
					<td style="padding-left: 20px">Use authentication?</td>
					<td><input name="authenticate" type="checkbox" value="1" />
					</td>
				</tr>
				<tr>
					<td style="padding-left: 20px">SMTP username</td>
					<td><input name="smtp_user" type="text" /></td>
				</tr>
				<tr>
					<td style="padding-left: 20px">SMTP password</td>
					<td><input name="smtp_pass" type="text" /></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td><input name="smtp_submit" type="submit" value="Test" /></td>
					<td>&nbsp;</td>
				</tr>
			</table>
		</div>
	</div>
	<p id="copyright">&copy; www.notonebit.com</p>
	<script src="js/jquery-1.6.2.min.js"></script>
	<script src="js/jquery-ui-1.8.custom.min.js"></script>
	<script src="js/utilities.js"></script>
</body>
=======
<?php
include('database.php');
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Simple Mailing List - SMTP diagnostic</title>

<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.custom.css"
	rel="stylesheet">
<link type="text/css" href="css/utilities.css" rel="stylesheet">

</head>

<body>
	<div id="content_container">
		<?php
		include('nav.php');
		?>
		<div id="content">
			<p id="subnav">
				<a class="noborder" href="utilities_log.php"><img
					src="images/logs.png" alt="Logs" /> </a> <a class="noborder"
					href="utilities_smtp.php"><img src="images/smtp-diagnostic.png"
					alt="SMTP diagnostic" /> </a> <a class="noborder"
					href="utilities_imap.php"><img src="images/imap-diagnostic.png"
					alt="IMAP diagnostic" /> </a>
			</p>
			<p id="notification"></p>
			<div class="helpText">
				<p>
					<acronym title="Simple Mail Transfer Protocol">SMTP</acronym> is a
					way to send email and is not used to receive email. Typically you
					connect to an SMTP server with a username and password and send
					email via that account. The default SMTP port is 25, although ports
					465 and 587 are also used.
				</p>
				<p>By default, SML uses PHP's mail() function to send email, which
					is typically configured to use sendmail, however you can configure
					SML to send email via an SMTP server. This utility is designed to
					help you determine your SMTP settings by attempting to send an
					email based on the settings you provide. If you receive the test
					email that is sent, you can enter the settings used here in
					config.inc.php. All diagnostic information will be displayed upon
					pressing the test button.</p>
			</div>
			<table>
				<tr>
					<td>SMTP server</td>
					<td><input name="smtp_server" type="text" />
					</td>
					<td id="results" rowspan="7"
						style="vertical-align: top; overflow: auto"></td>
				</tr>
				<tr>
					<td>SMTP port</td>
					<td><input name="smtp_port" type="text" /></td>
				</tr>
				<tr>
					<td>From address</td>
					<td><input name="smtp_from" type="text" /></td>
				</tr>
				<tr>
					<td>To address</td>
					<td><input name="smtp_to" type="text" /></td>
				</tr>
				<tr>
					<td style="padding-left: 20px">Use authentication?</td>
					<td><input name="authenticate" type="checkbox" value="1" />
					</td>
				</tr>
				<tr>
					<td style="padding-left: 20px">SMTP username</td>
					<td><input name="smtp_user" type="text" /></td>
				</tr>
				<tr>
					<td style="padding-left: 20px">SMTP password</td>
					<td><input name="smtp_pass" type="text" /></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td><input name="smtp_submit" type="submit" value="Test" /></td>
					<td>&nbsp;</td>
				</tr>
			</table>
		</div>
	</div>
	<p id="copyright">&copy; www.notonebit.com</p>
	<script src="js/jquery-1.6.2.min.js"></script>
	<script src="js/jquery-ui-1.8.custom.min.js"></script>
	<script src="js/utilities.js"></script>
</body>
>>>>>>> b59280b13ce027f89f28f2d12e991e62fe2dd1bd
</html>
