<<<<<<< HEAD
<?php
include("config.inc.php");

class MySQLDB
{
	var $connection; //The MySQL database connection

	function MySQLDB()
	{
		$this->connection = mysql_connect(DB_SERVER, DB_USER, DB_PASS) or die(mysql_error());
		mysql_select_db(DB_NAME, $this->connection) or die(mysql_error());
	}
	function isListNameAvailable($list_name)
	{
		$list_name = mysql_real_escape_string($list_name);
		$q = "SELECT name FROM ".TBL_LISTS." WHERE name = '$list_name'";
		$result = mysql_query($q, $this->connection);
		return (!$result || (mysql_numrows($result) < 1)) ? 1:0; // List name available - no matching list names found in lists table
	}
	function createList($list_name,$list_description,$list_status,$list_owner,$data_collection)
	{
		$list_name = (get_magic_quotes_gpc()) ? mysql_real_escape_string(stripslashes($list_name)):mysql_real_escape_string($list_name);
		$list_description = (get_magic_quotes_gpc()) ? mysql_real_escape_string(stripslashes($list_description)):mysql_real_escape_string($list_description);
		$q = "INSERT INTO ".TBL_LISTS." VALUES ('','$list_name','$list_description','$list_owner','$list_status','$data_collection')";
		return mysql_query($q, $this->connection);
	}
	function updateList($list_id,$list_name,$list_description,$list_status,$list_owner,$data_collection)
	{
		$list_name = (get_magic_quotes_gpc()) ? mysql_real_escape_string(stripslashes($list_name)):mysql_real_escape_string($list_name);
		$list_description = (get_magic_quotes_gpc()) ? mysql_real_escape_string(stripslashes($list_description)):mysql_real_escape_string($list_description);
		$q = "UPDATE ".TBL_LISTS." SET name='$list_name',description='$list_description',owner='$list_owner',status='$list_status',data_fields='$data_collection' WHERE id=$list_id";
		return mysql_query($q, $this->connection);
	}
	function updateListDataField($list_id,$data_collection)
	{
		$q = "UPDATE ".TBL_LISTS." SET data_fields='$data_collection' WHERE id=$list_id";
		return mysql_query($q, $this->connection);
	}
	function deleteList($list_id)
	{
		$q = "DELETE FROM ".TBL_LISTS." WHERE id=$list_id";
		if(mysql_query($q, $this->connection))
		{
			$q = "DELETE FROM ".TBL_BLACKLIST." WHERE list_id='$list_id'";
			if(mysql_query($q, $this->connection))
			{
				$q = "DELETE FROM ".TBL_LIST_MEMBERS." WHERE list_id='$list_id'";
				if(mysql_query($q, $this->connection))
				{
					return true;
				}
				else
				{
					$this->log(time(),'',NULL);
					return false;
				}
			}
			else
			{
				$this->log(time(),'',NULL);
				return false;
			}
		}
		else
		{
			$this->log(time(),'',NULL);
			return false;
		}
	}
	function duplicateList($list_id)
	{
		$q = "SELECT * FROM ".TBL_LISTS." WHERE id='$list_id'";
		$result = mysql_query($q, $this->connection);
		$dbarray = mysql_fetch_array($result);
		while(!$this->isListNameAvailable($dbarray['name']))
		{
			$dbarray['name'] .= '_copy';
		}
		$this->createList($dbarray['name'],$dbarray['description'],$dbarray['status'],$dbarray['owner'],$dbarray['data_fields']);
		$insert_id = mysql_insert_id();
		$q = "SELECT * FROM ".TBL_LIST_MEMBERS." WHERE list_id='$list_id'";
		$result = mysql_query($q, $this->connection);
		while($row=mysql_fetch_array($result))
		{
			$q1 = "INSERT INTO ".TBL_LIST_MEMBERS." VALUES('$row[address]','$insert_id','$row[confirmed]','$row[data_collection]')";
			$q1_result = mysql_query($q1, $this->connection);
		}
	}
	function getDataCollectionFields($order='')
	{
		$q = "SELECT id,type,name,required FROM ".TBL_DATA_COLLECTION." $order";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return $result;
	}
	function getDataField($id)
	{
		$q = "SELECT * FROM ".TBL_DATA_COLLECTION." WHERE id='$id'";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return $result;
	}
	function getListDataFields($list_id)
	{
		$q = "SELECT data_fields FROM ".TBL_LISTS." WHERE id='$list_id' AND data_fields != ''";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1)) // Error occurred, no members found
		{
			return NULL;
		}
		return $result;
	}
	function getListInfo($list)
	{
		$q = "SELECT * FROM ".TBL_LISTS." WHERE id = '$list'";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1)) // Error occurred, list not found
		{
			return NULL;
		}
		$dbarray = mysql_fetch_array($result);
		return $dbarray;
	}
	function getListName($id)
	{
		$q = "SELECT name FROM ".TBL_LISTS." WHERE id='$id'";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		$return = mysql_fetch_array($result);
		return $return[0];
	}
	function getListNames($where='')
	{
		$q = "SELECT * FROM ".TBL_LISTS."$where";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return $result;
	}
	function getListCount()
	{
		$q = "SELECT COUNT(*) FROM ".TBL_LISTS."";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		$dbarray = mysql_fetch_array($result);
		return $dbarray[0];
	}
	function getMembers($list_ids)
	{
		//		$q = "SELECT DISTINCT(address) FROM ".TBL_LIST_MEMBERS." WHERE list_id IN ($list_ids) ORDER BY address asc";
		$q = "SELECT DISTINCT(".TBL_LIST_MEMBERS.".address), ".TBL_MEMBERS.".bounces
				FROM ".TBL_LIST_MEMBERS.", ".TBL_MEMBERS."
				WHERE list_id IN ($list_ids) AND ".TBL_LIST_MEMBERS.".address=".TBL_MEMBERS.".address
						ORDER BY address asc";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1)) // Error occurred, no members found
		{
			return NULL;
		}
		return $result;
	}
	function getUnconfirmedMembers($list_ids)
	{
		$q = "SELECT DISTINCT(address) FROM ".TBL_LIST_MEMBERS." WHERE list_id IN ($list_ids) AND confirmed=0 ORDER BY address asc";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1)) // Error occurred, no members found
		{
			return NULL;
		}
		return $result;
	}
	function getMemberInfo($address)
	{
		$q = "SELECT * FROM ".TBL_MEMBERS." WHERE address='$address'";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1)) // Error occurred, no members found
		{
			return NULL;
		}
		return $result;
	}
	function getMemberDataCollection($address,$list)
	{
		$q = "SELECT data_collection FROM ".TBL_LIST_MEMBERS." WHERE address='$address' AND list_id='$list'";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1)) // Error occurred, no members found
		{
			return NULL;
		}
		return $result;
	}
	function getMemberLists($address)
	{
		$q = "SELECT address,name,confirmed,data_collection,list_id FROM ".TBL_LISTS.", ".TBL_LIST_MEMBERS." WHERE ".TBL_LIST_MEMBERS.".list_id = ".TBL_LISTS.".id And address='$address'";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return $result;
	}
	function getConfirmedStatus($address,$list)
	{
		$q= "SELECT confirmed FROM ".TBL_LIST_MEMBERS." WHERE address = '$address'AND list_id = '$list'";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		$return = mysql_fetch_array($result);
		return $return[0];
	}
	function toggleStatus($address,$list,$status)
	{
		$q = "UPDATE ".TBL_LIST_MEMBERS." SET confirmed='$status' WHERE address = '$address' AND list_id='$list'";
		return mysql_query($q, $this->connection);
	}
	function countMembers($list_ids)
	{
		$q = "SELECT DISTINCT(address) FROM ".TBL_LIST_MEMBERS." WHERE list_id IN ($list_ids) ORDER BY address asc";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return mysql_num_rows($result);
	}
	function countTotalConfirmedMembers()
	{
		$q = "SELECT DISTINCT(address) FROM ".TBL_LIST_MEMBERS." WHERE confirmed=1";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return mysql_num_rows($result);
	}
	function countConfirmedMembers($list_ids)
	{
		$q = "SELECT DISTINCT(address) FROM ".TBL_LIST_MEMBERS." WHERE list_id IN ($list_ids) AND confirmed=1 ORDER BY address asc";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return mysql_num_rows($result);
	}
	function countUnconfirmedMembers($list_ids)
	{
		$q = "SELECT DISTINCT(address) FROM ".TBL_LIST_MEMBERS." WHERE list_id IN ($list_ids) AND confirmed=0 ORDER BY address asc";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return mysql_num_rows($result);
	}
	function memberSearch($address)
	{
		$q = "SELECT address FROM ".TBL_MEMBERS." WHERE address LIKE '%$address%'";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return $result;
	}
	function updateListMember($address, $field, $value, $lists)
	{
		$q = "UPDATE ".TBL_LIST_MEMBERS." SET $field='$value' WHERE address = '$address' AND list_id IN($lists)";
		return mysql_query($q, $this->connection);
	}
	function deleteListMember($address,$lists)
	{
		$q="DELETE FROM ".TBL_LIST_MEMBERS." WHERE address='$address' AND list_id IN($lists)";
		return mysql_query($q, $this->connection);
	}
	function deleteConfirmedListMember($address,$lists)
	{
		$q="DELETE FROM ".TBL_LIST_MEMBERS." WHERE address='$address' AND confirmed='1' AND list_id IN($lists)";
		return mysql_query($q, $this->connection);
	}
	function autoSave($firstTime,$id,$subject,$content,$format,$attachments)
	{
		$timestamp = time();
		$q = ($firstTime=='true') ? "INSERT INTO ".TBL_DRAFTS." VALUES ('','$subject','$content','$timestamp','$format','$attachments')" : "UPDATE ".TBL_DRAFTS." SET subject='$subject', message='$content', timestamp='$timestamp', format='$format', attachments='$attachments' WHERE id='$id'";
		$result = mysql_query($q, $this->connection);
		if(!result)
		{
			$this->log(time(),'Autosave (compose.php)',serialize(func_get_args()));
			return NULL;
		}
		else if($firstTime=='true')
		{
			return mysql_insert_id();
		}
		else return $id;
	}
	function getNumDrafts()
	{
		$q = "SELECT * FROM ".TBL_DRAFTS;
		$result = mysql_query($q, $this->connection);
		return mysql_numrows($result);
	}
	function getDraftList()
	{
		$q = "SELECT * FROM ".TBL_DRAFTS;
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return $result;
	}
	function getDraft($id)
	{
		$q = "SELECT * FROM ".TBL_DRAFTS." WHERE id='$id'";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return $result;
	}
	function archiveMessage($subject, $message, $attachments, $lists, $timestamp, $format)
	{
		$q = "INSERT INTO ".TBL_MESSAGE_ARCHIVE." VALUES ('','$subject', '$message', '$attachments', '$lists', '$timestamp','0','$format')";
		return mysql_query($q, $this->connection);
	}
	function archiveSearch($term)
	{
		//		$q = "SELECT id, subject FROM ".TBL_MESSAGE_ARCHIVE." WHERE MATCH(subject, message) AGAINST ('$term')";
		$q = "SELECT id, subject FROM ".TBL_MESSAGE_ARCHIVE." WHERE subject LIKE '%$term%' OR message LIKE '%$term%'";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return $result;
	}
	function messageRecipients($message_id,$total)
	{
		$q = "UPDATE ".TBL_MESSAGE_ARCHIVE." SET recipients='$total' WHERE id='$message_id'";
		return mysql_query($q, $this->connection);
	}
	function deleteDraft($id)
	{
		$q = (isset($id)) ? "DELETE FROM ".TBL_DRAFTS." WHERE id='$id'":"DELETE FROM ".TBL_DRAFTS;
		return mysql_query($q, $this->connection);
	}
	function getRecipients($lists)
	{
		$q = "SELECT DISTINCT(address) FROM ".TBL_LIST_MEMBERS." WHERE list_id IN ($lists) AND confirmed=1";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return $result;
	}
	function enqueueMessage($address,$message_id,$timestamp)
	{
		$q = "INSERT INTO ".TBL_DELIVERY_QUEUE." VALUES ('$address', '$message_id', '$timestamp')";
		return mysql_query($q, $this->connection);
	}
	function dequeueMessage($address,$message_id)
	{
		$q = "DELETE FROM ".TBL_DELIVERY_QUEUE." WHERE message_id = '$message_id' AND email = '$address'";
		return mysql_query($q, $this->connection);
	}
	function log($timestamp,$message,$data)
	{
		$q = "INSERT INTO ".TBL_LOG." VALUES ('$timestamp', '$message', '$data')";
		return mysql_query($q, $this->connection);
	}
	function getLogContents($limit='')
	{
		$q = "SELECT * FROM ".TBL_LOG." ORDER BY timestamp DESC $limit";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return $result;
	}
	function getLogDetail($ts)
	{
		$q = "SELECT * FROM ".TBL_LOG." WHERE timestamp='$ts'";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return $result;
	}
	function getLogCount()
	{
		$q = "SELECT * FROM ".TBL_LOG;
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return mysql_num_rows($result);
	}
	function purgeLogs($amt)
	{
		$days = time() - (86400 * $amt);
		$q="DELETE FROM ".TBL_LOG." WHERE timestamp < $days";
		return mysql_query($q, $this->connection);
	}
	function insertAddress($address)
	{
		$key = md5(time().mt_rand());
		$q = "INSERT INTO ".TBL_MEMBERS." VALUES ('$address', '$key', '0')";
		return mysql_query($q, $this->connection);
	}
	function insertAddressList($address,$list,$confirmed,$data)
	{
		$q = "INSERT INTO ".TBL_LIST_MEMBERS." VALUES ('$address', '$list', '$confirmed','$data')";
		return mysql_query($q, $this->connection);
	}
	function deleteMember($address)
	{
		$q = "DELETE FROM ".TBL_MEMBERS." WHERE address='$address'";
		if (mysql_query($q, $this->connection) == true)
		{
			$q = "DELETE FROM ".TBL_LIST_MEMBERS." WHERE address='$address'";
			return mysql_query($q, $this->connection);
		} else return false;
	}
	function addBlacklistRule($rule,$list_array)
	{
		if(is_array($list_array))
		{
			foreach($list_array as $list)
			{
				$q = "INSERT INTO ".TBL_BLACKLIST." VALUES ('$rule','$list')";
				mysql_query($q, $this->connection);
			}
		}
	}
	function applyBlacklistRule($rule,$list_array)
	{
		if(is_array($list_array))
		{
			foreach($list_array as $list)
			{
				$q = "DELETE FROM ".TBL_LIST_MEMBERS." WHERE address LIKE '%$rule%' AND list_id=$list";
				mysql_query($q, $this->connection);
			}
		}
	}
	function getBlacklistRules()
	{
		$q = "SELECT * FROM ".TBL_BLACKLIST;
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return $result;
	}
	function deleteBlacklistRule($rule,$list)
	{
		$q = "DELETE FROM ".TBL_BLACKLIST." WHERE rule = '$rule' AND list_id='$list'";
		return mysql_query($q, $this->connection);
	}
	function checkBlacklistRule($pattern)
	{
		$q = "SELECT * FROM ".TBL_MEMBERS." WHERE address LIKE '%$pattern%' ORDER BY address ASC";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return $result;
	}
	function cleanupMembers()
	{
		// Removes members from the members table that don't have any entries in the list_members table
		// The best solution here would be a delete with the following sql as a subquery but MySQL doesn't allow you to modify a table being used in a subquery -- hence the inner while loop
		$q = "SELECT m.address FROM ".TBL_MEMBERS." AS m LEFT OUTER JOIN ".TBL_LIST_MEMBERS." AS l ON l.address = m.address WHERE l.address IS NULL";
		$result = mysql_query($q, $this->connection);
		if(mysql_numrows($result) >= 1)
		{
			//loop through rows with while, deleting from members table
			while($row = mysql_fetch_array($result))
			{
				$subq = "DELETE FROM ".TBL_MEMBERS." WHERE address = '$row[address]'";
				$subresult = mysql_query($subq, $this->connection);
			}
		}
	}
	function getArchivesCount()
	{
		$q = "SELECT id FROM ".TBL_MESSAGE_ARCHIVE;
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return mysql_num_rows($result);
	}
	function getArchiveList($limit='')
	{
		$q = "SELECT * FROM ".TBL_MESSAGE_ARCHIVE." ORDER BY start_send DESC $limit";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return $result;
	}
	function getArchivedMessage($id)
	{
		$q = "SELECT * FROM ".TBL_MESSAGE_ARCHIVE." WHERE id='$id'";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return $result;
	}
	function deleteMessage($id)
	{
		$q = "DELETE FROM ".TBL_MESSAGE_ARCHIVE." WHERE id=$id";
		return mysql_query($q, $this->connection);
	}
	function getAttachments($id)
	{
		$q = "SELECT attachments FROM ".TBL_MESSAGE_ARCHIVE." WHERE id='$id'";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return $result;
	}
	function recipientsRemaining($id)
	{
		$q = "SELECT COUNT(*) FROM ".TBL_DELIVERY_QUEUE." WHERE message_id='$id'";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		$count = mysql_fetch_array($result);
		return $count[0];
	}
	function insertDataField($type,$text,$sanitized,$required,$options,$selected,$multiple)
	{
		$q = "INSERT INTO ".TBL_DATA_COLLECTION." VALUES (NULL,'$type','$text','$sanitized','$required','$options','$selected','$multiple')";
		$result = mysql_query($q, $this->connection);
	}
	function deleteDataField($id)
	{
		$q = "DELETE FROM ".TBL_DATA_COLLECTION." WHERE id = '$id'";
		return mysql_query($q, $this->connection);
	}
	function stopMessage($id)
	{
		$q = "DELETE FROM ".TBL_DELIVERY_QUEUE." WHERE message_id = '$id'";
		return mysql_query($q, $this->connection);
	}
	function getQueue($time, $limit='', $id='')
	{
		if(!empty($id)) $specific="AND message_id='$id'";
		$q = "SELECT * FROM ".TBL_DELIVERY_QUEUE." WHERE timestamp <= '$time' $specific ORDER BY timestamp ASC $limit";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return $result;
	}
	function getRSS()
	{
		$q = "SELECT * FROM ".TBL_MESSAGE_ARCHIVE." ORDER BY start_send DESC";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return $result;
	}
	function updateBounceCount($address)
	{
		$q = "UPDATE ".TBL_MEMBERS." SET bounces = bounces + 1 WHERE address='$address'";
		return mysql_query($q, $this->connection);
	}
	function cleanupDeadbeats($threshold)
	{
		$q = "DELETE FROM ".TBL_DELIVERY_QUEUE." WHERE message_id = '$id'";
		$q = "DELETE ".TBL_MEMBERS.",".TBL_LIST_MEMBERS." FROM ".TBL_MEMBERS.",".TBL_LIST_MEMBERS." WHERE ".TBL_MEMBERS.".bounces>='$threshold' AND ".TBL_MEMBERS.".address=".TBL_LIST_MEMBERS.".address";
		return mysql_query($q, $this->connection);
	}

	// query
	// -----
	// Performs the given query on the database and returns the result, which may be false, true or a
	// resource identifier.
	function query($query)
	{
		return mysql_query($query, $this->connection);
	}
};

$database = new MySQLDB; // Instantiate a database connection
=======
<?php
include("config.inc.php");

class MySQLDB
{
	var $connection; //The MySQL database connection

	function MySQLDB()
	{
		$this->connection = mysql_connect(DB_SERVER, DB_USER, DB_PASS) or die(mysql_error());
		mysql_select_db(DB_NAME, $this->connection) or die(mysql_error());
	}
	function isListNameAvailable($list_name)
	{
		$list_name = mysql_real_escape_string($list_name);
		$q = "SELECT name FROM ".TBL_LISTS." WHERE name = '$list_name'";
		$result = mysql_query($q, $this->connection);
		return (!$result || (mysql_numrows($result) < 1)) ? 1:0; // List name available - no matching list names found in lists table
	}
	function createList($list_name,$list_description,$list_status,$list_owner,$data_collection)
	{
		$list_name = (get_magic_quotes_gpc()) ? mysql_real_escape_string(stripslashes($list_name)):mysql_real_escape_string($list_name);
		$list_description = (get_magic_quotes_gpc()) ? mysql_real_escape_string(stripslashes($list_description)):mysql_real_escape_string($list_description);
		$q = "INSERT INTO ".TBL_LISTS." VALUES ('','$list_name','$list_description','$list_owner','$list_status','$data_collection')";
		return mysql_query($q, $this->connection);
	}
	function updateList($list_id,$list_name,$list_description,$list_status,$list_owner,$data_collection)
	{
		$list_name = (get_magic_quotes_gpc()) ? mysql_real_escape_string(stripslashes($list_name)):mysql_real_escape_string($list_name);
		$list_description = (get_magic_quotes_gpc()) ? mysql_real_escape_string(stripslashes($list_description)):mysql_real_escape_string($list_description);
		$q = "UPDATE ".TBL_LISTS." SET name='$list_name',description='$list_description',owner='$list_owner',status='$list_status',data_fields='$data_collection' WHERE id=$list_id";
		return mysql_query($q, $this->connection);
	}
	function updateListDataField($list_id,$data_collection)
	{
		$q = "UPDATE ".TBL_LISTS." SET data_fields='$data_collection' WHERE id=$list_id";
		return mysql_query($q, $this->connection);
	}
	function deleteList($list_id)
	{
		$q = "DELETE FROM ".TBL_LISTS." WHERE id=$list_id";
		if(mysql_query($q, $this->connection))
		{
			$q = "DELETE FROM ".TBL_BLACKLIST." WHERE list_id='$list_id'";
			if(mysql_query($q, $this->connection))
			{
				$q = "DELETE FROM ".TBL_LIST_MEMBERS." WHERE list_id='$list_id'";
				if(mysql_query($q, $this->connection))
				{
					return true;
				}
				else
				{
					$this->log(time(),'',NULL);
					return false;
				}
			}
			else
			{
				$this->log(time(),'',NULL);
				return false;
			}
		}
		else
		{
			$this->log(time(),'',NULL);
			return false;
		}
	}
	function duplicateList($list_id)
	{
		$q = "SELECT * FROM ".TBL_LISTS." WHERE id='$list_id'";
		$result = mysql_query($q, $this->connection);
		$dbarray = mysql_fetch_array($result);
		while(!$this->isListNameAvailable($dbarray['name']))
		{
			$dbarray['name'] .= '_copy';
		}
		$this->createList($dbarray['name'],$dbarray['description'],$dbarray['status'],$dbarray['owner'],$dbarray['data_fields']);
		$insert_id = mysql_insert_id();
		$q = "SELECT * FROM ".TBL_LIST_MEMBERS." WHERE list_id='$list_id'";
		$result = mysql_query($q, $this->connection);
		while($row=mysql_fetch_array($result))
		{
			$q1 = "INSERT INTO ".TBL_LIST_MEMBERS." VALUES('$row[address]','$insert_id','$row[confirmed]','$row[data_collection]')";
			$q1_result = mysql_query($q1, $this->connection);
		}
	}
	function getDataCollectionFields($order='')
	{
		$q = "SELECT id,type,name,required FROM ".TBL_DATA_COLLECTION." $order";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return $result;
	}
	function getDataField($id)
	{
		$q = "SELECT * FROM ".TBL_DATA_COLLECTION." WHERE id='$id'";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return $result;
	}
	function getListDataFields($list_id)
	{
		$q = "SELECT data_fields FROM ".TBL_LISTS." WHERE id='$list_id' AND data_fields != ''";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1)) // Error occurred, no members found
		{
			return NULL;
		}
		return $result;
	}
	function getListInfo($list)
	{
		$q = "SELECT * FROM ".TBL_LISTS." WHERE id = '$list'";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1)) // Error occurred, list not found
		{
			return NULL;
		}
		$dbarray = mysql_fetch_array($result);
		return $dbarray;
	}
	function getListName($id)
	{
		$q = "SELECT name FROM ".TBL_LISTS." WHERE id='$id'";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		$return = mysql_fetch_array($result);
		return $return[0];
	}
	function getListNames($where='')
	{
		$q = "SELECT * FROM ".TBL_LISTS."$where";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return $result;
	}
	function getListCount()
	{
		$q = "SELECT COUNT(*) FROM ".TBL_LISTS."";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		$dbarray = mysql_fetch_array($result);
		return $dbarray[0];
	}
	function getMembers($list_ids)
	{
		//		$q = "SELECT DISTINCT(address) FROM ".TBL_LIST_MEMBERS." WHERE list_id IN ($list_ids) ORDER BY address asc";
		$q = "SELECT DISTINCT(".TBL_LIST_MEMBERS.".address), ".TBL_MEMBERS.".bounces
				FROM ".TBL_LIST_MEMBERS.", ".TBL_MEMBERS."
				WHERE list_id IN ($list_ids) AND ".TBL_LIST_MEMBERS.".address=".TBL_MEMBERS.".address
						ORDER BY address asc";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1)) // Error occurred, no members found
		{
			return NULL;
		}
		return $result;
	}
	function getUnconfirmedMembers($list_ids)
	{
		$q = "SELECT DISTINCT(address) FROM ".TBL_LIST_MEMBERS." WHERE list_id IN ($list_ids) AND confirmed=0 ORDER BY address asc";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1)) // Error occurred, no members found
		{
			return NULL;
		}
		return $result;
	}
	function getMemberInfo($address)
	{
		$q = "SELECT * FROM ".TBL_MEMBERS." WHERE address='$address'";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1)) // Error occurred, no members found
		{
			return NULL;
		}
		return $result;
	}
	function getMemberDataCollection($address,$list)
	{
		$q = "SELECT data_collection FROM ".TBL_LIST_MEMBERS." WHERE address='$address' AND list_id='$list'";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1)) // Error occurred, no members found
		{
			return NULL;
		}
		return $result;
	}
	function getMemberLists($address)
	{
		$q = "SELECT address,name,confirmed,data_collection,list_id FROM ".TBL_LISTS.", ".TBL_LIST_MEMBERS." WHERE ".TBL_LIST_MEMBERS.".list_id = ".TBL_LISTS.".id And address='$address'";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return $result;
	}
	function getConfirmedStatus($address,$list)
	{
		$q= "SELECT confirmed FROM ".TBL_LIST_MEMBERS." WHERE address = '$address'AND list_id = '$list'";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		$return = mysql_fetch_array($result);
		return $return[0];
	}
	function toggleStatus($address,$list,$status)
	{
		$q = "UPDATE ".TBL_LIST_MEMBERS." SET confirmed='$status' WHERE address = '$address' AND list_id='$list'";
		return mysql_query($q, $this->connection);
	}
	function countMembers($list_ids)
	{
		$q = "SELECT DISTINCT(address) FROM ".TBL_LIST_MEMBERS." WHERE list_id IN ($list_ids) ORDER BY address asc";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return mysql_num_rows($result);
	}
	function countTotalConfirmedMembers()
	{
		$q = "SELECT DISTINCT(address) FROM ".TBL_LIST_MEMBERS." WHERE confirmed=1";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return mysql_num_rows($result);
	}
	function countConfirmedMembers($list_ids)
	{
		$q = "SELECT DISTINCT(address) FROM ".TBL_LIST_MEMBERS." WHERE list_id IN ($list_ids) AND confirmed=1 ORDER BY address asc";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return mysql_num_rows($result);
	}
	function countUnconfirmedMembers($list_ids)
	{
		$q = "SELECT DISTINCT(address) FROM ".TBL_LIST_MEMBERS." WHERE list_id IN ($list_ids) AND confirmed=0 ORDER BY address asc";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return mysql_num_rows($result);
	}
	function memberSearch($address)
	{
		$q = "SELECT address FROM ".TBL_MEMBERS." WHERE address LIKE '%$address%'";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return $result;
	}
	function updateListMember($address, $field, $value, $lists)
	{
		$q = "UPDATE ".TBL_LIST_MEMBERS." SET $field='$value' WHERE address = '$address' AND list_id IN($lists)";
		return mysql_query($q, $this->connection);
	}
	function deleteListMember($address,$lists)
	{
		$q="DELETE FROM ".TBL_LIST_MEMBERS." WHERE address='$address' AND list_id IN($lists)";
		return mysql_query($q, $this->connection);
	}
	function deleteConfirmedListMember($address,$lists)
	{
		$q="DELETE FROM ".TBL_LIST_MEMBERS." WHERE address='$address' AND confirmed='1' AND list_id IN($lists)";
		return mysql_query($q, $this->connection);
	}
	function autoSave($firstTime,$id,$subject,$content,$format,$attachments)
	{
		$timestamp = time();
		$q = ($firstTime=='true') ? "INSERT INTO ".TBL_DRAFTS." VALUES ('','$subject','$content','$timestamp','$format','$attachments')" : "UPDATE ".TBL_DRAFTS." SET subject='$subject', message='$content', timestamp='$timestamp', format='$format', attachments='$attachments' WHERE id='$id'";
		$result = mysql_query($q, $this->connection);
		if(!result)
		{
			$this->log(time(),'Autosave (compose.php)',serialize(func_get_args()));
			return NULL;
		}
		else if($firstTime=='true')
		{
			return mysql_insert_id();
		}
		else return $id;
	}
	function getNumDrafts()
	{
		$q = "SELECT * FROM ".TBL_DRAFTS;
		$result = mysql_query($q, $this->connection);
		return mysql_numrows($result);
	}
	function getDraftList()
	{
		$q = "SELECT * FROM ".TBL_DRAFTS;
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return $result;
	}
	function getDraft($id)
	{
		$q = "SELECT * FROM ".TBL_DRAFTS." WHERE id='$id'";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return $result;
	}
	function archiveMessage($subject, $message, $attachments, $lists, $timestamp, $format)
	{
		$q = "INSERT INTO ".TBL_MESSAGE_ARCHIVE." VALUES ('','$subject', '$message', '$attachments', '$lists', '$timestamp','0','$format')";
		return mysql_query($q, $this->connection);
	}
	function archiveSearch($term)
	{
		//		$q = "SELECT id, subject FROM ".TBL_MESSAGE_ARCHIVE." WHERE MATCH(subject, message) AGAINST ('$term')";
		$q = "SELECT id, subject FROM ".TBL_MESSAGE_ARCHIVE." WHERE subject LIKE '%$term%' OR message LIKE '%$term%'";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return $result;
	}
	function messageRecipients($message_id,$total)
	{
		$q = "UPDATE ".TBL_MESSAGE_ARCHIVE." SET recipients='$total' WHERE id='$message_id'";
		return mysql_query($q, $this->connection);
	}
	function deleteDraft($id)
	{
		$q = (isset($id)) ? "DELETE FROM ".TBL_DRAFTS." WHERE id='$id'":"DELETE FROM ".TBL_DRAFTS;
		return mysql_query($q, $this->connection);
	}
	function getRecipients($lists)
	{
		$q = "SELECT DISTINCT(address) FROM ".TBL_LIST_MEMBERS." WHERE list_id IN ($lists) AND confirmed=1";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return $result;
	}
	function enqueueMessage($address,$message_id,$timestamp)
	{
		$q = "INSERT INTO ".TBL_DELIVERY_QUEUE." VALUES ('$address', '$message_id', '$timestamp')";
		return mysql_query($q, $this->connection);
	}
	function dequeueMessage($address,$message_id)
	{
		$q = "DELETE FROM ".TBL_DELIVERY_QUEUE." WHERE message_id = '$message_id' AND email = '$address'";
		return mysql_query($q, $this->connection);
	}
	function log($timestamp,$message,$data)
	{
		$q = "INSERT INTO ".TBL_LOG." VALUES ('$timestamp', '$message', '$data')";
		return mysql_query($q, $this->connection);
	}
	function getLogContents($limit='')
	{
		$q = "SELECT * FROM ".TBL_LOG." ORDER BY timestamp DESC $limit";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return $result;
	}
	function getLogDetail($ts)
	{
		$q = "SELECT * FROM ".TBL_LOG." WHERE timestamp='$ts'";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return $result;
	}
	function getLogCount()
	{
		$q = "SELECT * FROM ".TBL_LOG;
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return mysql_num_rows($result);
	}
	function purgeLogs($amt)
	{
		$days = time() - (86400 * $amt);
		$q="DELETE FROM ".TBL_LOG." WHERE timestamp < $days";
		return mysql_query($q, $this->connection);
	}
	function insertAddress($address)
	{
		$key = md5(time().mt_rand());
		$q = "INSERT INTO ".TBL_MEMBERS." VALUES ('$address', '$key', '0')";
		return mysql_query($q, $this->connection);
	}
	function insertAddressList($address,$list,$confirmed,$data)
	{
		$q = "INSERT INTO ".TBL_LIST_MEMBERS." VALUES ('$address', '$list', '$confirmed','$data')";
		return mysql_query($q, $this->connection);
	}
	function deleteMember($address)
	{
		$q = "DELETE FROM ".TBL_MEMBERS." WHERE address='$address'";
		if (mysql_query($q, $this->connection) == true)
		{
			$q = "DELETE FROM ".TBL_LIST_MEMBERS." WHERE address='$address'";
			return mysql_query($q, $this->connection);
		} else return false;
	}
	function addBlacklistRule($rule,$list_array)
	{
		if(is_array($list_array))
		{
			foreach($list_array as $list)
			{
				$q = "INSERT INTO ".TBL_BLACKLIST." VALUES ('$rule','$list')";
				mysql_query($q, $this->connection);
			}
		}
	}
	function applyBlacklistRule($rule,$list_array)
	{
		if(is_array($list_array))
		{
			foreach($list_array as $list)
			{
				$q = "DELETE FROM ".TBL_LIST_MEMBERS." WHERE address LIKE '%$rule%' AND list_id=$list";
				mysql_query($q, $this->connection);
			}
		}
	}
	function getBlacklistRules()
	{
		$q = "SELECT * FROM ".TBL_BLACKLIST;
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return $result;
	}
	function deleteBlacklistRule($rule,$list)
	{
		$q = "DELETE FROM ".TBL_BLACKLIST." WHERE rule = '$rule' AND list_id='$list'";
		return mysql_query($q, $this->connection);
	}
	function checkBlacklistRule($pattern)
	{
		$q = "SELECT * FROM ".TBL_MEMBERS." WHERE address LIKE '%$pattern%' ORDER BY address ASC";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return $result;
	}
	function cleanupMembers()
	{
		// Removes members from the members table that don't have any entries in the list_members table
		// The best solution here would be a delete with the following sql as a subquery but MySQL doesn't allow you to modify a table being used in a subquery -- hence the inner while loop
		$q = "SELECT m.address FROM ".TBL_MEMBERS." AS m LEFT OUTER JOIN ".TBL_LIST_MEMBERS." AS l ON l.address = m.address WHERE l.address IS NULL";
		$result = mysql_query($q, $this->connection);
		if(mysql_numrows($result) >= 1)
		{
			//loop through rows with while, deleting from members table
			while($row = mysql_fetch_array($result))
			{
				$subq = "DELETE FROM ".TBL_MEMBERS." WHERE address = '$row[address]'";
				$subresult = mysql_query($subq, $this->connection);
			}
		}
	}
	function getArchivesCount()
	{
		$q = "SELECT id FROM ".TBL_MESSAGE_ARCHIVE;
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return mysql_num_rows($result);
	}
	function getArchiveList($limit='')
	{
		$q = "SELECT * FROM ".TBL_MESSAGE_ARCHIVE." ORDER BY start_send DESC $limit";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return $result;
	}
	function getArchivedMessage($id)
	{
		$q = "SELECT * FROM ".TBL_MESSAGE_ARCHIVE." WHERE id='$id'";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return $result;
	}
	function deleteMessage($id)
	{
		$q = "DELETE FROM ".TBL_MESSAGE_ARCHIVE." WHERE id=$id";
		return mysql_query($q, $this->connection);
	}
	function getAttachments($id)
	{
		$q = "SELECT attachments FROM ".TBL_MESSAGE_ARCHIVE." WHERE id='$id'";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return $result;
	}
	function recipientsRemaining($id)
	{
		$q = "SELECT COUNT(*) FROM ".TBL_DELIVERY_QUEUE." WHERE message_id='$id'";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		$count = mysql_fetch_array($result);
		return $count[0];
	}
	function insertDataField($type,$text,$sanitized,$required,$options,$selected,$multiple)
	{
		$q = "INSERT INTO ".TBL_DATA_COLLECTION." VALUES (NULL,'$type','$text','$sanitized','$required','$options','$selected','$multiple')";
		$result = mysql_query($q, $this->connection);
	}
	function deleteDataField($id)
	{
		$q = "DELETE FROM ".TBL_DATA_COLLECTION." WHERE id = '$id'";
		return mysql_query($q, $this->connection);
	}
	function stopMessage($id)
	{
		$q = "DELETE FROM ".TBL_DELIVERY_QUEUE." WHERE message_id = '$id'";
		return mysql_query($q, $this->connection);
	}
	function getQueue($time, $limit='', $id='')
	{
		if(!empty($id)) $specific="AND message_id='$id'";
		$q = "SELECT * FROM ".TBL_DELIVERY_QUEUE." WHERE timestamp <= '$time' $specific ORDER BY timestamp ASC $limit";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return $result;
	}
	function getRSS()
	{
		$q = "SELECT * FROM ".TBL_MESSAGE_ARCHIVE." ORDER BY start_send DESC";
		$result = mysql_query($q, $this->connection);
		if(!$result || (mysql_numrows($result) < 1))
		{
			return NULL;
		}
		return $result;
	}
	function updateBounceCount($address)
	{
		$q = "UPDATE ".TBL_MEMBERS." SET bounces = bounces + 1 WHERE address='$address'";
		return mysql_query($q, $this->connection);
	}
	function cleanupDeadbeats($threshold)
	{
		$q = "DELETE FROM ".TBL_DELIVERY_QUEUE." WHERE message_id = '$id'";
		$q = "DELETE ".TBL_MEMBERS.",".TBL_LIST_MEMBERS." FROM ".TBL_MEMBERS.",".TBL_LIST_MEMBERS." WHERE ".TBL_MEMBERS.".bounces>='$threshold' AND ".TBL_MEMBERS.".address=".TBL_LIST_MEMBERS.".address";
		return mysql_query($q, $this->connection);
	}

	// query
	// -----
	// Performs the given query on the database and returns the result, which may be false, true or a
	// resource identifier.
	function query($query)
	{
		return mysql_query($query, $this->connection);
	}
};

$database = new MySQLDB; // Instantiate a database connection
>>>>>>> b59280b13ce027f89f28f2d12e991e62fe2dd1bd
?>