<<<<<<< HEAD
<?php
include('database.php');
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Simple Mailing List - Member Management</title>

<link type="text/css" href="css/custom-theme/jquery-ui-1.8.2.custom.css"
	rel="stylesheet">
<link type="text/css" href="css/members.css" rel="stylesheet">

</head>

<body>
	<div id="content_container">
		<?php
		include('nav.php');
		$totalMembers = $database->countTotalConfirmedMembers();
		if($totalMembers >=2) $plural = "s";
		?>
		<div id="content">
			<p id="notification"></p>
			<div class="right">
				<img class="openImport" src="images/import.png" alt="import">
			</div>
			<div class="right" style="margin-right: 4px" id="cleanup">
				<img src="images/deadbeat.png" alt="deadbeat cleanup">
			</div>
			<h1>
				You have
				<?php
				echo ($totalMembers == 0) ? " absolutely no one subscribing to ":" $totalMembers confirmed member$plural subscribing to ";

				$list_count = $database->getListCount();
				echo ($list_count == 1) ? "one list.":"$list_count available lists.";
				?>
			</h1>
			<p class="helpText">This is where you can manage all your list
				members. Use the import feature to load addresses from other sources
				and deadbeat cleanup to delete members that have accumulated too
				many bounced messages. After selecting one or more lists you will
				see the members listed in the middle of this page (members in red
				are unconfirmed to one or more lists). Clicking a member&#39;s
				address will bring up information on them and give you the ability
				to remove them from individual lists, delete them entirely, or
				confirm/unconfirm them from individual lists.</p>

			<div id="search">
				Search <input name="query" id="query" type="text" />
			</div>

			<div id="lists">
				<h3>Lists</h3>
				<p>
					<?php
					$num_lists = $database->getListCount();
					if($num_lists>1) echo "<label><input type=\"checkbox\" id=\"all\" name=\"selectall\" value=\"\">Select All/None</label><br>\n";
					$result = $database->getListNames();
					if(!is_null($result))
					{
						while($row=mysql_fetch_array($result))
						{
							$checked = ($num_lists == 1) ? ' checked="checked"':'';
							echo "<label style=\"margin-left:16px\" id=\"list-$row[id]\" ";
							if(!$row[status]) echo "class=\"closed\" ";
							echo "title=\"ID:$row[id]\"><input type=\"checkbox\" name=\"lists[]\" value=\"$row[id]\"$checked>$row[name]";
							//		echo (!empty($row['description'])) ? " - $row[description]":"";
							echo " $member_count</label><br>\n";
						}
					}
					?>
				</p>
			</div>
			<!-- #lists -->

			<div id="members">
				<h3>Members</h3>
				<p>
					<span id="member_count"></span>
				</p>
				<div id="member_list"></div>
			</div>

			<div id="member_data">
				<h3 id="member_address"></h3>
				<p id="member_lists"></p>
				<p id="member_bounces"></p>
				<p>
					<button type="button" class="delete" id="member_delete" href="#">
						<img src="images/delete.png" alt="Delete"> Delete Member
					</button>
				</p>
				<p class="helpText">
					To add a member to a list, use the <a class="openImport"
						id="profile_import" href="#">import</a> feature.
				</p>
			</div>
		</div>
		<!-- #content -->
	</div>
	<!-- #content_container -->
	<p id="copyright">&copy; www.notonebit.com</p>

	<div id="importDialog" title="Import">
		<form action="members-process.php" method="post"
			enctype="multipart/form-data">
			<p>
				Type or paste email addresses into the box below (space delimited).<br>If
				you are using a delimiter other than a space, enter it here: <input
					type="text" name="delimiter" id="delimiter" size="1"><br>
				<textarea id="addresses" name="addresses"></textarea>
				<br> Or upload a text file of address: <input name="importFile"
					type="file" /> Maximum file size:
				<?php echo ini_get('upload_max_filesize');?>
			</p>
			<p>
				<a href="blacklist.php">Blacklist rules</a> will be applied to
				imported addresses. If you would like to bypass blacklist rule
				checking, check this box: <input type="checkbox" id="bypass"
					name="bypass">
			</p>
			<p>Finally, select one or more lists to add these addresses to:</p>
			<div id="join_lists">
				<?php
				mysql_data_seek($result, 0);
				while($row=mysql_fetch_array($result))
				{
					echo "<label><input type=\"checkbox\" name=\"lists[]\" value=\"$row[id]\"$checked>$row[name]</label> \n";
				}
				?>
				<p style="text-align: center; margin: 10px 0 0 0">
					<button id="import">
						<img src="images/plus.gif" style="vertical-align: middle"> Import
						Addresses
					</button>
				</p>
			</div>
		</form>
	</div>
	<div id="cleanupDialog" title="Deadbeat Cleanup">
		Remove all members with <input type="text" size="2"
			name="bounceCleanup"> or more bounced messages.<br> <input
			type="submit" name="bounceSubmit" value="Remove">
	</div>
	<script src="js/jquery-1.6.2.min.js"></script>
	<script src="js/jquery-ui-1.8.custom.min.js"></script>
	<script src="js/members.js"></script>
</body>
=======
<?php
include('database.php');
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Simple Mailing List - Member Management</title>

<link type="text/css" href="css/custom-theme/jquery-ui-1.8.2.custom.css"
	rel="stylesheet">
<link type="text/css" href="css/members.css" rel="stylesheet">

</head>

<body>
	<div id="content_container">
		<?php
		include('nav.php');
		$totalMembers = $database->countTotalConfirmedMembers();
		if($totalMembers >=2) $plural = "s";
		?>
		<div id="content">
			<p id="notification"></p>
			<div class="right">
				<img class="openImport" src="images/import.png" alt="import">
			</div>
			<div class="right" style="margin-right: 4px" id="cleanup">
				<img src="images/deadbeat.png" alt="deadbeat cleanup">
			</div>
			<h1>
				You have
				<?php
				echo ($totalMembers == 0) ? " absolutely no one subscribing to ":" $totalMembers confirmed member$plural subscribing to ";

				$list_count = $database->getListCount();
				echo ($list_count == 1) ? "one list.":"$list_count available lists.";
				?>
			</h1>
			<p class="helpText">This is where you can manage all your list
				members. Use the import feature to load addresses from other sources
				and deadbeat cleanup to delete members that have accumulated too
				many bounced messages. After selecting one or more lists you will
				see the members listed in the middle of this page (members in red
				are unconfirmed to one or more lists). Clicking a member&#39;s
				address will bring up information on them and give you the ability
				to remove them from individual lists, delete them entirely, or
				confirm/unconfirm them from individual lists.</p>

			<div id="search">
				Search <input name="query" id="query" type="text" />
			</div>

			<div id="lists">
				<h3>Lists</h3>
				<p>
					<?php
					$num_lists = $database->getListCount();
					if($num_lists>1) echo "<label><input type=\"checkbox\" id=\"all\" name=\"selectall\" value=\"\">Select All/None</label><br>\n";
					$result = $database->getListNames();
					if(!is_null($result))
					{
						while($row=mysql_fetch_array($result))
						{
							$checked = ($num_lists == 1) ? ' checked="checked"':'';
							echo "<label style=\"margin-left:16px\" id=\"list-$row[id]\" ";
							if(!$row[status]) echo "class=\"closed\" ";
							echo "title=\"ID:$row[id]\"><input type=\"checkbox\" name=\"lists[]\" value=\"$row[id]\"$checked>$row[name]";
							//		echo (!empty($row['description'])) ? " - $row[description]":"";
							echo " $member_count</label><br>\n";
						}
					}
					?>
				</p>
			</div>
			<!-- #lists -->

			<div id="members">
				<h3>Members</h3>
				<p>
					<span id="member_count"></span>
				</p>
				<div id="member_list"></div>
			</div>

			<div id="member_data">
				<h3 id="member_address"></h3>
				<p id="member_lists"></p>
				<p id="member_bounces"></p>
				<p>
					<button type="button" class="delete" id="member_delete" href="#">
						<img src="images/delete.png" alt="Delete"> Delete Member
					</button>
				</p>
				<p class="helpText">
					To add a member to a list, use the <a class="openImport"
						id="profile_import" href="#">import</a> feature.
				</p>
			</div>
		</div>
		<!-- #content -->
	</div>
	<!-- #content_container -->
	<p id="copyright">&copy; www.notonebit.com</p>

	<div id="importDialog" title="Import">
		<form action="members-process.php" method="post"
			enctype="multipart/form-data">
			<p>
				Type or paste email addresses into the box below (space delimited).<br>If
				you are using a delimiter other than a space, enter it here: <input
					type="text" name="delimiter" id="delimiter" size="1"><br>
				<textarea id="addresses" name="addresses"></textarea>
				<br> Or upload a text file of address: <input name="importFile"
					type="file" /> Maximum file size:
				<?php echo ini_get('upload_max_filesize');?>
			</p>
			<p>
				<a href="blacklist.php">Blacklist rules</a> will be applied to
				imported addresses. If you would like to bypass blacklist rule
				checking, check this box: <input type="checkbox" id="bypass"
					name="bypass">
			</p>
			<p>Finally, select one or more lists to add these addresses to:</p>
			<div id="join_lists">
				<?php
				mysql_data_seek($result, 0);
				while($row=mysql_fetch_array($result))
				{
					echo "<label><input type=\"checkbox\" name=\"lists[]\" value=\"$row[id]\"$checked>$row[name]</label> \n";
				}
				?>
				<p style="text-align: center; margin: 10px 0 0 0">
					<button id="import">
						<img src="images/plus.gif" style="vertical-align: middle"> Import
						Addresses
					</button>
				</p>
			</div>
		</form>
	</div>
	<div id="cleanupDialog" title="Deadbeat Cleanup">
		Remove all members with <input type="text" size="2"
			name="bounceCleanup"> or more bounced messages.<br> <input
			type="submit" name="bounceSubmit" value="Remove">
	</div>
	<script src="js/jquery-1.6.2.min.js"></script>
	<script src="js/jquery-ui-1.8.custom.min.js"></script>
	<script src="js/members.js"></script>
</body>
>>>>>>> b59280b13ce027f89f28f2d12e991e62fe2dd1bd
</html>
