<<<<<<< HEAD
<?php
include('database.php');
// $_GET['a'] a=action. Possible values are: d=delete, c=copy(duplicate)

// Count lists. If only one don't make user select from list, just display.
if($database->getListCount() == 1)
{
	$list = mysql_fetch_array($database->getListnames());
	$list_info = $database->getListInfo($list['id']);
	$list_id = $list['id'];
}
if($_GET['id'])
{
	$list_info = $database->getListInfo($_GET['id']); // Used when selecting a list from the list drop down menu.
	$list_id = $_GET['id'];
}
if($_GET['a']=='d') // Delete a list
{
	$database->deleteList($_GET['id']);
	unset($_GET['id']);
}
if($_GET['a']=='c') // Copy(duplicate) a list
{
	$database->duplicateList($_GET['id']);
}
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Simple Mailing List - Edit/Delete a list</title>
<script src="js/jquery-1.6.2.min.js"></script>
<script src="js/jquery-ui-1.8.custom.min.js"></script>
<script src="js/list_management.js"></script>
<script>
$(function(){
	$('#data_collection').show();
<?php
if(!empty($list_id))
{
?>
	$.get("ajax/generate-form.php", { id: <?php echo $list_id; ?>}, function(data){
		$('#list_edit_html').html('Subscribe/Unsubscribe HTML:'+data).hide().fadeIn();
	});
<?php
}
if($_GET['u'])echo "$('#notification').html('Update successful <span id=\"notification_close\"><img src=\"images/close.png\" style=\"vertical-align:middle\" alt=\"close\"></span>').hide().slideDown();\n";
?>
});
</script>

<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.custom.css"
	rel="stylesheet">
<link type="text/css" href="css/list_management.css" rel="stylesheet">
</head>

<body>
	<div id="content_container">
		<?php
		include('nav.php');
		?>
		<div id="content">
			<p id="subnav">
				<a class="noborder" href="list_management_add.php"><img
					src="images/add-list.png" alt="Add a list" /> </a> <a
					class="noborder" href="list_management_edit.php"><img
					src="images/edit-delete-list.png" alt="Edit/Delete a list" /> </a>
				<a class="noborder" href="data_collection.php"><img
					src="images/data-collection.png" alt="Add a list" /> </a>
			</p>
			<p id="notification"></p>
			<p>
				Select a list to edit or delete: <select name="list_list"
					id="list_list">
					<option value="-1">-Select a list-</option>
					<?php
					$result = $database->getListNames();
					while($row=mysql_fetch_array($result))
					{
						echo "<option value=\"$row[id]\"";
						if($row[id]==$_GET['id'] OR !empty($list_id)) echo " selected";
						echo ">$row[name]";
						//	echo (!empty($row[description])) ? " - $row[description]":"";
						echo " - $row[owner]";
						echo "</option>\n";
					}
					?>
				</select>
			</p>

			<?php
			if(!is_null($list_info))
			{
				?>
			<form method="post" action="">
				<div id="list_info">
					<h3 id="add_list_heading">Edit List</h3>
					<div class="left" style="margin-right: 20px">
						<label class="label">List Name<br> <input name="list_name"
							id="list_name" type="text" value="<?php echo $list_info[name];?>">
						</label>
					</div>
					<div class="left">
						<label class="label">List Owner<br> <input name="list_owner"
							id="list_owner" type="text"
							value="<?php echo $list_info[owner];?>">
						</label>
					</div>
					<div class="clear">
						<label class="label">List Description (optional)<br> <textarea
								name="list_description" cols="40" rows="4"><?php echo $list_info[description];?></textarea>
						</label>
					</div>
					<h3 class="clear" id="add_list_heading">Other Actions</h3>
					<p id="utils">
						<span id="delete">Delete this list</span> <span id="duplicate">Duplicate
							this list</span> List Open?: <input name="list_status"
							type="checkbox" value="1"
							<?php if($list_info[status]) echo "checked=\"checked\""?>>
					</p>
					<button type="button" id="update">Update</button>
				</div>

				<div style="float: left; margin: 0 0 0 30px;">
					<h3 id="data_collection_heading" class="expand">Data Collection</h3>
					<div id="data_collection">
						<p style="float: right;">Selected</p>
						<p>Available</p>
						<ul id="available" class='droptrue'>
							<?php
							$result = $database->getDataCollectionFields('');
							if(!is_null($result))
							{
								$fields = explode(',',$list_info[data_fields]);
								while($row=mysql_fetch_array($result))
								{
									if(!in_array($row[id],$fields))
									{
										echo "<li id=\"$row[id]\" class=\"ui-state-default\">";
										echo "<img src=\"images/form-$row[type].png\"> $row[name]";
										if($row['required']) echo " (required)";
										echo "</li>\n";
									}
									$dc[$row[id]]=array($row[name],$row[type],$row[required]);
								}
							}
							?>
						</ul>
						<ul id="chosen" class='droptrue'>
							<?php
							if(is_array($fields))
							{
								foreach($fields as $field)
								{
									if(!empty($field))
									{
										echo "<li id=\"$field\" class=\"ui-state-default\">";
										echo "<img src=\"images/form-".$dc[$field][1].".png\"> ".$dc[$field][0];
										if($dc[$field][2]) echo " (required)";
										echo "</li>\n";
									}
								}
							}
							?>
						</ul>
					</div>
				</div>
				<input type="hidden" name="list_id" value="<?php echo $list_id?>">
			</form>

			<div id="list_edit_html"></div>

			<?php
			} // End of if($_GET['id'])
			?>
		</div>
		<!-- #content -->
	</div>
	<!-- #content_container -->
	<p id="copyright">&copy; www.notonebit.com</p>
</body>
=======
<?php
include('database.php');
// $_GET['a'] a=action. Possible values are: d=delete, c=copy(duplicate)

// Count lists. If only one don't make user select from list, just display.
if($database->getListCount() == 1)
{
	$list = mysql_fetch_array($database->getListnames());
	$list_info = $database->getListInfo($list['id']);
	$list_id = $list['id'];
}
if($_GET['id'])
{
	$list_info = $database->getListInfo($_GET['id']); // Used when selecting a list from the list drop down menu.
	$list_id = $_GET['id'];
}
if($_GET['a']=='d') // Delete a list
{
	$database->deleteList($_GET['id']);
	unset($_GET['id']);
}
if($_GET['a']=='c') // Copy(duplicate) a list
{
	$database->duplicateList($_GET['id']);
}
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Simple Mailing List - Edit/Delete a list</title>
<script src="js/jquery-1.6.2.min.js"></script>
<script src="js/jquery-ui-1.8.custom.min.js"></script>
<script src="js/list_management.js"></script>
<script>
$(function(){
	$('#data_collection').show();
<?php
if(!empty($list_id))
{
?>
	$.get("ajax/generate-form.php", { id: <?php echo $list_id; ?>}, function(data){
		$('#list_edit_html').html('Subscribe/Unsubscribe HTML:'+data).hide().fadeIn();
	});
<?php
}
if($_GET['u'])echo "$('#notification').html('Update successful <span id=\"notification_close\"><img src=\"images/close.png\" style=\"vertical-align:middle\" alt=\"close\"></span>').hide().slideDown();\n";
?>
});
</script>

<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.custom.css"
	rel="stylesheet">
<link type="text/css" href="css/list_management.css" rel="stylesheet">
</head>

<body>
	<div id="content_container">
		<?php
		include('nav.php');
		?>
		<div id="content">
			<p id="subnav">
				<a class="noborder" href="list_management_add.php"><img
					src="images/add-list.png" alt="Add a list" /> </a> <a
					class="noborder" href="list_management_edit.php"><img
					src="images/edit-delete-list.png" alt="Edit/Delete a list" /> </a>
				<a class="noborder" href="data_collection.php"><img
					src="images/data-collection.png" alt="Add a list" /> </a>
			</p>
			<p id="notification"></p>
			<p>
				Select a list to edit or delete: <select name="list_list"
					id="list_list">
					<option value="-1">-Select a list-</option>
					<?php
					$result = $database->getListNames();
					while($row=mysql_fetch_array($result))
					{
						echo "<option value=\"$row[id]\"";
						if($row[id]==$_GET['id'] OR !empty($list_id)) echo " selected";
						echo ">$row[name]";
						//	echo (!empty($row[description])) ? " - $row[description]":"";
						echo " - $row[owner]";
						echo "</option>\n";
					}
					?>
				</select>
			</p>

			<?php
			if(!is_null($list_info))
			{
				?>
			<form method="post" action="">
				<div id="list_info">
					<h3 id="add_list_heading">Edit List</h3>
					<div class="left" style="margin-right: 20px">
						<label class="label">List Name<br> <input name="list_name"
							id="list_name" type="text" value="<?php echo $list_info[name];?>">
						</label>
					</div>
					<div class="left">
						<label class="label">List Owner<br> <input name="list_owner"
							id="list_owner" type="text"
							value="<?php echo $list_info[owner];?>">
						</label>
					</div>
					<div class="clear">
						<label class="label">List Description (optional)<br> <textarea
								name="list_description" cols="40" rows="4"><?php echo $list_info[description];?></textarea>
						</label>
					</div>
					<h3 class="clear" id="add_list_heading">Other Actions</h3>
					<p id="utils">
						<span id="delete">Delete this list</span> <span id="duplicate">Duplicate
							this list</span> List Open?: <input name="list_status"
							type="checkbox" value="1"
							<?php if($list_info[status]) echo "checked=\"checked\""?>>
					</p>
					<button type="button" id="update">Update</button>
				</div>

				<div style="float: left; margin: 0 0 0 30px;">
					<h3 id="data_collection_heading" class="expand">Data Collection</h3>
					<div id="data_collection">
						<p style="float: right;">Selected</p>
						<p>Available</p>
						<ul id="available" class='droptrue'>
							<?php
							$result = $database->getDataCollectionFields('');
							if(!is_null($result))
							{
								$fields = explode(',',$list_info[data_fields]);
								while($row=mysql_fetch_array($result))
								{
									if(!in_array($row[id],$fields))
									{
										echo "<li id=\"$row[id]\" class=\"ui-state-default\">";
										echo "<img src=\"images/form-$row[type].png\"> $row[name]";
										if($row['required']) echo " (required)";
										echo "</li>\n";
									}
									$dc[$row[id]]=array($row[name],$row[type],$row[required]);
								}
							}
							?>
						</ul>
						<ul id="chosen" class='droptrue'>
							<?php
							if(is_array($fields))
							{
								foreach($fields as $field)
								{
									if(!empty($field))
									{
										echo "<li id=\"$field\" class=\"ui-state-default\">";
										echo "<img src=\"images/form-".$dc[$field][1].".png\"> ".$dc[$field][0];
										if($dc[$field][2]) echo " (required)";
										echo "</li>\n";
									}
								}
							}
							?>
						</ul>
					</div>
				</div>
				<input type="hidden" name="list_id" value="<?php echo $list_id?>">
			</form>

			<div id="list_edit_html"></div>

			<?php
			} // End of if($_GET['id'])
			?>
		</div>
		<!-- #content -->
	</div>
	<!-- #content_container -->
	<p id="copyright">&copy; www.notonebit.com</p>
</body>
>>>>>>> b59280b13ce027f89f28f2d12e991e62fe2dd1bd
</html>
