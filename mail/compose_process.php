<<<<<<< HEAD
<?php
include('database.php');
ini_set('max_execution_time',60);
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title></title>
<script src="js/jquery-1.6.2.min.js"></script>
<script src="js/jquery-ui-1.8.custom.min.js"></script>
<script src="js/compose_process.js"></script>

<link type="text/css" href="css/custom-theme/jquery-ui-1.8.2.custom.css"
	rel="stylesheet">
<link type="text/css" href="css/compose_process.css" rel="stylesheet">

</head>
<body>
	<div id="content_container">
		<?php
		include('nav.php');
		?>
		<div id="content">

			<p id="notification"></p>

			<?php
			$subject = $_POST['subject'];
			$message = $_POST['message'];
			$attachments = (!empty($_POST['attachment'])) ? serialize($_POST['attachment']):NULL;
			$lists = (!empty($_POST['lists'])) ? serialize($_POST['lists']):NULL;
			$format = $_POST['format'];

			if($lists==NULL)
			{
				exit("Error: No lists selected to send message to. Please go back and select at least one list.\n</body>\n</html>");
			}

			// Build a list of distinct recipients and insert each one into the queue table
			echo "<script>$('#notification').html('Building recipient list...please wait.<img id=\"wait\" src=\"images/ajax-loader.gif\">');</script>\n";
			flush();
			$recipients = $database->getRecipients(implode(',',$_POST[lists]));

			// Test $recipients to make sure we have someone to send the message to
			if($recipients == NULL)
			{
				// display and log error
				$error[]=$message_id;
				$error[]=$lists;
				$database->log(time(),'Build list of recipients',serialize($error));
				exit("Error: No recipients selected\n</body>\n</html>");
			}
			else
			{
				echo "<script>$('#wait').remove();$('#notification').html($('#notification').html()+'<br>Building queue...please wait.<img id=\"wait\" src=\"images/ajax-loader.gif\">');</script>\n";

				$subject = (get_magic_quotes_gpc()) ? mysql_real_escape_string(stripslashes($subject)):mysql_real_escape_string($subject);
				$message = (get_magic_quotes_gpc()) ? mysql_real_escape_string(stripslashes($message)):mysql_real_escape_string($message);

				if($_POST['when']=='now')
				{
					$timestamp = time();
				}
				else
				{
					$date = strtotime($_POST['datepicker']); // Calendar date user entered
					$month = date("n", $date);
					$day = date("j", $date);
					$year = date("Y", $date);
					$hour = $_POST['hour'];
					$min = $_POST['minute'];
					if($_POST['ampm']=='pm')$hour+=12;
					$timestamp = mktime($hour, $min, 0, $month, $day, $year);
				}

				// Insert message into archive and get the returned message id
				if($database->archiveMessage($subject, $message, $attachments, $lists, $timestamp, $format))
				{
					// Message inserted, get row id
					$message_id = mysql_insert_id();

					// Delete the associated draft (if one exists)
					if(!empty($_POST['draftID'])) $database->deleteDraft($_POST['draftID']);
				}
				else
				{
					// Inserting failed (see log?)
					$database->log(time(),'Insert message into archive',$subject.'|'.$message_id.'|'.$lists);
				}

				$total=0; // Recipient count for archives
				while($row = mysql_fetch_array($recipients))
				{
					// Queue message for delivery
					$result = $database->enqueueMessage($row['address'],$message_id,$timestamp);
					// log bad results
					$total++;
					// When queueing a message to a large number of recipients, flush the buffer so the user isn't left wondering WTF is going on?
					if ($total%5000==0)
					{
						echo "<script>$('#notification').html('Queueing...$total');</script>\n";
						flush();
					}
				}
				echo "<script>$('#notification').html('Queueing...$total');</script>\n";
				flush();

				// update archive with total recipents for mesage
				$database->messageRecipients($message_id,$total);

				echo "<h1>Hooray, message queued for delivery!</h1>";

				if(CRON_ENABLED)
				{
					// display message about message being queued and progress can be seen in the archives or errors in the log
					echo "<p>Your message has been queued and will be processed automatically. Note that for automatic delivery that you <strong>MUST</strong> ";
					echo "have a cron job setup to handle message delivery.</p>";
				}
				else
				{
					// display message about how to process. manually via archive or clik here to spawn a new window to keep open
					echo "<p>Your configuration is currently set to manually process the delivery queue. You can either: <ul><li>Manually process the delivery ";
					echo "queue from the <a href=\"archives.php\">Archives</a>, or</li><li><a id=\"manual\" href=\"#\">Click here</a> to open a new window which will attempt to process ";
					echo "the queue by refreshing itself once per minute</li></ul></p><input type=\"hidden\" name=\"message_id\" value=\"$message_id\">";
				}
				echo "<script>$('#wait').remove();</script>";
			}
			?>
			<p>
				To check delivery progress, visit the message <a href="archives.php">Archives</a>.
				<a href="compose.php">Click here</a> to compose another message.
			</p>
		</div>
		<!-- #content -->
	</div>
	<!-- #content_container -->

	<div id="processDialog" title="Manual Delivery Processing">
		<p>
			<img src="images/ajax-loader.gif"> Processing delivery queue. Keep
			this dialog box open to continue processing your queue. Or, to force
			a send,
			<button id="force" style="padding: 0; font-size: .8em;">click here</button>
		</p>
		<p>
			Sending
			<?php echo QUEUE_SIZE;?>
			messages at a time (<span id="remaining"><?php echo $total; ?> </span>
			remaining). Next batch to be processed in <span id="countdown">0</span>
			seconds. <span id="countdown_message"></span>
		</p>
		<p>
			To stop processing the queue, <a href="#">click here</a> or close
			this dialog box.
		</p>
	</div>

</body>
=======
<?php
include('database.php');
ini_set('max_execution_time',60);
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title></title>
<script src="js/jquery-1.6.2.min.js"></script>
<script src="js/jquery-ui-1.8.custom.min.js"></script>
<script src="js/compose_process.js"></script>

<link type="text/css" href="css/custom-theme/jquery-ui-1.8.2.custom.css"
	rel="stylesheet">
<link type="text/css" href="css/compose_process.css" rel="stylesheet">

</head>
<body>
	<div id="content_container">
		<?php
		include('nav.php');
		?>
		<div id="content">

			<p id="notification"></p>

			<?php
			$subject = $_POST['subject'];
			$message = $_POST['message'];
			$attachments = (!empty($_POST['attachment'])) ? serialize($_POST['attachment']):NULL;
			$lists = (!empty($_POST['lists'])) ? serialize($_POST['lists']):NULL;
			$format = $_POST['format'];

			if($lists==NULL)
			{
				exit("Error: No lists selected to send message to. Please go back and select at least one list.\n</body>\n</html>");
			}

			// Build a list of distinct recipients and insert each one into the queue table
			echo "<script>$('#notification').html('Building recipient list...please wait.<img id=\"wait\" src=\"images/ajax-loader.gif\">');</script>\n";
			flush();
			$recipients = $database->getRecipients(implode(',',$_POST[lists]));

			// Test $recipients to make sure we have someone to send the message to
			if($recipients == NULL)
			{
				// display and log error
				$error[]=$message_id;
				$error[]=$lists;
				$database->log(time(),'Build list of recipients',serialize($error));
				exit("Error: No recipients selected\n</body>\n</html>");
			}
			else
			{
				echo "<script>$('#wait').remove();$('#notification').html($('#notification').html()+'<br>Building queue...please wait.<img id=\"wait\" src=\"images/ajax-loader.gif\">');</script>\n";

				$subject = (get_magic_quotes_gpc()) ? mysql_real_escape_string(stripslashes($subject)):mysql_real_escape_string($subject);
				$message = (get_magic_quotes_gpc()) ? mysql_real_escape_string(stripslashes($message)):mysql_real_escape_string($message);

				if($_POST['when']=='now')
				{
					$timestamp = time();
				}
				else
				{
					$date = strtotime($_POST['datepicker']); // Calendar date user entered
					$month = date("n", $date);
					$day = date("j", $date);
					$year = date("Y", $date);
					$hour = $_POST['hour'];
					$min = $_POST['minute'];
					if($_POST['ampm']=='pm')$hour+=12;
					$timestamp = mktime($hour, $min, 0, $month, $day, $year);
				}

				// Insert message into archive and get the returned message id
				if($database->archiveMessage($subject, $message, $attachments, $lists, $timestamp, $format))
				{
					// Message inserted, get row id
					$message_id = mysql_insert_id();

					// Delete the associated draft (if one exists)
					if(!empty($_POST['draftID'])) $database->deleteDraft($_POST['draftID']);
				}
				else
				{
					// Inserting failed (see log?)
					$database->log(time(),'Insert message into archive',$subject.'|'.$message_id.'|'.$lists);
				}

				$total=0; // Recipient count for archives
				while($row = mysql_fetch_array($recipients))
				{
					// Queue message for delivery
					$result = $database->enqueueMessage($row['address'],$message_id,$timestamp);
					// log bad results
					$total++;
					// When queueing a message to a large number of recipients, flush the buffer so the user isn't left wondering WTF is going on?
					if ($total%5000==0)
					{
						echo "<script>$('#notification').html('Queueing...$total');</script>\n";
						flush();
					}
				}
				echo "<script>$('#notification').html('Queueing...$total');</script>\n";
				flush();

				// update archive with total recipents for mesage
				$database->messageRecipients($message_id,$total);

				echo "<h1>Hooray, message queued for delivery!</h1>";

				if(CRON_ENABLED)
				{
					// display message about message being queued and progress can be seen in the archives or errors in the log
					echo "<p>Your message has been queued and will be processed automatically. Note that for automatic delivery that you <strong>MUST</strong> ";
					echo "have a cron job setup to handle message delivery.</p>";
				}
				else
				{
					// display message about how to process. manually via archive or clik here to spawn a new window to keep open
					echo "<p>Your configuration is currently set to manually process the delivery queue. You can either: <ul><li>Manually process the delivery ";
					echo "queue from the <a href=\"archives.php\">Archives</a>, or</li><li><a id=\"manual\" href=\"#\">Click here</a> to open a new window which will attempt to process ";
					echo "the queue by refreshing itself once per minute</li></ul></p><input type=\"hidden\" name=\"message_id\" value=\"$message_id\">";
				}
				echo "<script>$('#wait').remove();</script>";
			}
			?>
			<p>
				To check delivery progress, visit the message <a href="archives.php">Archives</a>.
				<a href="compose.php">Click here</a> to compose another message.
			</p>
		</div>
		<!-- #content -->
	</div>
	<!-- #content_container -->

	<div id="processDialog" title="Manual Delivery Processing">
		<p>
			<img src="images/ajax-loader.gif"> Processing delivery queue. Keep
			this dialog box open to continue processing your queue. Or, to force
			a send,
			<button id="force" style="padding: 0; font-size: .8em;">click here</button>
		</p>
		<p>
			Sending
			<?php echo QUEUE_SIZE;?>
			messages at a time (<span id="remaining"><?php echo $total; ?> </span>
			remaining). Next batch to be processed in <span id="countdown">0</span>
			seconds. <span id="countdown_message"></span>
		</p>
		<p>
			To stop processing the queue, <a href="#">click here</a> or close
			this dialog box.
		</p>
	</div>

</body>
>>>>>>> b59280b13ce027f89f28f2d12e991e62fe2dd1bd
</html>
