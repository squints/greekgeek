<<<<<<< HEAD
<?php
include('database.php');
include('functions.php');
include('paginator.class.php');

if($_GET['d'])
{
	// Request to stop sending a message. Take the id and delete all messages in the delivery queue.
	$database->stopMessage($_GET['d']);
}
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Simple Mailing List - Archives</title>

<link type="text/css" href="css/custom-theme/jquery-ui-1.8.2.custom.css"
	rel="stylesheet">
<link type="text/css" href="css/archives.css" rel="stylesheet">

</head>

<body>
	<div id="content_container">
		<?php
		include('nav.php');
		?>
		<div id="content" style="overflow: visible">
			<p id="notification"></p>

			<?php
			$num_messages = $database->getArchivesCount();
			if ($num_messages >= 1)
			{
				?>
			<div id="search">
				<form method="post">
					Search <input name="query" id="query" type="text" />
				</form>
			</div>
			<?php
			echo "<h1>There are <span id=\"archive_count\">$num_messages</span> messages in the archive.</h1>\n";
			}
			else
			{
				echo "<p>There are no messages in your archive. Why not <a href=\"compose.php\">send one today</a>?</p>";
				exit("\n</body>\n</html>");
			}

			$pages = new Paginator;
			$pages->items_total = $num_messages;
			$pages->mid_range = 5;
			$pages->paginate();
			if($num_messages > $pages->items_per_page)
			{
				echo "<p>".$pages->display_pages();
				echo $pages->display_jump_menu();
				echo $pages->display_items_per_page()."</p>\n";
			}
			echo "<input type=\"hidden\" name=\"paginator_ipp\" value=\"$pages->items_per_page\">\n";
			echo "<input type=\"hidden\" name=\"paginator_page\" value=\"$pages->current_page\">\n";
			echo "<input type=\"hidden\" name=\"paginator_total\" value=\"$pages->items_total\">\n";
			?>

			<table>
				<tr>
					<th>Subject</th>
					<th>Sent</th>
					<th>Progress</th>
					<th>Sent&nbsp;to</th>
					<th>Attachments</th>
					<th><img src="images/delete.png" alt="Delete"></th>
				</tr>
				<?php
				$messages = $database->getArchiveList($pages->limit);
				while($row = mysql_fetch_array($messages))
				{
					$sent_to = $row['recipients'] - $database->recipientsRemaining($row['id']);
					$percentage = floor($sent_to/$row[recipients] * 100);
					echo "<tr><td><a class=\"message\" href=\"$row[id]\">$row[subject]</a></td>\n";
					echo "<td class=\"nowrap\">".date("F j, Y @ g:i a",$row[start_send])."</td>\n";
					echo "<td class=\"center\"><div class=\"prog\" title=\"$sent_to/$row[recipients] ($percentage%)\"><span style=\"position:absolute\">$percentage</span></div>".process($percentage,$row[id])."</td>\n";
					echo "<td class=\"center\"><a href=\"$row[id]\" class=\"lists\">Lists</a></td>\n";
					echo "<td>".showAttachments($row[attachments])."</td>\n";
					echo "<td class=\"center\"><a class=\"delete\" href=\"$row[id]\"><img src=\"images/delete.png\" style=\"border:none\"></a></td>\n</tr>\n";
				}
				?>

			</table>

			<?php
			if($num_messages > $pages->items_per_page)
			{
				echo "<p>".$pages->display_pages();
				echo $pages->display_jump_menu();
				echo $pages->display_items_per_page()."</p>";
			}
			?>

		</div>
		<!-- #content -->
	</div>
	<!-- #content_container -->
	<p id="copyright">&copy; www.notonebit.com</p>

	<div id="viewDialog" title="View Message">
		<p>
			Subject: <span id="subject"></span> <a id="resend" href="#">Send
				again?</a>
		</p>
		<div id="message"></div>
	</div>

	<div id="listDialog" title="Lists"></div>

	<div id="processDialog" title="Manual Delivery Processing">
		<p>
			<img src="images/ajax-loader.gif"> Processing delivery queue. Keep
			this dialog box open to continue processing your queue. Or, to force
			a send,
			<button id="force" style="padding: 0; font-size: .8em;">click here</button>
		</p>
		<p>
			Sending
			<?php echo QUEUE_SIZE;?>
			messages at a time (<span id="remaining"><?php echo $total; ?> </span>
			remaining). Next batch to be processed in <span id="countdown">0</span>
			seconds. <span id="countdown_message"></span>
		</p>
		<p>
			To stop processing the queue, <a href="#">click here</a> or close
			this dialog box.
		</p>
	</div>
	<script src="js/jquery-1.6.2.min.js"></script>
	<script src="js/jquery-ui-1.8.custom.min.js"></script>
	<script src="js/archives.js"></script>
</body>
=======
<?php
include('database.php');
include('functions.php');
include('paginator.class.php');

if($_GET['d'])
{
	// Request to stop sending a message. Take the id and delete all messages in the delivery queue.
	$database->stopMessage($_GET['d']);
}
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Simple Mailing List - Archives</title>

<link type="text/css" href="css/custom-theme/jquery-ui-1.8.2.custom.css"
	rel="stylesheet">
<link type="text/css" href="css/archives.css" rel="stylesheet">

</head>

<body>
	<div id="content_container">
		<?php
		include('nav.php');
		?>
		<div id="content" style="overflow: visible">
			<p id="notification"></p>

			<?php
			$num_messages = $database->getArchivesCount();
			if ($num_messages >= 1)
			{
				?>
			<div id="search">
				<form method="post">
					Search <input name="query" id="query" type="text" />
				</form>
			</div>
			<?php
			echo "<h1>There are <span id=\"archive_count\">$num_messages</span> messages in the archive.</h1>\n";
			}
			else
			{
				echo "<p>There are no messages in your archive. Why not <a href=\"compose.php\">send one today</a>?</p>";
				exit("\n</body>\n</html>");
			}

			$pages = new Paginator;
			$pages->items_total = $num_messages;
			$pages->mid_range = 5;
			$pages->paginate();
			if($num_messages > $pages->items_per_page)
			{
				echo "<p>".$pages->display_pages();
				echo $pages->display_jump_menu();
				echo $pages->display_items_per_page()."</p>\n";
			}
			echo "<input type=\"hidden\" name=\"paginator_ipp\" value=\"$pages->items_per_page\">\n";
			echo "<input type=\"hidden\" name=\"paginator_page\" value=\"$pages->current_page\">\n";
			echo "<input type=\"hidden\" name=\"paginator_total\" value=\"$pages->items_total\">\n";
			?>

			<table>
				<tr>
					<th>Subject</th>
					<th>Sent</th>
					<th>Progress</th>
					<th>Sent&nbsp;to</th>
					<th>Attachments</th>
					<th><img src="images/delete.png" alt="Delete"></th>
				</tr>
				<?php
				$messages = $database->getArchiveList($pages->limit);
				while($row = mysql_fetch_array($messages))
				{
					$sent_to = $row['recipients'] - $database->recipientsRemaining($row['id']);
					$percentage = floor($sent_to/$row[recipients] * 100);
					echo "<tr><td><a class=\"message\" href=\"$row[id]\">$row[subject]</a></td>\n";
					echo "<td class=\"nowrap\">".date("F j, Y @ g:i a",$row[start_send])."</td>\n";
					echo "<td class=\"center\"><div class=\"prog\" title=\"$sent_to/$row[recipients] ($percentage%)\"><span style=\"position:absolute\">$percentage</span></div>".process($percentage,$row[id])."</td>\n";
					echo "<td class=\"center\"><a href=\"$row[id]\" class=\"lists\">Lists</a></td>\n";
					echo "<td>".showAttachments($row[attachments])."</td>\n";
					echo "<td class=\"center\"><a class=\"delete\" href=\"$row[id]\"><img src=\"images/delete.png\" style=\"border:none\"></a></td>\n</tr>\n";
				}
				?>

			</table>

			<?php
			if($num_messages > $pages->items_per_page)
			{
				echo "<p>".$pages->display_pages();
				echo $pages->display_jump_menu();
				echo $pages->display_items_per_page()."</p>";
			}
			?>

		</div>
		<!-- #content -->
	</div>
	<!-- #content_container -->
	<p id="copyright">&copy; www.notonebit.com</p>

	<div id="viewDialog" title="View Message">
		<p>
			Subject: <span id="subject"></span> <a id="resend" href="#">Send
				again?</a>
		</p>
		<div id="message"></div>
	</div>

	<div id="listDialog" title="Lists"></div>

	<div id="processDialog" title="Manual Delivery Processing">
		<p>
			<img src="images/ajax-loader.gif"> Processing delivery queue. Keep
			this dialog box open to continue processing your queue. Or, to force
			a send,
			<button id="force" style="padding: 0; font-size: .8em;">click here</button>
		</p>
		<p>
			Sending
			<?php echo QUEUE_SIZE;?>
			messages at a time (<span id="remaining"><?php echo $total; ?> </span>
			remaining). Next batch to be processed in <span id="countdown">0</span>
			seconds. <span id="countdown_message"></span>
		</p>
		<p>
			To stop processing the queue, <a href="#">click here</a> or close
			this dialog box.
		</p>
	</div>
	<script src="js/jquery-1.6.2.min.js"></script>
	<script src="js/jquery-ui-1.8.custom.min.js"></script>
	<script src="js/archives.js"></script>
</body>
>>>>>>> b59280b13ce027f89f28f2d12e991e62fe2dd1bd
</html>
