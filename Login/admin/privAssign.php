<br /><table border="1">
	    	<tr>
		<th>First Name</th>
		<th>Last Name</th>
        <th>Position</th>
        <th>Privlege</th>
        <th>New Privlege</th>
        <th></th>
	</tr>
	<?php
	$result = mysqli_query($mysqli, "SELECT id, username, first, last, position, privid FROM users");//where active when applicable

	while($row = mysqli_fetch_array($result))
	{
		?>
	<form id="post" action="../admin/updatePriv.php" method="post">

		<tr>

			<td><?php echo $row['first'];?></td>
			<td><?php echo $row['last'];?></td>
			<td><?php echo $row['position'];?></td>
			<td><?php echo $row['privid'];?></td>
			<td><select name="priv">
					<option placeholder="" value="" selected></option>
					<option value="1">Admin</option>
					<option value="2">Archon</option>
					<option value="3">Deputy Archon</option>
					<option value="4">Recorder</option>
					<option value="5">Treasurer</option>
					<option value="6">Scholar Chair</option>
					<option value="7">Rush Chair</option>
					<option value="8">Risk Manager</option>
					<option value="9">Pledge Ed</option>
					<option value="10">Social Chair</option>
					<option value="11">House Manager</option>
					<option value="12">Pledge</option>
					<option value="13">Brothas</option>
					<option value="69">Innactive</option>
			</select>
			</td>
			<input type="hidden" name="User" value="<?php echo $row['id'];?>">
			<input type="hidden" name="Username"
				value="<?php echo $row['username'];?>">
			<td><button id="sub">Change User Privleges</button></td>
		</tr>
	</form>

	<?php
	}
	?>
</table></center>
</div>
<div id="box2adm">
					<h2>Mass Command Line</h2>
<center><table border=1>
	<tr>
		<th>Command</th>
		<th>Action</th>
	</tr>
	<tr>
		<td>strip</td>
		<td>clears all but pledges and admins</td>
	</tr>
	<tr>
		<td>depledge</td>
		<td>changes pledges to brothers</td>
	</tr>
	<tr>
		<td>clearall</td>
		<td>Sets all to brothers but admins</td>
	</tr>
</table>
<form id="privadmin" action="../admin/privAdmin.php" method="post">
	<input placeholder="Command" type="text" name="command">
	<button id="execute">Execute</button>
</form></center>
</div>
