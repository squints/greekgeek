<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]> <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Brother Login</title>
<link rel="stylesheet" href="css/style.css">
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>
	<form method="post" action="module/checkLogin.php" class="login">
		<p>
			<label for="login">Username:</label> <input type="text"
				name="username">
		</p>

		<p>
			<label for="password">Password:</label> <input type="password"
				name="password">
		</p>

		<p class="login-submit">
			<button type="submit" class="login-button">Login</button>
		</p>


	</form>

	<section class="about">
		<p class="about-links">
			<a href="http://localhost/SAE" target="_parent">Back to Home</a> <a
				href="http://www.SAE.net" target="_parent">National SAE</a>
		</p>
		<p class="about-author">
			&copy; 2012&ndash;2013 Sigma Alpha Epsilon Georgia Omega<br>
			Alternate design and coding by Autry Short and M.H. "squints"
			McCarsky
	
	</section>
</body>
</html>
