<?php 
include "../module/securityFunctions.php";
include "../module/db_connect.php";
sec_session_start();
//this goes at the very top of the page, in the header


if(login_check($mysqli) == true) {?>

<html>
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>SPSU &Sigma;AE</title>
<link rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="style.css" />
</head>
<body>
	<div id="bg">
		<div id="outer">
			<div id="header">
				<div id="logo">&Sigma;AE Georgia Omega</div>
				<div id="nav">
					<ul>
						<li class="first active"><a href="homemain.php">Home</a>
						</li>
						<li><a href="brothersmain.php">brothers</a>
						</li>
						<li><a href="calendarmain.php">Calendar</a>
						</li>
						<li><a href="scholarshipmain.php">scholarship</a>
						</li>
						<li><a href="housemain.php">house</a>
						</li>
                        <?php if ((privCheck("admin", $mysqli)) === true){?>
                        <li><a href="admincontrol.php">admin</a>
						</li>
                        <?php } ?>
						<li><a href="resourcesmain.php">Resources</a>
						</li>
						<li class="last"><a href="archivesmain.php">archives</a>
						</li>
					</ul>
					<br class="clear" />
				</div>
			</div>
			<div id="sidebar1">
				<h3>Announcements</h3>
				<ul class="linkedList">
					<li class="first"><?php include '../module/anns.php';?>
					
					<li class="last">
			
			</div>
			<div id="sidebar2">
				<ul class="linkedList">
					<li class="first"><li class="first"><?php echo $_SESSION['username']." ";?><a href="../module/logout.php">Not you?</a><br /><a href="../views/offreportmain.php">Post an Officer Report</a><br> <a href="updatemain.php">Update Account</a><br /> <a href="updatemain.php">Change Password</a><br /><a href="reportmain.php">Report a Problem</a><br /><a href="../module/logout.php">Logout</a><br>
					
					<li class="last">
			
			</div>
			<div id="content">
				<div id="box1">
					<h2>
						<center align>Update Your Account</center>
					</h2>
					<?php include "../module/userUpdateForm.php";?>
				</div>
				
			</div>
			<br class="clear" />
		</div>
	</div>
	<div id="copyright">
		&copy; 2012&ndash;2013 Sigma Alpha Epsilon Georgia Omega<br> Design
		and coding by Autry Short and M.H. "squints" McCarsky
	</div>
	</div>
</body>
</html>
<?php
} else {movePage(403,"../http/forbidden.html");

}?>