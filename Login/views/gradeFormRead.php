<?php 
include "../module/securityFunctions.php";
include "../module/db_connect.php";
sec_session_start();
//this goes at the very top of the page, in the header
error_reporting(0);
if(login_check($mysqli) == true) {?>

<html>
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>SPSU &Sigma;AE</title>
<link rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="style.css" />
</head>
<body>
	<div id="bg">
		<div id="outer">
			<div id="header">
				<div id="logo">&Sigma;AE Georgia Omega</div>
				<div id="nav">
					<ul>
						<li class="first active"><a href="homemain.php">Home</a>
						</li>
						<li><a href="brothersmain.php">brothers</a>
						</li>
						<li><a href="calendarmain.php">Calendar</a>
						</li>
						<li><a href="scholarshipmain.php">scholarship</a>
						</li>
						<li><a href="housemain.php">house</a>
						</li>
                        <?php if ((privCheck("admin", $mysqli)) === true){?>
                        <li><a href="admincontrol.php">admin</a>
						</li>
                        <?php } ?>
						<li><a href="resourcesmain.php">Resources</a>
						</li>
						<li class="last"><a href="archivesmain.php">archives</a>
						</li>
					</ul>
					<br class="clear" />
				</div>
			</div>
            <div id="main">
			<div id="sidebar2">
				<ul class="linkedList">
				<?php echo $_SESSION['username']." ";?><a href="../module/logout.php">Not you?</a><br /><a href="../views/offreportmain.php">Post an Officer Report</a><br> <a href="updatemain.php">Update Account</a><br /> <a href="updatemain.php">Change Password</a><br /><a href="reportmain.php">Report a Problem</a><br /><a href="../module/logout.php">Logout</a><br>
                </ul>			
			</div>
			<div id="contentschoread">
            <?php
            $user = $_GET['user'];
			if ((privCheck ("scho", $mysqli) === true)||($user === $_SESSION['user_id'])){
			$name = mysqli_query($mysqli,"SELECT first, last FROM grades WHERE userid = '$user' LIMIT 1");
				while($row1 = mysqli_fetch_array($name)){
					
				?>
					<h2>
						<center align>Recent Study Hours for <?php echo $row1['first']." ".$row1['last'];?></center>
					</h2>
                    <br>
                    <center>
				<?php }
				
				
				
				
				// Check connection
				if (mysqli_connect_errno())
				{
					echo "Failed to connect to MySQL: " . mysqli_connect_error();
				}
			
				$result = mysqli_query($mysqli,"SELECT * FROM grades WHERE userid = '$user' ORDER BY sortdate DESC LIMIT 10");
				
				while($row = mysqli_fetch_array($result))
				{
					?>
				<table border="1">
					<tr>
						<th><?php echo "Date Submited: ";
						echo $row['date'];?>
						</th>
						<th>Recent Grades</th>
						<th>Hours</th>
				
				
				
						<?php if (strlen($row['class1']) != 0) { ?>
					
					
					<tr>
						<th><?php echo "Recent grades for ".$row['class1'];?>
						</th>
						<td><?php echo $row['recent1'];?>
						</td>
						<td><?php echo $row['hour1'];?>
						</td>
					</tr>
					<?php } ?>
				
				
				
					<?php if (strlen($row['class2']) != 0) { ?>
					<tr>
						<th><?php echo "Recent grades for ".$row['class2'];?>
						</th>
						<td><?php echo $row['recent2'];?>
						</td>
						<td><?php echo $row['hour2'];?>
						</td>
					</tr>
					<?php } ?>
				
				
				
					<?php if (strlen($row['class3']) != 0) { ?>
					<tr>
						<th><?php echo "Recent grades for ".$row['class3'];?>
						</th>
						<td><?php echo $row['recent3'];?>
						</td>
						<td><?php echo $row['hour3'];?>
						</td>
					</tr>
					<?php } ?>
				
				
				
					<?php if (strlen($row['class4']) != 0) { ?>
					<tr>
						<th><?php echo "Recent grades for ".$row['class4'];?>
						</th>
						<td><?php echo $row['recent4'];?>
						</td>
						<td><?php echo $row['hour4'];?>
						</td>
					</tr>
					<?php } ?>
				
				
				
					<?php if (strlen($row['class5']) != 0) { ?>
					<tr>
						<th><?php echo "Recent grades for ".$row['class5'];?>
						</th>
						<td><?php echo $row['recent5'];?>
						</td>
						<td><?php echo $row['hour5'];?>
						</td>
					</tr>
					<?php } ?>
				
				
				
					<?php if (strlen($row['class6']) != 0) { ?>
					<tr>
						<th><?php echo "Recent grades for ".$row['class6'];?>
						</th>
						<td><?php echo $row['recent6'];?>
						</td>
						<td><?php echo $row['hour6'];?>
						</td>
					</tr>
					<?php } ?>
				
				
				
					<?php if (strlen($row['class7']) != 0) { ?>
					<tr>
						<th><?php echo "Recent grades for ".$row['class7'];?>
						</th>
						<td><?php echo $row['recent7'];?>
						</td>
						<td><?php echo $row['hour7'];?>
						</td>
					</tr>
					<?php } ?>
				</table>
                </center>
				<br />
				<br />
				<?php
				}
				mysqli_close($mysqli);
			}else echo "You do not have permission to view this page.  If you think that you should please report the problem in the sidebar.";
				?>
				<br class="clear" />
			</div>
		</div>
        </div>
	</div>
	<div id="copyright">
		&copy; 2012&ndash;2013 Sigma Alpha Epsilon Georgia Omega<br> Design
		and coding by Autry Short and M.H. "squints" McCarsky
	</div>
	
</body>
</html>
<?php
} else {movePage(403,"../http/forbidden.html");
}?>
