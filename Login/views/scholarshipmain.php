<?php 
include "../module/securityFunctions.php";
include "../module/db_connect.php";
sec_session_start();
//this goes at the very top of the page, in the header
error_reporting(0);
if(login_check($mysqli) == true) {?>

<html>
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>SPSU &Sigma;AE</title>
<link rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="style.css" />
</head>
<body>
	<div id="bg">
		<div id="outer">
			<div id="header">
				<div id="logo">&Sigma;AE Georgia Omega</div>
				<div id="nav">
					<ul>
						<li class="first active"><a href="homemain.php">Home</a>
						</li>
						<li><a href="brothersmain.php">brothers</a>
						</li>
						<li><a href="calendarmain.php">Calendar</a>
						</li>
						<li><a href="scholarshipmain.php">scholarship</a>
						</li>
						<li><a href="housemain.php">house</a>
						</li>
                        <?php if ((privCheck("admin", $mysqli)) === true){?>
                        <li><a href="admincontrol.php">admin</a>
						</li>
                        <?php } ?>
						<li><a href="resourcesmain.php">Resources</a>
						</li>
						<li class="last"><a href="archivesmain.php">archives</a>
						</li>
					</ul>
					<br class="clear" />
				</div>
			</div>
            <?php if ((privCheck("scho", $mysqli)) === true){ ?>
            <div id="main">
			<div id="sidebar2">
				<ul class="linkedList">
				<?php echo $_SESSION['username']." ";?><a href="../module/logout.php">Not you?</a><br /><a href="../views/offreportmain.php">Post an Officer Report</a><br> <a href="updatemain.php">Update Account</a><br /> <a href="updatemain.php">Change Password</a><br /><a href="reportmain.php">Report a Problem</a><br /><a href="../module/logout.php">Logout</a><br>
                </ul>			
			</div>
			<div id="contentscho">
					<h2>
						<center align>Recent Study Hours</center>
					</h2>
                <center>(Clcik once on a column header to sort descending and click again to sort ascending or click on a first name for a detailed review.)</center>
				<?php include "../module/gradeFormTable.php";?>
				<br class="clear" />
			</div>
		</div>
        </div>
	</div>
    <?php }
	else {
	?>
                <div id="main">
			<div id="sidebar1">
				<h3>Announcements</h3>
				<ul class="linkedList">
				<?php include '../module/anns.php';?>
                </ul>			
			</div>
			<div id="sidebar2">
				<ul class="linkedList">
				<?php echo $_SESSION['username']." ";?><a href="../module/logout.php">Not you?</a><br /><a href="../views/offreportmain.php">Post an Officer Report</a><br> <a href="updatemain.php">Update Account</a><br /> <a href="updatemain.php">Change Password</a><br /><a href="reportmain.php">Report a Problem</a><br /><a href="../module/logout.php">Logout</a><br>
                </ul>
					
			
			</div>
			<div id="content">
					<h2>
						<center align>Post Study Hours</center>
					</h2>
                    <a href="gradeFormRead.php?user=<?php echo $_SESSION['user_id'];?>">A detailed review of recent study hours</a>
				<?php include "../module/gradeForm.php";?>
				<br class="clear" />
			</div>
		</div>
        </div>
	</div>
	<div id="copyright">
		&copy; 2012&ndash;2013 Sigma Alpha Epsilon Georgia Omega<br> Design
		and coding by Autry Short and M.H. "squints" McCarsky
	</div>
	
</body>
</html>
<?php
	}} else {movePage(403,"../http/forbidden.html");

}?>