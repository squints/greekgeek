<?php 
include "../module/securityFunctions.php";
include "../module/db_connect.php";
sec_session_start();
//this goes at the very top of the page, in the header


if(login_check($mysqli) == true) {?>

<html>
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>SPSU &Sigma;AE</title>
<link rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="style.css" />
</head>
<body>
	<div id="bg">
		<div id="outer">
			<div id="header">
				<div id="logo">&Sigma;AE Georgia Omega</div>
				<div id="nav">
					<ul>
						<li class="first active"><a href="homemain.php">Home</a>
						</li>
						<li><a href="brothersmain.php">brothers</a>
						</li>
						<li><a href="calendarmain.php">Calendar</a>
						</li>
						<li><a href="scholarshipmain.php">scholarship</a>
						</li>
						<li><a href="housemain.php">house</a>
						</li>
                        <?php if ((privCheck("admin", $mysqli)) === true){?>
                        <li><a href="admincontrol.php">admin</a>
						</li>
                        <?php } ?>
						<li><a href="resourcesmain.php">Resources</a>
						</li>
						<li class="last"><a href="archivesmain.php">archives</a>
						</li>
					</ul>
					<br class="clear" />
				</div>
			</div>
			<div id="sidebar1">
				<h3>Announcements</h3>
				<ul class="linkedList">
					<li class="first">
					<?php
					error_reporting(0);
					
					$result = mysqli_query($mysqli,"SELECT title, username, announcement, date FROM posts WHERE type = 'House' ORDER BY id DESC LIMIT 5");
					
					while($row = mysqli_fetch_array($result))
					{
						echo "<div id=announcementdetail>"."<h4>" .$row['title']. "</h4>";
						echo"<br>";
						echo $row['username']." says,"."</div>";
						echo"<br>";
						echo "<div id=announcement>".$row['announcement']."</div>";
						echo"<br>";
						echo "<div id=announcementdetail>".$row['date']."</div>";
						echo"<br>";
						echo "<li>";
						echo"<br>";
					}
					?>					
					<li class="last">
			
			</div>
			<div id="sidebar2">
				<h3>Make an Anouncement</h3>
				<ul class="linkedList">
					<li class="first"><?php include '../module/annpost.php';?><?php echo $_SESSION['username']." ";?><a href="../module/logout.php">Not you?</a><br /><a href="../views/offreportmain.php">Post an Officer Report</a><br> <a href="updatemain.php">Update Account</a><br /> <a href="updatemain.php">Change Password</a><br /><a href="reportmain.php">Report a Problem</a><br /><a href="../module/logout.php">Logout</a><br>
					
					<li class="last">
			
			</div>
			<div id="content">
				<div id="box1">
					<h2>
						<center align>House</center>
					</h2><center align>
				<?php include "../module/choreRR.php";?>
<br  />
	<?php 
$result = mysqli_query($mysqli, "SELECT users.first, users.last, users.roomNum, choreslist.chores FROM choreslist LEFT JOIN users ON choreslist.roomNum = users.roomNum ORDER BY choreslist.roomNum");?>
<table border="1">
	<tr>
		<th>First Name</th>
		<th>Last Name</th>
		<th>Room Number</th>
		<th>Week's Chore</th>
	</tr>
<?php while($row = mysqli_fetch_array($result)){
    ?>
	<tr>
		<td><?php echo $row['first'] ?></td>
		<td><?php echo $row['last'] ?></td>
		<td><?php echo $row['roomNum'] ?></td>
		<td><?php echo $row['chores'] ?></td>
	</tr>
	<?php }?>
</table>   
<br /><table border="1">
	    	<tr>
		<th>First Name</th>
		<th>Last Name</th>
        <th>Position</th>
        <th>Privlege</th>
        <th>New Privlege</th>
        <th></th>
	</tr>
	<?php
	$result = mysqli_query($mysqli, "SELECT id, username, first, last, position, privid FROM users");//where active when applicable

	while($row = mysqli_fetch_array($result))
	{
		?>
	<form id="post" action="../admin/updatePriv.php" method="post">

		<tr>

			<td><?php echo $row['first'];?></td>
			<td><?php echo $row['last'];?></td>
			<td><?php echo $row['position'];?></td>
			<td><?php echo $row['privid'];?></td>
			<td><select name="priv">
					<option placeholder="" value="" selected></option>
					<option value="1">Admin</option>
					<option value="2">Archon</option>
					<option value="3">Deputy Archon</option>
					<option value="4">Recorder</option>
					<option value="5">Treasurer</option>
					<option value="6">Scholar Chair</option>
					<option value="7">Rush Chair</option>
					<option value="8">Risk Manager</option>
					<option value="9">Pledge Ed</option>
					<option value="10">Social Chair</option>
					<option value="11">House Manager</option>
					<option value="12">Pledge</option>
					<option value="13">Brothas</option>
					<option value="69">Innactive</option>
			</select>
			</td>
			<input type="hidden" name="User" value="<?php echo $row['id'];?>">
			<input type="hidden" name="Username"
				value="<?php echo $row['username'];?>">
			<td><button id="sub">Change User Privleges</button></td>
		</tr>
	</form>

	<?php
	}
	?>
</table></center>
description of each chore so that small tags can be used in the table    
				</center>
			</div>
				<br class="clear" />
		</div>
	</div>
    <br class="clear" />
	<div id="copyright">
		&copy; 2012&ndash;2013 Sigma Alpha Epsilon Georgia Omega<br> Design
		and coding by Autry Short and M.H. "squints" McCarsky
	</div>
	</div>
</body>
</html>
<?php
} else {movePage(403,"../http/forbidden.html");

}?>