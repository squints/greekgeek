<?php 
include "../module/securityFunctions.php";
include "../module/db_connect.php";
sec_session_start();
//this goes at the very top of the page, in the header


if(login_check($mysqli) == true) {?>

<html>
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>SPSU &Sigma;AE</title>
<link rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="style.css" />
</head>
<body>
	<div id="bg">
		<div id="outer">
			<div id="header">
				<div id="logo">&Sigma;AE Georgia Omega</div>
				<div id="nav">
					<ul>
						<li class="first active"><a href="homemain.php">Home</a>
						</li>
						<li><a href="brothersmain.php">brothers</a>
						</li>
						<li><a href="calendarmain.php">Calendar</a>
						</li>
						<li><a href="scholarshipmain.php">scholarship</a>
						</li>
						<li><a href="housemain.php">house</a>
						</li>
                        <?php if ((privCheck("admin", $mysqli)) === true){?>
                        <li><a href="admincontrol.php">admin</a>
						</li>
                        <?php } ?>
						<li><a href="resourcesmain.php">Resources</a>
						</li>
						<li class="last"><a href="archivesmain.php">archives</a>
						</li>
					</ul>
					<br class="clear" />
				</div>
			</div>
			<div id="sidebar1">
				<h3>Announcements</h3>
				<ul class="linkedList">
					<li class="first"><?php include '../module/anns.php';?>
					
					<li class="last">
			
			</div>
			<div id="sidebar2">
				<h3>Make an Anouncement</h3>
				<ul class="linkedList">
					<li class="first"><?php include '../module/annpost.html';?><h3>Make an Officer Report</h3><?php include '../module/report.html';?>
						<?php echo $_SESSION['username']." ";?><a href="../module/logout.php">Not you?</a><br /> <a href="updatemain.php">Update Account</a><br /> <a href="updatemain.php">Change Password</a><br /><a href="reportmain.php">Report a problem</a><br /><a href="../module/logout.php">Logout</a><br>
					
					<li class="last">
			
			</div>
			<div id="content">
				<div id="box1">
					<h2>
						<center align>resource</center>
					</h2>
					<iframe
						src="https://www.google.com/calendar/embed?title=Monthly%20Calendar&amp;showTitle=0&amp;showPrint=0&amp;showTabs=0&amp;showCalendars=0&amp;showTz=0&amp;mode=AGENDA&amp;height=600&amp;wkst=1&amp;bgcolor=%23ffffff&amp;src=georgiaomegasae%40gmail.com&amp;color=%23B1440E&amp;ctz=America%2FNew_York"
						style="border: solid 1px #777" width="520" height="500"
						frameborder="0" scrolling="no"></iframe>
				</div>
				<div id="box2">
					<h2>Top Brother GPAs</h2>
					<?php include '../module/topgpa.php';?>

				</div>
				<div id="box3">
					<h2>Etiam arcu laoreet egestas</h2>
					<p>Tincidunt proin lorem phasellus. Iaculis sociis nibh sociis.</p>
					<ul class="linkedList">
						<li class="first"><a href="#">Parturient nascetur tellus elit</a>
						</li>
						<li><a href="#">Adipiscing turpis sed dignissim</a>
						</li>
						<li><a href="#">Nibh aliquam tempor ornare</a>
						</li>
						<li><a href="#">Commodo consectetur nulla vitae</a>
						</li>
						<li class="last"><a href="#">Parturient ullamcorper magnis</a>
						</li>
					</ul>
				</div>
				<br class="clear" />
			</div>
			<br class="clear" />
		</div>
	</div>
	<div id="copyright">
		&copy; 2012&ndash;2013 Sigma Alpha Epsilon Georgia Omega<br> Design
		and coding by Autry Short and M.H. "squints" McCarsky
	</div>
	</div>
</body>
</html>
<?php
} else {movePage(403,"../http/forbidden.html");

}?>