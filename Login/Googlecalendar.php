<?php
require_once 'Zend/Loader.php';
//require './module/securityFunctions.php';
Zend_Loader::loadClass('Zend_Gdata');
Zend_Loader::loadClass('Zend_Gdata_AuthSub');
Zend_Loader::loadClass('Zend_Gdata_Calendar');
Zend_Loader::loadClass('Zend_Http_Client');

/** Retrieve the current URL so that the AuthSub server knows where to
 * redirect the user after authentication is complete.
*/
function getCurrentUrl()
{
	global $_SERVER;

	// Filter php_self to avoid a security vulnerability.
	$php_request_uri = htmlentities(substr($_SERVER['REQUEST_URI'], 0, strcspn($_SERVER['REQUEST_URI'], "\n\r")), ENT_QUOTES);

	if (isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) == 'on') {
		$protocol = 'https://';
	} else {
		$protocol = 'http://';
	}
	$host = $_SERVER['HTTP_HOST'];
	if ($_SERVER['HTTP_PORT'] != '' &&
	(($protocol == 'http://' && $_SERVER['HTTP_PORT'] != '80') ||
			($protocol == 'https://' && $_SERVER['HTTP_PORT'] != '443'))) {
		$port = ':' . $_SERVER['HTTP_PORT'];
	} else {
		$port = '';
	}
	return $protocol . $host . $port . $php_request_uri;
}

/** Obtain an AuthSub authenticated HTTP client, redirecting the user to the
 *  AuthSub server to login if necessary.
 */
function getAuthSubHttpClient()
{
	global $_SESSION, $_GET;

	// If there is no AuthSub session or one-time token waiting for us,
	// redirect the user to the AuthSub server to get one.
	if (!isset($_SESSION['sessionToken']) && !isset($_GET['token'])) {
		// Parameters to give to AuthSub server
		$next = getCurrentUrl();
		$scope = "http://www.google.com/calendar/feeds/";
		$secure = false;
		$session = true;

		// Redirect the user to the AuthSub server to sign in

		$authSubUrl = Zend_Gdata_AuthSub::getAuthSubTokenUri($next, $scope, $secure, $session);
		header("HTTP/1.0 307 Temporary redirect");

		header("Location: " . $authSubUrl);

		exit();
	}

	// Convert an AuthSub one-time token into a session token if needed
	if (!isset($_SESSION['sessionToken']) && isset($_GET['token'])) {
		$_SESSION['sessionToken'] =
		Zend_Gdata_AuthSub::getAuthSubSessionToken($_GET['token']);
	}

	// At this point we are authenticated via AuthSub and can obtain an
	// authenticated HTTP client instance

	// Create an authenticated HTTP client
	$client = Zend_Gdata_AuthSub::getHttpClient($_SESSION['sessionToken']);
	return $client;
}

// -> Script execution begins here <-

// Make sure http://code.google.com/apis/gdata/reference.html#Queriesthat the user has a valid session, so we can record the
// AuthSub session token once it is available.
session_start();

// Create an instance of the Calendar service, redirecting the user
// to the AuthSub server if necessary.
$service = new Zend_Gdata_Calendar(getAuthSubHttpClient());
try {
	$listFeed= $service->getCalendarListFeed();
} catch (Zend_Gdata_App_Exception $e) {
	echo "Error: " . $e->getMessage();
}
///AUTRY TODO: You wanted to put this in a itable or something?
echo "<h1>Calendar List Feed</h1>";
echo "<ul>";
foreach ($listFeed as $calendar) {
	echo "<li>" . $calendar->title .
	" (Event Feed: " . $calendar->id . ")</li>";
}
echo "</ul>";
?>

