<?php

$stmt = $mysqli->prepare("SELECT lastUpdate FROM choreList ORDER BY lastUpdate DESC LIMIT 1");

$stmt->execute(); // Execute the prepared query.

$stmt->store_result();

$stmt->bind_result($lastTime); // get main.phpiables from result.

$stmt->fetch();

$stmt->close();

$time = date("F j, Y, g:i a", strtotime($lastTime));

$Timenow = new DateTime();

$Timethen = new DateTime($time);

$Timethen = $Timethen->add(new DateInterval('P7D'));




if($Timethen < $Timenow) {

	rotate($mysqli);

	echo "Chores list successfully updated.";

} else {

	$interval = $Timenow->diff($Timethen);

	echo $interval->format('Time until chore rotation: %d days, %h hours, %i minutes, %s seconds.');

}


function resultToArray($result) {

	$rows = array();

	while($row = $result->fetch_row()) {

		$rows[] = $row;

	}

	return $rows;

}


function rotate($mysqli) {




	$result = $mysqli->query("SELECT chores FROM choreList");

	$rows = resultToArray($result);

	$result->free();


	$newRow = array();

	$newRow[0] = $rows[count($rows)-1][0];


	for($i = 1, $size = count($rows); $i<$size; $i++){

		$newRow[$i] = $rows[$i-1][0];

	}


	for($i = 0, $size = count($newRow); $i<$size; $i++){


		$stmt = $mysqli->prepare("UPDATE choreList SET roomNum=? WHERE chores=?");

		$stmt->bind_param('ss', $newRow[$i], $rows[$i][0]);

		$stmt->execute();

		$stmt->close();


	}



}

function reverse($mysqli) {




	$result = $mysqli->query("SELECT chores FROM choreList");

	$rows = resultToArray($result);

	$result->free();


	$newRow = array();

	$newRow[count($rows)-1] = $rows[0][0];


	for($i = count($rows)-2; $i>=0; $i--){

		$newRow[$i] = $rows[$i+1][0];

	}


	for($i = 0, $size = count($newRow); $i<$size; $i++){


		$stmt = $mysqli->prepare("UPDATE choreList SET roomNum=? WHERE chores=?");

		$stmt->bind_param('ss', $newRow[$i], $rows[$i][0]);

		$stmt->execute();

		$stmt->close();


	}



}

?>
