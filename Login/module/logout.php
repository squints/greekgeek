<?php
include 'securityFunctions.php';
sec_session_start();

// Unset all session values
$_SESSION = array();
// get session parameters
$params = session_get_cookie_params();
// Delete the actual cookie.
setcookie(session_name(), '', time() - 72000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
// Destroy session
session_destroy();


?>
<html>
<head>
<title>You have sucessfully logged out!</title>


<body>
	You have successfully logged out!
	<br /> You will be automatically redirected, if not click here:
	<a href="Index.php">Login Page</a>
	<meta http-equiv="refresh"
		content="3; url=http://localhost/sae-website/Login/">
</body>
</head>
</html>
