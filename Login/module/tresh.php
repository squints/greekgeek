<?php
function add($mysqli, $amount, $user)
{
	$stmt = $mysqli->prepare("UPDATE `tres` SET `amount` = `amount` + ? WHERE `id` = ?");
	$stmt->bind_param('ss', $amount, $user ); // Bind "username" to parameter.
	$stmt->execute();
	$stmt->close();
}

function pay($mysqli, $amount, $user)
{
	$stmt = $mysqli->prepare("UPDATE `tres` SET `amount` = `amount` - ? WHERE `id` = ?");
	$stmt->bind_param('ss', $amount, $user ); // Bind "username" to parameter.
	$stmt->execute();
	$stmt->close();
}

function showAmount($mysqli, $user)
{
	$stmt = $mysqli->prepare("SELECT `amount` FROM `tres` WHERE `user`=? ");
	$stmt->bind_param('s', $user); // Bind "username" to parameter.
	$stmt->execute(); // Execute the prepared query.
	$stmt->store_result();
	$stmt->bind_result($amount); // get main.phpiables from result.
	$stmt->fetch();
	$stmt->close();
	echo "Current amount owed: ".$amount;
}
?>