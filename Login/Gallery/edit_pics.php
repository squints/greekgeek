<?php
include '../module/db_connect.php';

function readDB_Pics($mysqli){
	echo "<h2>Current albums:</h2>";
	$result = mysqli_query($mysqli, "SELECT album, albumid FROM albums ORDER BY album ASC ");

	//rows = [picnum[album, source, thumb ], picnum[album, source, thumb], etc.]
	while ($row = mysqli_fetch_array($result)){
		$shizzle=$row['album'];
		$nizzle=$row['albumid'];
		$result1 = mysqli_query($mysqli, "SELECT source FROM pics WHERE albumid=$nizzle");
		echo "<br><ul><li><h3>Album Name: ".$shizzle."</h3></li>";

		while ($row = mysqli_fetch_array($result1)){
			echo "<ul><li>".$row['source']."</li>";

			echo "</ul>";
	 }
	 echo "</ul>";
	}

}

function writeXML($mysqli){

	if(file_exists("config.xml")){
		unlink("config.xml");
	}

	$xml = new XMLWriter();

	$xml->openURI("config.xml");
	$xml->startDocument();
	$xml->setIndent(true);

	$xml->startElement('gallery');
	$xml->startElement('albums');

	$result = mysqli_query($mysqli, "SELECT album, albumid, icon FROM albums ORDER BY album ASC ");

	//rows = [picnum[album, source, thumb ], picnum[album, source, thumb], etc.]
	while ($row = mysqli_fetch_array($result)){
		$shizzle=$row['album'];
		$nizzle=$row['albumid'];
		$forizzle=$row['icon'];
		$result1 = mysqli_query($mysqli, "SELECT source FROM pics WHERE albumid=$nizzle");
		$xml->startElement('album');
		$xml->writeAttribute('thumbnailsFolder','images/');
		$xml->writeAttribute('imagesFolder','images/');
		$xml->writeAttribute('icon',$forizzle);
		$xml->writeAttribute('description',$shizzle);
		$xml->startElement('items');

		while ($row = mysqli_fetch_array($result1)){

			$xml->startElement('item');
			$xml->writeAttribute('source',$row['source']);
			$xml->writeAttribute('thumb',$row['source']);
			$xml->endElement();
	 }
	 $xml->endElement();//end items
	 $xml->endElement();//end albums
	}





	$xml->endElement();//end albums
	$xml->endElement();//end gallery
	$xml->flush();
}

function addPicture($album, $src){

	$insert= $mysqli->prepare("INSERT INTO pics ( albumid, source) VALUES (?, ?)");
	$insert->bind_param("ss", $album, $src);

	$success =$insert->execute();

	if($success === true){
		echo "Successfully added picture!";
	}
	else { echo "OH NOES! Couldn't add pic.";
	}

}

function deletePicture($src){

	$insert= $mysqli->prepare("DELETE FROM pics  WHERE source=?");
	$insert->bind_param("s", $src);

	$success =$insert->execute();

	if($success === true){
		echo "Successfully deleted picture!";
	}
	else { echo "OH NOES! Couldn't delete pic.";
	}

}

function deleteAlbum($album){

	$insert= $mysqli->prepare("DELETE FROM pics  WHERE album=?");
	$insert->bind_param("s", $album);

	$success =$insert->execute();

	if($success === true){
		echo "Successfully deleted picture!";
	}
	else { echo "OH NOES! Couldn't delete pic.";
	}

}

readDB_Pics($mysqli);

writeXML($mysqli);

?>